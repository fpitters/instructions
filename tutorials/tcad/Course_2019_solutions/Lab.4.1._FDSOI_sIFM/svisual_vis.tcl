load_library ifm

set FILEs IdVg_n@node|sdevice@_sIFM_rdf_I_ndrain.csv
ifm::ReadsIFM out= sIFM nrow= Nrow ncol= Ncol files= $FILEs ids= rdf

create_plot 1d -name IdVg_rdf
select_plots IdVg_rdf

load_file IdVg_n@node|sdevice@_sIFM_ac_des.plt -name AC_IFM
load_file IdVg_n@node|sdevice@_sys_des.plt -name DC_IFM

set Vgs [get_variable_data "v(ngate)" -dataset DC_IFM]
set Ids [get_variable_data "i(vdrain,ndrain)" -dataset DC_IFM]
set adgs [get_variable_data "a(ndrain,ngate)" -dataset AC_IFM]

ifm::GetMOSIVs out= IV_rdf sifm= sIFM nrow= Nrow ncol= Ncol sgn= 1.0 v= $Vgs i= $Ids y= $adgs id= rdf smooth= yes

for {set i 0} {$i < 200} {incr i} {
	create_variable -name V -dataset IdVg_rdf(@node|sdevice@,rdf,$i) -values $IV_rdf(X,$i)
	create_variable -name I -dataset IdVg_rdf(@node|sdevice@,rdf,$i) -values $IV_rdf(Y,$i)
	create_curve -name IV_ran($i) -dataset IdVg_rdf(@node|sdevice@,rdf,$i) -axisX "V" -axisY "I"
	set_curve_prop IV_ran($i) -color blue -line_style solid -line_width 1 
}

create_curve -name IV_ref -dataset DC_IFM -axisX "v(ngate)" -axisY "i(vdrain,ndrain)"
set_curve_prop IV_ref -color red -line_style solid -line_width 3

set_axis_prop -plot IdVg_rdf -axis x -title "Gate Voltage (V)" -title_font_size 15 -scale_font_size 12
set_axis_prop -plot IdVg_rdf -axis y -title "Drain Current (A/um)" -title_font_size 15 -scale_font_size 12

