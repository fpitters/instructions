math coord.ucs


line x location=0.0<um>   tag=SubTop      spacing= 20.0<nm>
line x location=2.0<um>   tag=SubBottom   spacing= 20.0<nm>

region silicon xlo=SubTop xhi=SubBottom
init concentration=1e+10<cm-3> field=Boron

implant species=Boron Silicon 

implant Boron dose=1e14<cm-2> energy=400.0<keV> 

SetPlxList {BTotal}
WritePlx n@node@_AsImplant.plx
struct tdr=n@node@_AsImplant

diffuse temperature= 1100<C> time=2<hr>
WritePlx n@node@_as_anneal.plx

pdbSet Silicon Boron AcInit 1e17
pdbSet Silicon Boron CluRate {[Arr 5e6 5.6]}

init tdr= n@node@_AsImplant
diffuse temperature= 1100<C> time=2<hr>
WritePlx n@node@_sol.plx
