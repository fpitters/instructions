* Copyright (c) 1994-2015 Synopsys, Inc.
* This parameter file and the associated documentation are proprietary
* to Synopsys, Inc.  This parameter file may only be used in accordance
* with the terms and conditions of a written license agreement with
* Synopsys, Inc.  All other use, reproduction, or distribution of this
* parameter file is strictly prohibited.

* [01Sch] Schaffler, Properties of Advanced Semiconductor Materials GaN, AlN, InN, BN, SiC, Sige,
* John Wiley & Sons, Inc., New York, pp. 149-188, 2001

Epsilon
  {
    * Ratio of the permittivities of material and vacuum
    * epsilon() = epsilon

    epsilon = 11.7 #[1], [01Sch]
  }

Epsilon_aniso
  {
    * Ratio of the permittivities of material and vacuum
    * epsilon() = epsilon

    epsilon = 11.7 #[1], see isotropic case
  }

LatticeHeatCapacity
  {
    * cv() = cv + cv_b * T + cv_c * T^2 + cv_d * T^3 
    * lumped electron-hole-lattice heat capacity

    cv = 1.63 #[J/(K cm^3)], [01Sch]
    cv_b = 0.0000e+00 #[J/(K^2 cm^3)]
    cv_c = 0.0000e+00 #[J/(K^3 cm^3)]
    cv_d = 0.0000e+00 #[J/(K^4 cm^3)]
  }

RefractiveIndex
{ *  Optical Refractive Index

  * refractiveindex() = refractiveindex * (1 + alpha * (T-Tpar))
	Tpar	= 3.0000e+02	# [K]
	refractiveindex	= 3.45	# [1]
	alpha	= 2.0000e-04	# [1/K]

  * Gain dependence of refractive index in active region:
  * a) Linear model: delta n = a0 * ( (n+p)/(2 * N0) - 1)
  * b) Logarithmic model: delta n = a0 * log ( (n+p)/(2 * N0) )
  * where n/p are the carrier densities in the active region. 
	a0	= 0.0000e+00	# [1]
	N0	= 1.0000e+18	# [1/cm^3]
}


ComplexRefractiveIndex
{ *  Complex refractive index model: n_complex = n + i*k (unitless)
  *  
  *  with n = n_0 + delta_n_lambda + delta_n_T + delta_n_carr + delta_n_gain 
  *       k = k_0 + delta_k_lambda             + delta_k_carr                
  
  * Base refractive index and extinction coefficient: 
  *     n_0, k_0 
  
  * Wavelength dependence (real and imag): 
  *     Formula 0: delta_n_lambda = Cn_lambda * lambda + Dn_lambda * lambda^2 
  *                delta_k_lambda = Ck_lambda * lambda + Dk_lambda * lambda^2 
  *     Formula 1: Read tabulated values 
  *                NumericalTable (...)  
  *     Formula 2: Read tabulated values from file 
  *                NumericalTable = <string> 
  
  * Temperature dependence (real): 
  *     delta_n_T = n_0 * ( Cn_temp * (T-Tpar)) 
  
  * Carrier dependence (real) 
  *     delta_n_carr = - Cn_carr * (const.) * (n/m_e + p/m_h) 
  
  * Carrier dependence (imag) 
  *     delta_k_carr = 1 / (4*PI) * (wavelength^Gamma_k_carr_e*Ck_carr_e*n + wavelength^Gamma_k_carr_h*Ck_carr_h*p) 
  
  * Gain dependence (real) 
  *     lin: delta_n_gain = Cn_gain * ( (n+p)/(2 * Npar) - 1) 
  *     log: delta_n_gain = Cn_gain * log ( (n+p)/(2 * Npar ) )
	n_0	= 3.45	# [1]
	k_0	= 0.0000e+00	# [1]
	Cn_lambda	= 0.0000e+00	# [um^-1]
	Dn_lambda	= 0.0000e+00	# [um^-2]
	Ck_lambda	= 0.0000e+00	# [um^-1]
	Dk_lambda	= 0.0000e+00	# [um^-2]
	Cn_temp	= 2.0000e-04	# [K^-1]
	Cn_carr	= 1	# [1]
	Ck_carr	= 3.7100e-18 ,	3.3300e-18	# [cm^2]
	Gamma_k_carr	= 3 ,	3	# [1]
	Cn_gain	= 0.0000e+00	# [1]
	Npar	= 1.0000e+18	# [cm^-3]
        Formula = 1
        TableInterpolation = PositiveSpline, PositiveSpline
        NumericalTable ( 
          0.2101	1.0922	2.8831;
          0.2119	1.1009	2.90361;
          0.2138	1.1184	2.9315;
          0.2156	1.14237	2.96529;
          0.2175	1.1705	3.0035;
          0.2194	1.19797	3.04419;
          0.2214	1.2299	3.0872;
          0.2234	1.2698	3.13376;
          0.2254	1.3177	3.1778;
          0.2275	1.3785	3.21564;
          0.2296	1.4411	3.2413;
          0.2317	1.49894	3.2415;
          0.2339	1.5433	3.2345;
          0.2362	1.55796	3.23247;
          0.2384	1.5588	3.2404;
          0.2407	1.55514	3.26932;
          0.2431	1.5504	3.3133;
          0.2455	1.55126	3.3704;
          0.248	1.5572	3.4397;
          0.2505	1.56976	3.51967;
          0.253	1.5894	3.6102;
          0.2556	1.61655	3.70929;
          0.2583	1.6528	3.8211;
          0.261	1.6961	3.94773;
          0.2638	1.7578	4.0934;
          0.2666	1.84299	4.26904;
          0.2695	1.9684	4.4566;
          0.2725	2.15662	4.64815;
          0.2755	2.3952	4.8243;
          0.2786	2.67333	4.96418;
          0.2818	3.0086	5.0643;
          0.285	3.4429	5.14668;
          0.2883	3.8925	5.1139;
          0.2917	4.30741	4.81355;
          0.2952	4.6283	4.4327;
          0.2988	4.7398	4.11512;
          0.3024	4.7601	3.8384;
          0.3061	4.77064	3.63769;
          0.31	4.7663	3.4841;
          0.3139	4.76873	3.36293;
          0.3179	4.779	3.2667;
          0.322	4.80814	3.18317;
          0.3263	4.8479	3.1119;
          0.3306	4.89223	3.04714;
          0.3351	4.9456	2.9925;
          0.3397	5.00374	2.94735;
          0.3444	5.0868	2.9186;
          0.3493	5.19221	2.93819;
          0.3542	5.3839	2.9304;
          0.3594	5.80733	2.86335;
          0.3647	6.2153	2.6374;
          0.3701	6.38979	2.02886;
          0.3757	6.3887	1.378;
          0.3815	6.14004	0.998007;
          0.3875	5.8155	0.7465;
          0.3936	5.58197	0.598994;
          0.3999	5.3818	0.515;
          0.4065	5.21597	0.44355;
          0.4133	5.0755	0.3916;
          0.4203	4.95259	0.349431;
          0.4275	4.8447	0.3161;
          0.435	4.74682	0.287837;
          0.4428	4.6586	0.2634;
          0.4509	4.57823	0.240775;
          0.4592	4.505	0.2185;
          0.4679	4.43795	0.192556;
          0.4769	4.376	0.1686;
          0.4862	4.31774	0.15245;
          0.4959	4.2632	0.1396;
          0.5061	4.212	0.127413;
          0.5166	4.1639	0.1161;
          0.5276	4.11859	0.105156;
          0.5391	4.0759	0.0941;
          0.551	4.03563	0.081281;
          0.5636	3.9975	0.0697;
          0.5767	3.96109	0.063163;
          0.5904	3.9266	0.0576;
          0.6048	3.89436	0.049456;
          0.6199	3.8636	0.041;
          0.6358	3.83326	0.033381;
          0.6525	3.8045	0.0264;
          0.6702	3.77842	0.020056;
          0.6888	3.7542	0.0149;
          0.7085	3.73137	0.011813;
          0.7293	3.71	9.8000e-03;
          0.7514	3.69049	8.1750e-03;
          0.7749	3.6714	6.9000e-03;
          0.7999	3.65111	5.6310e-03;
          0.8266	3.6305	4.6000e-03;
          0.8551	3.60901	4.0130e-03;
          0.8856	3.5894	3.6000e-03;
          0.9184	3.57576	3.1440e-03;
          0.9537	3.5636	2.7000e-03;
          0.9919	3.54898	2.1940e-03;
          1.033	3.5344	1.8000e-03;
          1.078	3.52153	1.8000e-03;
          1.127	3.5089	1.8000e-03;
          1.181	3.49585	1.5750e-03;
          1.24	3.4817	9.0000e-04;
        ) 
	Tpar	= 3.0000e+02	# [K]
} 


* SpectralConversion
* { * Spectral Conversion Model
*   No default model, user has to define.
*   All wavelength parameters should be in nanometers.
*   Choice of Analytic or NumericalTable selected in Physics section of region
*  
*   ConversionEfficiency = float     * ratio of absorbed photons that are reemitted.
*   AbsorptionScaling = float        * scale absorption
*   EmissionScaling = float          * scale emission
*   Analytic (
*     AbsorptionProfile = (
*        Gaussian(lambda0 sigma peakvalue dc_offset lambda_range0 lambda_range1)
*        Lorentzian(lambda0 width peakvalue dc_offset lambda_range0 lambda_range1)
*        ...
*     )
*     EmissionProfile = (
*        Gaussian(lambda0 sigma peakvalue dc_offset lambda_range0 lambda_range1)
*        Lorentzian(lambda0 width peakvalue dc_offset lambda_range0 lambda_range1)
*        ...
*     )
*   )
*   NumericalTable (
*     AbsorptionProfile = (
*        lambda0 value0
*        lambda1 value1
*        ...
*     )
*     EmissionProfile = (
*        lambda0 value0
*        lambda1 value1
*        ...
*     )

*   ConversionEfficiency = 1.0
* }

Kappa
{ *  Lattice thermal conductivity

  * Formula = 1:
  * kappa() = kappa + kappa_b * T + kappa_c * T^2 
	kappa	= 1.5	# [W/(K cm)]
	kappa_b	= 0.0000e+00	# [W/(K^2 cm)]
	kappa_c	= 0.0000e+00	# [W/(K^3 cm)]
}


Kappa_aniso
{ *  Lattice thermal conductivity

  * Formula = 1:
  * kappa() = kappa + kappa_b * T + kappa_c * T^2 
	kappa	= 1.5	# [W/(K cm)]
	kappa_b	= 0.0000e+00	# [W/(K^2 cm)]
	kappa_c	= 0.0000e+00	# [W/(K^3 cm)]
}


EnergyRelaxationTime
{ *  Energy relaxation times in picoseconds
	tau_w_ele	= 0.3	# [ps]
	tau_w_hol	= 0.25	# [ps]

 * Below is the example of energy relaxation time approximation
 * by the ratio of two irrational polynomials.
 * If Wmax(interval-1) < Wc < Wmax(interval), then:
 * tau_w = (tau_w)*(Numerator^Gn)/(Denominator^Gd),
 * where (Numerator or Denominator)=SIGMA[A(i)(Wc^P(i))],
 * Wc=1.5(k*Tcar)/q (in eV).
 * By default: Wmin(0)=Wmax(-1)=0; Wmax(0)=infinity.
 * The option can be activated by specifying appropriate Formula equal to 2.
 *      Formula(tau_w_ele) = 2
 *      Formula(tau_w_hol) = 2
 *      Wmax(interval)_ele = 
 *      tau_w_ele(interval)     =     
 *      Numerator(interval)_ele{
 *        A(0)  = 
 *        P(0)  = 
 *        A(1)  = 
 *        P(1)  = 
 *        G     = 
 *      }
 *      Denominator(interval)_ele{
 *        A(0)  = 
 *        P(0)  = 
 *        G     = 
 *      }

 * Note: Energy relaxation times can be either molefraction dependent
 *       or energy dependent, but not both!

 *      Wmax(interval)_hol = 
 *      tau_w_hol(interval)     =  
}

AvalancheFactors
{ *  Coefficientss for avalanche generation with hydro
  *  Factors n_l_f, p_l_f for energy relaxation length in the expressions
  *  for effective electric field for avalanche generation
  *  eEeff = eEeff / n_l_f  ( or b = b*n_l_f ) 
  *  hEeff = hEeff / p_l_f  ( or b = b*p_l_f ) 
  *  Additional coefficients n_gamma, p_gamma, n_delta, p_delta 
	n_l_f	= 1	# [1]
	p_l_f	= 1	# [1]
	n_gamma	= 1	# [1]
	p_gamma	= 1	# [1]
	n_delta	= 1.5	# [1]
	p_delta	= 1.5	# [1]
}

Bandgap
{ * Eg = Eg0 + dEg0 + alpha Tpar^2 / (beta + Tpar) - alpha T^2 / (beta + T)
  * dEg0(<bgn_model_name>) is a band gap correction term.  It is used together with
  * an appropriate BGN model, if this BGN model is chosen in Physics section
  * Parameter 'Tpar' specifies the value of lattice 
  * temperature, at which parameters below are defined
  * Chi0 is electron affinity.
	Chi0	= 4.05	# [eV]
	Bgn2Chi	= 0.5	# [1]
	Eg0	= 1.16964	# [eV]
	dEg0(Bennett)	= 0.0000e+00	# [eV]
	dEg0(Slotboom)	= -4.7950e-03	# [eV]
	dEg0(OldSlotboom)	= -1.5950e-02	# [eV]
	dEg0(delAlamo)	= -1.4070e-02	# [eV]
	alpha	= 4.7300e-04	# [eV K^-1]
	beta	= 6.3600e+02	# [K]
	alpha2	= 0.0000e+00	# [eV K^-1]
	beta2	= 0.0000e+00	# [K]
	EgMin	= 0.0000e+00	# [eV]
	dEgMin	= 0.01	# [eV]
	Tpar	= 0.0000e+00	# [K]
}


OldSlotboom
{ * deltaEg = dEg0 + Ebgn ( ln(N/Nref) + [ (ln(N/Nref))^2 + C]^1/2 )
  * dEg0 is defined in BandGap section 
	Ebgn	= 9.0000e-03	# [eV]
	Nref	= 1.0000e+17	# [cm^(-3)]
	C	= 0.5	# [1]
}


Slotboom
{ * deltaEg = dEg0 + Ebgn ( ln(N/Nref) + [ (ln(N/Nref))^2 + C]^1/2 )
  * dEg0 is defined in BandGap section 
	Ebgn	= 6.9200e-03	# [eV]
	Nref	= 1.3000e+17	# [cm^(-3)]
	C	= 0.5	# [1]
}

delAlamo
{ * deltaEg = dEg0 + Ebgn  ln(N/Nref) 
  * dEg0 is defined in BandGap section 
	Ebgn	= 0.0187	# [eV]
	Nref	= 7.0000e+17	# [cm^(-3)]
}

Bennett
{ * deltaEg = dEg0 + Ebgn (ln(N/Nref))^2
  * dEg0 is defined in BandGap section 
	Ebgn	= 6.8400e-03	# [eV]
	Nref	= 3.1620e+18	# [cm^(-3)]
}

FreeCarrierAbsorption
{
  * Coefficients for free carrier absorption:
  * fcaalpha_n for electrons,
  * fcaalpha_p for holes

  * FCA = (alpha_n * n + alpha_p * p) * Light Intensity
	fcaalpha_n	= 4.0000e-18	# [cm^2]
	fcaalpha_p	= 8.0000e-18	# [cm^2]
}

QWStrain
{
  * Zincblende crystals:
  *   Parameters: a_nu, a_c, b, C_12, C_11
  *   StrainConstant eps (formula = 1) or lattice constant
  *   a0 (formula = 2) for energy shift of quantum-well
  *   subbands.
  *   a0(T) = a0 + alpha (T-Tpar)

  * Wurtzite crystals:
  *   Parameters: a_c, D1, D2, D3, D4, C_13, C_33
  *   Lattice constants a0 and c0 (formula 2 must be used)
  *   a0(T) = a0 + alpha (T-Tpar)
  *   c0(T) = c0 + alpha (T-Tpar)

	  * Default formula	= 1	# [1]
	eps	= 0.0000e+00	# [1]
	a0	= 3.1890e-10	# [cm]
	alpha	= 0.0000e+00	# [cm/K]
	Tpar	= 3.0000e+02	# [K]
	a_nu	= 0.19	# [eV]
	a_c	= -4.0800e+00	# [eV]
	b_shear	= 0.9163	# [eV]
	c_11	= 11.879	# [1e-2 GPa]
	c_12	= 5.376	# [1e-2 GPa]
	d1	= -8.9000e-01	# [eV]
	d2	= 4.27	# [eV]
	d3	= 5.18	# [eV]
	d4	= -2.5900e+00	# [eV]
	c_13	= 1	# [1e-2 GPa]
	c_33	= 3.92	# [1e-2 GPa]
	c0	= 5.1850e-10	# [cm]
}

eDOSMass
{
  * For effective mass specification Formula1 (me approximation):
  * or Formula2 (Nc300) can be used :
	Formula	= 1	# [1]
  * Formula1:
  * me/m0 = [ (6 * mt)^2 *  ml ]^(1/3) + mm
  * mt = a[Eg(0)/Eg(T)] 
  * Nc(T) = 2(2pi*kB/h_Planck^2*me*T)^3/2 = 2.5094e19 ((me/m0)*(T/300))^3/2 
	a	= 0.1905	# [1]
	ml	= 0.9163	# [1]
	mm	= 0.0000e+00	# [1]
}

hDOSMass
{
  * For effective mass specification Formula1 (mh approximation):
  * or Formula2 (Nv300) can be used :
	Formula	= 1	# [1]
  * Formula1:
  * mh =  m0*{[(a+bT+cT^2+dT^3+eT^4)/(1+fT+gT^2+hT^3+iT^4)]^(2/3) + mm}
  * Nv(T) = 2(2pi*kB/h_Planck^2*mh*T)^3/2 = 2.5094e19 ((mh/m0)*(T/300))^3/2 
	a	= 0.443587	# [1]
	b	= 0.003609528	# [K^-1]
	c	= 0.0001173515	# [K^-2]
	d	= 1.263218e-06	# [K^-3]
	e	= 3.025581e-09	# [K^-4]
	f	= 0.004683382	# [K^-1]
	g	= 0.0002286895	# [K^-2]
	h	= 7.469271e-07	# [K^-3]
	i	= 1.727481e-09	# [K^-4]
	mm	= 0	# [1]
}

SchroedingerParameters:
{ * For the hole masses for Schroedinger equation you can
  * use different formulas.
  * 0: use the isotropic density of states effective mass
  * 1: (for materials with Si-like hole band structure)
  *    m(k)/m0=1/(A+-sqrt(B+C*((xy)^2+(yz)^2+(zx)^2)))
  *    where k=(x,y,z) is unit normal vector in reziprocal
  *    space.  '+' for light hole band, '-' for heavy hole band
  * 2: Heavy hole mass mh and light hole mass ml are
  *    specified explicitly.
  *    Use me as electron mass for free-carrier effect in 
  *    the refractive index model.
  * For electron masses, the following formula options exist:
  * 0: use the isotropic density of states effective mass
  * 1: (for materials with Si-like hole band structure)
  *    use the a, ml, and mm parameters from eDOSMass.
  *    Typically, this leads to anisotropy.
	formula	= 1 ,	1	# [1]
  * Formula(hole) 1 parameters:
	A	= 4.22	# [1]
	B	= 0.6084	# [1]
	C	= 23.058	# [1]
  * Lifting of degeneracy of bulk valleys. The value for
  * electrons is added to the band edge for the subband
  * ladder of lower degeneracy if positive, and subtracted
  * from the band edge for the ladder of higher degeneracy
  * if negative. (that is, the value of the band edge is
  * always increased).  For holes, the value is subtracted from
  * the band edge for the heavy hole band is positive,
  * add added tp that of the light hole band if
  * negative.  The signs are such that the shift always
  * moves the band edges 'outward', away from midgap.  The
  * gap itself is defined as the separation of the
  * unshifted band edges and remains unaffected.
	offset	= 0.0000e+00 ,	0.0000e+00	# [eV]
  * Alternative to the specification of formula, offset,
  * and masses, you can make an arbitrary number of ladder
  * specification, 'eLadder(mz, mxy, deg, dE) and hLadder(...)
  * Here, mz is the quantization mass, mxy an in-plane DOS mass,
  * deg the ladder degeneracy, and dE an shift of the band edge
  * for the ladder (non-negative; the shift is always outward,
  * away from midgap).  When present, we solve the Schroedinger
  * equation separately for each ladder

  * Temperatures in rescaling of the mxy for eLadder and hLadder
	ShiftTemperature	= 1.0000e+10 ,	1.0000e+10	# [K]
}

QuantumPotentialParameters
{ * gamma:  weighting factor for quantum potential
  * theta:  weight for quadratic term
  * xi:     weight for quasi Fermi potential
  * eta:    weight for electrostatic potential
  * nu :    weight for DOS mass change from stress
	gamma	= 3.6 ,	5.6	# [1]
	theta	= 0.5 ,	0.5	# [1]
	xi	= 1 ,	1	# [1]
	eta	= 1 ,	1	# [1]
	nu	= 0.0000e+00 ,	0.0000e+00	# [1]
}

ConstantMobility:
{ * mu_const = mumax (T/T0)^(-Exponent)
	mumax	= 1.4170e+03 ,	4.7050e+02	# [cm^2/(Vs)]
	Exponent	= 2.5 ,	2.2	# [1]
	mutunnel	= 0.05 ,	0.05	# [cm^2/(Vs)]
}

ConstantMobility_aniso:
{ * mu_const = mumax (T/T0)^(-Exponent)
	mumax	= 1.4170e+03 ,	4.7050e+02	# [cm^2/(Vs)]
	Exponent	= 2.5 ,	2.2	# [1]
	mutunnel	= 0.05 ,	0.05	# [cm^2/(Vs)]
}

DopingDependence:
{
  * For doping dependent mobility model three formulas
  * can be used. Formula1 is based on Masetti et al. approximation.
  * Formula2 uses approximation, suggested by Arora.
	formula	= 1 ,	1	# [1]
  * If formula=1, model suggested by Masetti et al. is used:
  * mu_dop = mumin1 exp(-Pc/N) + (mu_const - mumin2)/(1+(N/Cr)^alpha)
  *                             - mu1/(1+(Cs/N)^beta)
  * with mu_const from ConstantMobility
	mumin1	= 52.2 ,	44.9	# [cm^2/Vs]
	mumin2	= 52.2 ,	0.0000e+00	# [cm^2/Vs]
	mu1	= 43.4 ,	29	# [cm^2/Vs]
	Pc	= 0.0000e+00 ,	9.2300e+16	# [cm^3]
	Cr	= 9.6800e+16 ,	2.2300e+17	# [cm^3]
	Cs	= 3.4300e+20 ,	6.1000e+20	# [cm^3]
	alpha	= 0.68 ,	0.719	# [1]
	beta	= 2 ,	2	# [1]
}

DopingDependence_aniso:
{
  * For doping dependent mobility model three formulas
  * can be used. Formula1 is based on Masetti et al. approximation.
  * Formula2 uses approximation, suggested by Arora.
	formula	= 1 ,	1	# [1]
  * If formula=1, model suggested by Masetti et al. is used:
  * mu_dop = mumin1 exp(-Pc/N) + (mu_const - mumin2)/(1+(N/Cr)^alpha)
  *                             - mu1/(1+(Cs/N)^beta)
  * with mu_const from ConstantMobility
	mumin1	= 52.2 ,	44.9	# [cm^2/Vs]
	mumin2	= 52.2 ,	0.0000e+00	# [cm^2/Vs]
	mu1	= 43.4 ,	29	# [cm^2/Vs]
	Pc	= 0.0000e+00 ,	9.2300e+16	# [cm^3]
	Cr	= 9.6800e+16 ,	2.2300e+17	# [cm^3]
	Cs	= 3.4300e+20 ,	6.1000e+20	# [cm^3]
	alpha	= 0.68 ,	0.719	# [1]
	beta	= 2 ,	2	# [1]
}

ThinLayerMobility:
{ * Mobility model for thin layers with geometric
  * quantization.  See the manual for details.
	beta	= 4 ,	4	# [1]
	zeta	= 2.88 ,	1.05	# [1]
	p1	= 0.55 ,	0.0000e+00	# [1]
	p2	= 4.0000e+02 ,	0.66	# [1]
	p3	= 1.44 ,	1	# [1]
	mz1	= 0.916 ,	0.29	# [m0]
	mz2	= 0.19 ,	0.25	# [m0]
	wt01	= 3.0000e-06 ,	0.0000e+00	# [um]
	wt02	= 3.5000e-07 ,	0.0000e+00	# [um]
	muac01	= 3.1500e+02 ,	30.2	# [cm^2/(Vs)]
	muac02	= 6.4 ,	69	# [cm^2/(Vs)]
	mutf0	= 0.15 ,	0.28	# [cm^2/(Vs)]
	musp0	= 1.1450e-08 ,	1.6000e-10	# [cm^2/(Vs)]
	tsp0	= 1.0000e-04 ,	1.0000e-04	# [um]
	ftf0	= 6.2500e+03 ,	1.0000e+100	# [V/cm]
	tmin	= 2.0000e-03 ,	2.0000e-03	# [um]
	eta1	= 6 ,	6	# [1]
	eta2	= 1 ,	1	# [1]
	mutfh0	= 1.0000e+06 ,	1.0000e+06	# [cm^2/(Vs)]
	ftfh0	= 1.0000e+100 ,	1.0000e+100	# [V/cm]
	a_bp	= 1 ,	1	# [1]
	a_sp	= 1 ,	1	# [1]
	a_tf	= 1 ,	1	# [1]
}

ThinLayerMobility_aniso:
{ * Mobility model for thin layers with geometric
  * quantization.  See the manual for details.
	beta	= 4 ,	4	# [1]
	zeta	= 2.88 ,	1.05	# [1]
	p1	= 0.55 ,	0.0000e+00	# [1]
	p2	= 4.0000e+02 ,	0.66	# [1]
	p3	= 1.44 ,	1	# [1]
	mz1	= 0.916 ,	0.29	# [m0]
	mz2	= 0.19 ,	0.25	# [m0]
	wt01	= 3.0000e-06 ,	0.0000e+00	# [um]
	wt02	= 3.5000e-07 ,	0.0000e+00	# [um]
	muac01	= 3.1500e+02 ,	30.2	# [cm^2/(Vs)]
	muac02	= 6.4 ,	69	# [cm^2/(Vs)]
	mutf0	= 0.15 ,	0.28	# [cm^2/(Vs)]
	musp0	= 1.1450e-08 ,	1.6000e-10	# [cm^2/(Vs)]
	tsp0	= 1.0000e-04 ,	1.0000e-04	# [um]
	ftf0	= 6.2500e+03 ,	1.0000e+100	# [V/cm]
	tmin	= 2.0000e-03 ,	2.0000e-03	# [um]
	eta1	= 6 ,	6	# [1]
	eta2	= 1 ,	1	# [1]
	mutfh0	= 1.0000e+06 ,	1.0000e+06	# [cm^2/(Vs)]
	ftfh0	= 1.0000e+100 ,	1.0000e+100	# [V/cm]
	a_bp	= 1 ,	1	# [1]
	a_sp	= 1 ,	1	# [1]
	a_tf	= 1 ,	1	# [1]
}

HighFieldDependence:
{ * Caughey-Thomas model:
  * mu_highfield = ( (alpha+1)*mu_lowfield ) / 
  *        ( alpha + ( 1 + ( (alpha+1)*mu_lowfield*E/vsat)^beta )^(1/beta) ) 
  * beta = beta0 (T/T0)^betaexp.
	beta0	= 1.109 ,	1.213	# [1]
	betaexp	= 0.66 ,	0.17	# [1]
	alpha	= 0.0000e+00 ,	0.0000e+00	# [1]

  * Smoothing parameter for HydroHighField Caughey-Thomas model:
  * if Tl < Tc < (1+K_dT)*Tl, then smoothing between low field mobility
  * and HydroHighField mobility is used.
	K_dT	= 0.2 ,	0.2	# [1]
  * Transferred-Electron Effect:
  * mu_highfield = (mu_lowfield+(vsat/E)*(E/E0_TrEf)^4)/(1+(E/E0_TrEf)^4)
	E0_TrEf	= 4.0000e+03 ,	4.0000e+03	# [1]
	Ksmooth_TrEf	= 1 ,	1	# [1]

 * For vsat either Formula1 or Formula2 can be used.
	Vsat_Formula	= 1 ,	1	# [1]
 * Formula1 for saturation velocity:
 *            vsat = vsat0 (T/T0)^(-Vsatexp)
 * (Parameter Vsat_Formula has to be not equal to 2)
	vsat0	= 1.0700e+07 ,	8.3700e+06	# [1]
	vsatexp	= 0.87 ,	0.52	# [1]
}

HighFieldDependence_aniso:
{ * Caughey-Thomas model:
  * mu_highfield = ( (alpha+1)*mu_lowfield ) / 
  *        ( alpha + ( 1 + ( (alpha+1)*mu_lowfield*E/vsat)^beta )^(1/beta) ) 
  * beta = beta0 (T/T0)^betaexp.
	beta0	= 1.109 ,	1.213	# [1]
	betaexp	= 0.66 ,	0.17	# [1]
	alpha	= 0.0000e+00 ,	0.0000e+00	# [1]

  * Smoothing parameter for HydroHighField Caughey-Thomas model:
  * if Tl < Tc < (1+K_dT)*Tl, then smoothing between low field mobility
  * and HydroHighField mobility is used.
	K_dT	= 0.2 ,	0.2	# [1]
  * Transferred-Electron Effect:
  * mu_highfield = (mu_lowfield+(vsat/E)*(E/E0_TrEf)^4)/(1+(E/E0_TrEf)^4)
	E0_TrEf	= 4.0000e+03 ,	4.0000e+03	# [1]
	Ksmooth_TrEf	= 1 ,	1	# [1]

 * For vsat either Formula1 or Formula2 can be used.
	Vsat_Formula	= 1 ,	1	# [1]
 * Formula1 for saturation velocity:
 *            vsat = vsat0 (T/T0)^(-Vsatexp)
 * (Parameter Vsat_Formula has to be not equal to 2)
	vsat0	= 1.0700e+07 ,	8.3700e+06	# [1]
	vsatexp	= 0.87 ,	0.52	# [1]
}

UniBoDopingDependence:
{
  * Baccarani model (University of Bologna) is used:
  * Constant mobility model is defined here as well:
  * mu_const = mumax (T/T0)^(-Exponent+Exponent2*(T/T0))
  * mu_dop = mu_0(Na,Nd,T/T0)                                               
  *         + (mu_const - mu_0)/(1 + (Nd/Cr)^alpha + (Na/Cr2)^beta)         
  *         - mu_1(Na,Nd,T/T0)/(1+(Nd/Cs + Na/Cs2)^(-2))                    
  * with mu_const from above
  * mu_0 = (mumin1 (T/T0)^(-mumin1_exp) Nd + mumin2 (T/T0)^(-mumin2_exp)Na)/(Nd+Na)
  * mu_1 = (mu1 (T/T0)^(-mu1_exp) Nd + mu2 (T/T0)^(-mu2_exp) Na)/(Nd+Na)
  * Cr=Cr (T/T0)^Cr_exp
  * Cr2=Cr2 (T/T0)^Cr2_exp
  * Cs=Cs (T/T0)^Cs_exp
	mumax	= 1.4410e+03 ,	4.7050e+02	# [cm^2/(Vs)]
	Exponent	= 2.45 ,	2.16	# [1]
	Exponent2	= -1.1000e-01 ,	0.0000e+00	# [1]
	mumin1	= 55 ,	90	# [cm^2/Vs]
	mumin2	= 1.3200e+02 ,	44	# [cm^2/Vs]
	mumin1_exp	= 0.6 ,	1.3	# [1]
	mumin2_exp	= 1.3 ,	0.7	# [1]
	mu1	= 42.4 ,	28.2	# [cm^2/Vs]
	mu2	= 73.5 ,	28.2	# [cm^2/Vs]
	mu1_exp	= 0.5 ,	2	# [1]
	mu2_exp	= 1.25 ,	0.8	# [1]
	Pc	= 0.0000e+00 ,	0.0000e+00	# [cm^3]
	Cr	= 8.9000e+16 ,	1.3000e+18	# [cm^3]
	Cr2	= 1.2200e+17 ,	2.4500e+17	# [cm^3]
	Cs	= 2.9000e+20 ,	1.1000e+18	# [cm^3]
	Cs2	= 7.0000e+20 ,	6.1000e+20	# [cm^3]
	Cr_exp	= 3.65 ,	2.2	# [1]
	Cr2_exp	= 2.65 ,	3.1	# [1]
	Cs_exp	= 0.0000e+00 ,	6.2	# [1]
	alpha	= 0.68 ,	0.77	# [1]
	beta	= 0.72 ,	0.719	# [1]
}

UniBoDopingDependence_aniso:
{
  * Baccarani model (University of Bologna) is used:
  * Constant mobility model is defined here as well:
  * mu_const = mumax (T/T0)^(-Exponent+Exponent2*(T/T0))
  * mu_dop = mu_0(Na,Nd,T/T0)                                               
  *         + (mu_const - mu_0)/(1 + (Nd/Cr)^alpha + (Na/Cr2)^beta)         
  *         - mu_1(Na,Nd,T/T0)/(1+(Nd/Cs + Na/Cs2)^(-2))                    
  * with mu_const from above
  * mu_0 = (mumin1 (T/T0)^(-mumin1_exp) Nd + mumin2 (T/T0)^(-mumin2_exp)Na)/(Nd+Na)
  * mu_1 = (mu1 (T/T0)^(-mu1_exp) Nd + mu2 (T/T0)^(-mu2_exp) Na)/(Nd+Na)
  * Cr=Cr (T/T0)^Cr_exp
  * Cr2=Cr2 (T/T0)^Cr2_exp
  * Cs=Cs (T/T0)^Cs_exp
	mumax	= 1.4410e+03 ,	4.7050e+02	# [cm^2/(Vs)]
	Exponent	= 2.45 ,	2.16	# [1]
	Exponent2	= -1.1000e-01 ,	0.0000e+00	# [1]
	mumin1	= 55 ,	90	# [cm^2/Vs]
	mumin2	= 1.3200e+02 ,	44	# [cm^2/Vs]
	mumin1_exp	= 0.6 ,	1.3	# [1]
	mumin2_exp	= 1.3 ,	0.7	# [1]
	mu1	= 42.4 ,	28.2	# [cm^2/Vs]
	mu2	= 73.5 ,	28.2	# [cm^2/Vs]
	mu1_exp	= 0.5 ,	2	# [1]
	mu2_exp	= 1.25 ,	0.8	# [1]
	Pc	= 0.0000e+00 ,	0.0000e+00	# [cm^3]
	Cr	= 8.9000e+16 ,	1.3000e+18	# [cm^3]
	Cr2	= 1.2200e+17 ,	2.4500e+17	# [cm^3]
	Cs	= 2.9000e+20 ,	1.1000e+18	# [cm^3]
	Cs2	= 7.0000e+20 ,	6.1000e+20	# [cm^3]
	Cr_exp	= 3.65 ,	2.2	# [1]
	Cr2_exp	= 2.65 ,	3.1	# [1]
	Cs_exp	= 0.0000e+00 ,	6.2	# [1]
	alpha	= 0.68 ,	0.77	# [1]
	beta	= 0.72 ,	0.719	# [1]
}

Scharfetter * relation and trap level for SRH recombination:
{ * tau = taumin + ( taumax - taumin ) / ( 1 + ( N/Nref )^gamma)
  * tau(T) = tau * ( (T/300)^Talpha )          (TempDep)
  * tau(T) = tau * exp( Tcoeff * ((T/300)-1) ) (ExpTempDep)
	taumin	= 0.0000e+00 ,	0.0000e+00	# [s]
	taumax	= 1.0000e-05 ,	3.0000e-06	# [s]
	Nref	= 1.0000e+16 ,	1.0000e+16	# [cm^(-3)]
	gamma	= 1 ,	1	# [1]
	Talpha	= -1.5000e+00 ,	-1.5000e+00	# [1]
	Tcoeff	= 2.55 ,	2.55	# [1]
	Etrap	= 0.0000e+00	# [eV]
}

PooleFrenkel
{ * TrapXsection = Xsec0*(1+Gpf) 
  * Gpf = (1+(a-1)*exp(a))/a^2-0.5 
  * where 
  *       a = (1/kT)*(q^3*F/pi/e0/epsPF)^0.5, 
  *       F is the electric field. 
	epsPF	= 11.7 ,	11.7	# [1]
}

RadiativeRecombination * coefficients:
{ * R_Radiative = C * (T/Tpar)^alpha * (n p - ni_eff^2)
  * C    
  * alpha 
	C	= 0.0000e+00	# [cm^3/s]
	alpha	= 0.0000e+00	# []
}

Absorption
{ * Formula = 1:
  * A = A1*exp((phE-E1)/E2),     for phE < E1 
  *   = A1 + A2*((phE-E1)/E2)^P, for phE >= E1 
  * Formula = 2:
  * A = A0*exp((phE-phE0)*ST),              for phE < phE0 
  *   = AT*(2*ST*(phE-E0-(LN-0.5)/ST))^0.5, for phE >= phE0 
  * where 
  *      phE is a photon energy, 
  *      LN = log(AT/A0), 
  *      ST = S/(T+T0), T is a temperature, 
  *      phE0 = E0 + LN/ST. 

	Formula	= 1	# [1]
  * Formula = 1:
	A1	= 1.0000e+04	# [cm-1]
	A2	= 100	# [cm-1]
	E1	= 1.12	# [eV]
	E2	= 0.025	# [eV]
	P	= 0.5	# [1]
}

LatticeParameters
{ * Crystal system, elasticity, and deformation potential are defined.
  * X and Y vectors define the simulation coordinate system relative to the
  * crystal orientation system. Also there is an option to represent the crystal
  * system relative to the simulation one. In this case a keyword CrystalAxis 
  * has to be in this section and X and Y vectors will represent [100] and [010]
  * axis of the crystal system in the simulation one.
  * Additional notes: 1 Pa = 10 dyn/cm^2; tensile stress/strain is positive. 
  * S[i][j] - elasticity modulus; i,j = 1,2,...6 and j>=i.
  * CrystalSystem is symmetry, used ONLY to define the elasticity matrics.
  * Cubic (CrystalSystem=0): S[1][1],S[1][2],S[4][4]
  * Hexagonal (CrystalSystem=1): S[1][1],S[1][2],S[1][3],S[3][3],S[4][4]
  * X = (1, 0.0000e+00, 0.0000e+00) #[1]
  * Y = (0.0000e+00, 1, 0.0000e+00) #[1]

	S[1][1]	= 0.77	# [1e-12 cm^2/din]
	S[1][2]	= -2.1000e-01	# [1e-12 cm^2/din]
	S[4][4]	= 1.25	# [1e-12 cm^2/din]
	CrystalSystem	= 0	# [1]

  * Deformation potentials of 2 k.p model for electron delta-valleys 
	xis	= 7	# [eV]
	dbs	= 0.53	# [eV]
	xiu	= 9.16	# [eV]
	xid	= 0.77	# [eV]
	Mkp	= 1.2	# [1]

  * Deformation potentials of 6 k.p model for hole bands
	adp	= 2.1	# [eV]
	bdp	= -2.3300e+00	# [eV]
	ddp	= -4.7500e+00	# [eV]
	dso	= 0.044	# [eV]

  * Luttinger parameters
	gamma_1	= 4.27	# [1]
	gamma_2	= 0.315	# [1]
	gamma_3	= 1.4576	# [1]

  * Deformation potentials and energy (in reference to delta-valley) for L-valleys 
	xiu_l	= 11.5	# [eV]
	xid_l	= -6.5800e+00	# [eV]
	e_l	= 1.1	# [eV]

  * Deformation potentials and energy (in reference to delta-valley) for Gamma-valley 
	xid_gamma	= -7.0000e+00	# [eV]
	e_gamma	= 2.3	# [eV]
}

