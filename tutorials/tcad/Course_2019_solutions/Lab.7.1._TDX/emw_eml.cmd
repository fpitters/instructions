Globals {
  GridFile       = "@tdr@"
  ParameterFile  = "@parameter@"
  InspectFile    = "@plot@"
  LogFile        = "@log@"
  TotalTimeSteps = 50000
}

ComplexRefractiveIndex {
  WavelengthDep = {Real,Imag}
}

Boundary {
  Type = periodicOblique
  Sides = {Y}
} 

Boundary {     
  Type = CPML
  Sides = {X} 
}

PlaneWaveExcitation {
  BoxCorner1 = (2.0,0,0)    
  BoxCorner2 = (2.0,1.5,0)
  Theta      = 90
  Phi        = 0
  Psi        = 0                                 
  Wavelength = 600 * nm
  Intensity  = 0.2 * W/cm2
  Nrise      =  4   * number of signal periods until full power
}

Extractor {
  Name     = "n@node@_g"
  Quantity = {absorbedPhotonDensity, OpticalGeneration}
  GridFile = "n@node|sde@_msh.tdr"
}

Detector {
  Tolerance = 1e-3
}
