AdvancedCalibration

line x location= 0    spacing= 0.01 tag= top
line x location= 0.15 spacing= 0.02
line x location= 1.0  spacing= 0.2  tag= bottom

line y location= 0.0      spacing= 0.1*0.25  tag= left
line y location= 0.5*0.25 spacing= 0.05*0.25
line y location= 2*0.25   spacing= 0.25      tag= right 

region silicon xlo= top xhi= bottom ylo= left yhi= right 

# step Init
init concentration= 1e17 field= Boron slice.angle= 180 !DelayFullD


# step Gate Oxidation
pdbSet Oxide Grid perp.add.dist 0.01e-4 
diffuse time=10 temp=950 O2

# step Deposit poly
deposit polysilicon anisotropic thickness= 0.1 

# step Poly Mask
mask name= poly left=-0.25/2 right= 0.25/2

# step Etch Poly
etch polysilicon anisotropic thickness= 0.12 mask= poly
etch oxide       anisotropic thickness= 0.02

# step LDD implant
implant arsenic dose=1e14 energy=30

# step Spacer formation
deposit nitride isotropic thickness= 0.3*0.25
etch  nitride anisotropic thickness= 0.35*0.25

# step SD Implant
implant phosphorus dose=1e+15 energy=15

# step SD Anneal
diffuse time=1<s> temp= 1000

# step contactSD
deposit aluminum anisotropic thickness= 0.05
mask name= contact left=0.25*1.2 
etch aluminum anisotropic thickness= 0.1 mask= contact 

# step Reflect
transform reflect left

contact name=substrate bottom
contact name=source point y=-0.25*1.5 x=-0.010 replace
contact name=drain  point y=0.25*1.5  x=-0.010 replace
contact name=gate   point y=0         x=-0.050 

# step save
struct tdr=n@node@ !Gas !interfaces

