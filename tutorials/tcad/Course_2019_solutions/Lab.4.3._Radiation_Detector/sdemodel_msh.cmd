Title ""

Controls {
}

IOControls {
	outputFile="sdemodel"
	EnableSections
}

Definitions {
	Constant "ConstantProfileDefinition_substrate" {
		Species = "PhosphorusActiveConcentration"
		Value = 1e+12
	}
	AnalyticalProfile "AnalyticalProfileDefinition_p" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 1e+12, Depth = 1)
		LateralFunction = Gauss(Factor = 0.8)
	}
	AnalyticalProfile "AnalyticalProfileDefinition_n" {
		Species = "PhosphorusActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 1e+12, Depth = 0.2)
		LateralFunction = Gauss(Factor = 0.5)
	}
	Refinement "RefinementDefinition_global" {
		MaxElementSize = ( 10 10 10 )
		MinElementSize = ( 0.01 0.01 0.001 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
		RefineFunction = MaxLenInt(Interface("Silicon","Oxide"), Value=0.001, factor=1.2, DoubleSide)
	}
}

Placements {
	Constant "ConstantProfilePlacement_substrate" {
		Reference = "ConstantProfileDefinition_substrate"
		EvaluateWindow {
			Element = region ["region_substrate"]
		}
	}
	AnalyticalProfile "AnalyticalProfilePlacement_p" {
		Reference = "AnalyticalProfileDefinition_p"
		ReferenceElement {
			Element = Polygon [ (30 30 300) (170 30 300) (170 170 300) (30 170 300)]
		}
	}
	AnalyticalProfile "AnalyticalProfilePlacement_n" {
		Reference = "AnalyticalProfileDefinition_n"
		ReferenceElement {
			Element = Polygon [(0 0 0) (200 0 0) (200 200 0) (0 200 0)]
		}
	}
	Refinement "RefinementPlacement_global" {
		Reference = "RefinementDefinition_global"
		RefineWindow = Cuboid [(0 0 0) (200 200 310)]
	}
}

