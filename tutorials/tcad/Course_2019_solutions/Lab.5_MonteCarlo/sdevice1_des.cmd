File {
  Grid=     "@tdr@"
  Output=   "@log@"
  Parameter="@parameter@" 
  Load=  "n@node|-1@_des.sav"
  MonteCarloOut = "n@node@_mc"
  Plot= "@tdrdat@"
}

Electrode {
{ name="source" voltage=0.0 }
{ name="drain"  voltage=-0.8 }
{ name="gate"   voltage=-0.8 Workfunction = 4.58  } 
}

Plot {
hMCAvalanche
hMCCurrent
hMCDensity
hMCEnergy
MCField
}

MonteCarlo {
  SurfScattRatio = 0.85
  CurrentErrorBar = 2.0
  MinCurrentComput = 10
  DrainContact = "drain" # Name or No. of Drain contact in .tdr 
  # (count from 0, view with TDX)
  SelfConsistent(FrozenQF)
  Window = Cuboid[ (-0.05,-0.1 -0.1)
  (0.05, 0.1 0.1) ]
  FinalTime = 5e-06
  Plot { Range=(0,20e-06) intervals = 500 }
}

Physics {
  Mobility( PhuMob HighFieldSaturation Enormal )
  EffectiveIntrinsicDensity ( Slotboom NoFermi )
  hQuantumPotential
  Recombination(SRH)
}

Math {
  Number_of_Threads = 8
  Extrapolate
  RelErrControl
  Digits=8
  ErrRef(Electron)=1.e10
  ErrRef(Hole)=1.e10
  NotDamped=20
  Iterations=30
  ExitOnFailure
-CheckUndefinedModels
  RHSmin= 1e-15
  method= ILS
}

solve {
coupled {poisson hole hQuantumPotential }
montecarlo
}

