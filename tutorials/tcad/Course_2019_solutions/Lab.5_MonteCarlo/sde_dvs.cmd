(sde:clear)
(sdegeo:set-default-boolean "ABA")

(define N_source 1e20)
(define N_drain 1e20)
(define N_channel 0.0)
(define Tch 0.005)
(define Tox 0.0008)
(define Hg 0.5e-3) 
(define Lg 0.011)
(define Lspacer 0.0025)
(define Ld 0.01)
(define Ls 0.01)

(define Ttot (+ Tch Tox Tox))
(define Ltot (+ Ls Lspacer Lg Lspacer Ld))
(define Xmax (/ Ltot 2.0))
(define Xg (/ Lg 2.0))
(define Xsg (+ (/ Lg 2)))
(define Xdrain (* -1.0 (+ Xg Ld)))
(define Xsource (+ Xg Ls))

(sdegeo:create-cylinder (position (* Xg -1.0) 0.0 0.0) (position Xg 0.0 0.0) (+ (/ Tch 2) Tox) "Oxide" "R_oxide")
(sdegeo:create-cylinder (position (* Xg -1.0) 0.0 0.0) (position Xdrain 0.0 0.0) (+ (/ Tch 2) Tox) "Oxide" "R_SP1")
(sdegeo:create-cylinder (position Xsource 0.0 0.0) (position Xg 0.0 0.0) (+ (/ Tch 2) Tox) "Oxide" "R_SP2")
(sdegeo:create-cylinder (position (* Xg -1.0) 0.0 0.0) (position Xg 0.0 0.0) (/ Tch 2) "Silicon" "R_channel")
(sdegeo:create-cylinder (position Xsource 0.0 0.0) (position Xg 0.0 0.0) (/ Tch 2) "Silicon"  "R_source")
(sdegeo:create-cylinder (position Xdrain 0.0 0.0) (position (* Xg -1.0) 0.0 0.0) (/ Tch 2) "Silicon" "R_drain")

(sdegeo:define-contact-set "source" 4.0 (color:rgb 1.0 0.0 0.0 ) "##" )
(sdegeo:define-contact-set "drain" 4.0 (color:rgb 0.0 1.0 0.0 ) "##" )
(sdegeo:define-contact-set "gate" 4.0 (color:rgb 0.0 0.5 0.5 ) "##" )

(sdegeo:set-current-contact-set "source")
(sdegeo:set-contact-faces (find-face-id (position 0.0155 (/ Tch 4) 0.0  )))

(sdegeo:set-current-contact-set "drain")
(sdegeo:set-contact-faces (find-face-id (position (* -1.0 0.0155) (/ Tch 4) 0.0 )))

(sdegeo:set-current-contact-set "gate")
(sdegeo:set-contact-faces (find-face-id (position 0.0  (/ Ttot 2) 0.0)))


(sdedr:define-refeval-window "BaseDop.Channel" "Cuboid"
(position (* -1.0 Xg) (* -1.0 Ttot) (* -1.0 Ttot)) (position Xg Ttot Ttot))
(sdedr:define-constant-profile "Const.Channel" "BoronActiveConcentration" N_channel)
(sdedr:define-constant-profile-placement "PlaceCD.Channel" "Const.Channel" "BaseDop.Channel")

(sdedr:define-refeval-window "BaseDop.Source" "Cuboid" (position Xsource (* -1.0 Ttot) (* -1.0 Ttot)) (position Xg Ttot Ttot))
(sdedr:define-gaussian-profile "GaussDop.Source" "BoronActiveConcentration"
"PeakPos" 0 "PeakVal" N_source "ValueAtDepth" 1e12 "Depth" 0.002 "Gauss" "Factor" 1)
(sdedr:define-analytical-profile-placement "PlaceAP.Source" "GaussDop.Source" "BaseDop.Source" "Positive" "NoReplace" "Eval")


(sdedr:define-refeval-window "BaseDop.Drain" "Cuboid" (position Xdrain (* -1.0 Ttot) (* -1.0 Ttot)) (position (* -1.0 Xg) Ttot Ttot))
(sdedr:define-gaussian-profile "GaussDop.Drain" "BoronActiveConcentration"
"PeakPos" 0 "PeakVal" N_drain "ValueAtDepth" 1e12 "Depth" 0.002 "Gauss" "Factor" 1)
(sdedr:define-analytical-profile-placement "PlaceAP.Drain" "GaussDop.Drain" "BaseDop.Drain" "Positive" "NoReplace" "Eval")


(sdedr:define-refeval-window "Global" "Cuboid" (position Xsource  (* -1.0 Ttot) (* -1.0 Ttot)) (position Xdrain Ttot Ttot))
(sdedr:define-refinement-size "Global" 0.0005 0.0005 0.0005 0.0001 0.0001 0.0001)
(sdedr:define-refinement-placement "Global" "Global" (list "window" "Global" ) )
(sdedr:define-refinement-function "Global" "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-function "Global" "MaxLenInt" "Silicon" "HfO2" 0.002 1)

(sdedr:define-refeval-window "Channel" "Cuboid" (position Xg (* -1.0 Ttot) (* -1.0 Ttot)) (position (* -1.0 Xg) Ttot Ttot))
(sdedr:define-refinement-size "Channel" 0.00005 0.00005 0.00005 0.0001 0.0001 0.0001)
(sdedr:define-refinement-placement "Channel" "Global" (list "window" "Channel" ) )
(sdedr:define-refinement-function "Channel" "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-function "Channel" "MaxLenInt" "Silicon" "HfO2" 0.002 1)

(sde:build-mesh "snmesh" " " "n@node@_msh")



