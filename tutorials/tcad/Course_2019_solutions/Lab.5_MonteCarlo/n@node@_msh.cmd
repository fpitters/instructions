Title "Untitled"

Controls {
}

IOControls {
	EnableSections
}

Definitions {
	Constant "Const.Channel" {
		Species = "BoronActiveConcentration"
		Value = 0
	}
	AnalyticalProfile "GaussDop.Source" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 0, Depth = 0.0025)
		LateralFunction = Gauss(Factor = 1)
	}
	AnalyticalProfile "GaussDop.Drain" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 0, Depth = 0.0025)
		LateralFunction = Gauss(Factor = 1)
	}
	Refinement "Global" {
		MaxElementSize = ( 0.005 0.005 0.005 )
		MinElementSize = ( 0.001 0.001 0.001 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
		RefineFunction = MaxLenInt(Interface("Silicon","HfO2"), Value=0.001, factor=1.5)
	}
	Refinement "RefDef.Channel" {
		MaxElementSize = ( 0.005 0.005 0.005 )
		MinElementSize = ( 0.001 0.001 0.001 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
		RefineFunction = MaxLenInt(Interface("Silicon","HfO2"), Value=0.02, factor=1)
	}
	Refinement "RefinementDefinition_Channel1" {
		MaxElementSize = ( 0.0005 0.0005 5e-05 )
		MinElementSize = ( 0.0001 0.0001 0.0001 )
	}
	Refinement "RefinementDefinition_Channel2" {
		MaxElementSize = ( 0.0005 0.0005 5e-05 )
		MinElementSize = ( 0.0001 0.0001 0.0001 )
	}
}

Placements {
	AnalyticalProfile "PlaceAP.Source" {
		Reference = "GaussDop.Source"
		ReferenceElement {
			Element = Cuboid [(0.008 -0.0008 -0.0008) (0.0155 0.0008 0.0008)]
			Direction = positive
		}
	}
	AnalyticalProfile "PlaceAP.Drain" {
		Reference = "GaussDop.Drain"
		ReferenceElement {
			Element = Cuboid [(-0.0155 -0.0008 -0.0008) (-0.008 0.0008 0.0008)]
			Direction = positive
		}
	}
	Refinement "Global" {
		Reference = "Global"
		RefineWindow = Cuboid [(-0.0155 -0.0066 -0.0066) (0.0155 0.0066 0.0066)]
	}
	Refinement "PlaceRF.Channel" {
		Reference = "RefDef.Channel"
		RefineWindow = region ["R_channel"]
	}
	Refinement "RefinementPlacement_Channel1" {
		Reference = "RefinementDefinition_Channel1"
		RefineWindow = Cuboid [(-0.0155 0.0005 0.0022) (0.0155 0.000515 0.0026)]
	}
	Refinement "RefinementPlacement_Channel2" {
		Reference = "RefinementDefinition_Channel2"
		RefineWindow = Cuboid [(-0.0155 0.0005 -0.0026) (0.0155 0.000515 -0.0022)]
	}
}

