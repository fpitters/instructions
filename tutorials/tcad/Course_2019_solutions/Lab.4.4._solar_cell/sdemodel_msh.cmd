Title "Untitled"

Controls {
}

Definitions {
	Constant "ConstantProfileDefinition_p" {
		Species = "BoronActiveConcentration"
		Value = 1e+16
	}
	Refinement "RefinementDefinition_1" {
		MaxElementSize = ( 1 1 1 )
		MinElementSize = ( 1 1 1 )
	}
	Multibox "MultiboxDefinition_si"
 {
		MaxElementSize = ( 0.2 1 0.2 )
		MinElementSize = ( 0.2 0.1 0.2 )
		Ratio = ( 1 1.1 1 )
	}
}

Placements {
	Constant "ConstantProfilePlacement_p" {
		Reference = "ConstantProfileDefinition_p"
		EvaluateWindow {
			Element = material ["Silicon"]
		}
	}
	Refinement "RefinementPlacement_1" {
		Reference = "RefinementDefinition_1"
		RefineWindow = material ["Silicon"]
	}
	Multibox "MultiboxPlacement_si" {
		Reference = "MultiboxDefinition_si"
		RefineWindow = Cuboid [(0 0 0) (5 5 12)]
	}
}

