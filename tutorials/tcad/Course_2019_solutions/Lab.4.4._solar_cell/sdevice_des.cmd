#setdep @previous@

File {
  grid  = "@tdr@"
  current = "@plot@"
  output  = "@log@"
  plot    = "@tdrdat@"
  IlluminationSpectrum= "spectrum.txt"
}

Electrode {
  { name = "B_contact" voltage = 0}
  { name = "T_contact" voltage = 0}
}

RayTraceBC {
  { name = "sideContact1" reflectivity= 1.0 }
  { name = "sideContact2" reflectivity= 1.0 }
  { name = "sideContact3" reflectivity= 1.0 }
  { name = "sideContact4" reflectivity= 1.0 }
}


Plot {
  AbsorbedPhotonDensity
  OpticalGeneration  
  DopingConcentration
  ComplexRefractiveIndex 
}


Physics {
  Optics(
    ComplexRefractiveIndex(
      WavelengthDep(real imag)
    )
 
    OpticalGeneration (
      ComputeFromSpectrum
    )
    
    OpticalSolver (
      RayTracing (
        CompactMemoryOption
        RectangularWindow (
          RectangleV1= (-12, 0, 0)
          RectangleV2= (-12, 0.5, 0.5)
          RectangleV3= (-12, 0.5, 0)
          RayDirection= (1, 0, 0)
          LengthDiscretization= 200
          WidthDiscretization= 200
        ) * end RectangularWindow
      ) * end of RayTracing
    ) * end of OpticalSolver
  
  ) * end of Optics
}  
  
Math {
  Stacksize= 200000000
}

Solve {
  Poisson
}
