

(sdegeo:create-cuboid (position 0 0 0)  (position -10 0.5 0.5) "Silicon" "region_sub")

(sdegeo:create-cuboid (position -10 0 0) (position -12 0.5 0.5) "Gas" "ambient")

#if [string compare @surface@ random] ==0



(do ( (j 0 (+ j 1)) )
	( (= j 4) ) 
	(begin

(do ( (i 0 (+ i 1)) ) ; i: Counter name; 0: initial value; (+ i 1): incrementer 
    ( (= i 4) )       ; End Tester
    (begin            ; Body of loop
      (define REGION (string-append "region_p" (number->string (+ i j))))
      (sdegeo:create-cuboid (position -10  (+ 0.05 (* 0.1 i))  (+ 0.05 (* 0.1 j))) (position -10.1 (+ 0.05 (* 0.1 (+ i 1))) (+ 0.05 (* 0.1 (+ j 1)))) "Silicon" REGION)
      
      (sdegeo:taper-faces (list (car (find-face-id (position -10.05 (+ 0.05 (* 0.1 i)) (+ 0.1 (* 0.1 j))))))(position -10 (+ 0.05 (* 0.1 i)) (+ 0.1 (* 0.1 j))) (gvector -1 0 0) 30)
      
      (sdegeo:taper-faces (list (car (find-face-id (position -10.05 (+ 0.1 (* 0.1 i)) (+ 0.15 (* 0.1 j))))))(position -10 (+ 0.1 (* 0.1 i)) (+ 0.15 (* 0.1 j))) (gvector -1 0 0) 30)

      (sdegeo:taper-faces (list (car (find-face-id (position -10.05 (+ 0.05 (* 0.1 (+ i 1))) (+ 0.1 (* 0.1 j))))))(position -10 (+ 0.05 (* 0.1 (+ i 1))) (+ 0.1 (* 0.1 j))) (gvector -1 0 0) 30)

      (sdegeo:taper-faces (list (car (find-face-id (position -10.05 (+ 0.1 (* 0.1 i))  (+ 0.05 (* 0.1 j))))))(position -10 (+ 0.1 (* 0.1 i)) (+ 0.05 (* 0.1 j))) (gvector -1 0 0) 30)
      
    )
)
	)
)

(sdegeo:bool-unite (list (car (find-body-id (position -5 0.25 0.25))) (car (find-body-id (position -10.02165064 0.4 0.4))) (car (find-body-id (position -10.02165064 0.3 0.4))) (car (find-body-id (position -10.02165064 0.4 0.3))) (car (find-body-id (position -10.02165064 0.2 0.4))) (car (find-body-id (position -10.02165064 0.3 0.3))) (car (find-body-id (position -10.02165064 0.4 0.2))) (car (find-body-id (position -10.02165064 0.1 0.4))) (car (find-body-id (position -10.02165064 0.2 0.3))) (car (find-body-id (position -10.02165064 0.3 0.2))) (car (find-body-id (position -10.02165064 0.4 0.1))) (car (find-body-id (position -10.02165064 0.1 0.3))) (car (find-body-id (position -10.02165064 0.2 0.2))) (car (find-body-id (position -10.02165064 0.3 0.1))) (car (find-body-id (position -10.02165064 0.1 0.2))) (car (find-body-id (position -10.02165064 0.2 0.1))) (car (find-body-id (position -10.02165064 0.1 0.1)))))
#endif

(sdegeo:define-contact-set "T_contact" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:set-current-contact-set "T_contact")

#if [string compare @surface@ flat] ==0

(sdegeo:define-3d-contact (list (car (find-face-id (position -10 0.25 0.25)))) "T_contact")

#elseif [string compare @surface@ random] ==0

(sdegeo:define-3d-contact (list (car (find-face-id (position -10.02886751 0.26666667 0.40833333))) (car (find-face-id (position -10.02886751 0.30833333 0.36666667))) (car (find-face-id (position -10.02886751 0.29166667 0.43333333))) (car (find-face-id (position -10.02886751 0.33333333 0.39166667))) (car (find-face-id (position -10.02886751 0.39166667 0.33333333))) (car (find-face-id (position -10.02886751 0.40833333 0.26666667))) (car (find-face-id (position -10.02886751 0.43333333 0.29166667))) (car (find-face-id (position -10.02886751 0.36666667 0.30833333))) (car (find-face-id (position -10.02886751 0.16666667 0.40833333))) (car (find-face-id (position -10.02886751 0.43333333 0.19166667))) (car (find-face-id (position -10.02886751 0.40833333 0.16666667))) (car (find-face-id (position -10.02886751 0.26666667 0.30833333))) (car (find-face-id (position -10.02886751 0.30833333 0.26666667))) (car (find-face-id (position -10.02886751 0.33333333 0.29166667))) (car (find-face-id (position -10.02886751 0.19166667 0.43333333))) (car (find-face-id (position -10.02886751 0.29166667 0.33333333))) (car (find-face-id (position -10.02886751 0.39166667 0.23333333))) (car (find-face-id (position -10.02886751 0.36666667 0.20833333))) (car (find-face-id (position -10.02886751 0.20833333 0.36666667))) (car (find-face-id (position -10.02886751 0.23333333 0.39166667))) (car (find-face-id (position -10 0.025 0.25))) (car (find-face-id (position -10.02886751 0.19166667 0.33333333))) (car (find-face-id (position -10.02886751 0.30833333 0.16666667))) (car (find-face-id (position -10.02886751 0.33333333 0.19166667))) (car (find-face-id (position -10.02886751 0.09166667 0.43333333))) (car (find-face-id (position -10.02886751 0.43333333 0.09166667))) (car (find-face-id (position -10.02886751 0.16666667 0.30833333))) (car (find-face-id (position -10.02886751 0.29166667 0.23333333))) (car (find-face-id (position -10.02886751 0.26666667 0.20833333))) (car (find-face-id (position -10.02886751 0.20833333 0.26666667))) (car (find-face-id (position -10.02886751 0.23333333 0.29166667))) (car (find-face-id (position -10.02886751 0.16666667 0.20833333))) (car (find-face-id (position -10.02886751 0.19166667 0.23333333))) (car (find-face-id (position -10.02886751 0.23333333 0.19166667))) (car (find-face-id (position -10.02886751 0.20833333 0.16666667))) (car (find-face-id (position -10.02886751 0.33333333 0.09166667))) (car (find-face-id (position -10.02886751 0.09166667 0.33333333))) (car (find-face-id (position -10.02886751 0.36666667 0.10833333))) (car (find-face-id (position -10.02886751 0.13333333 0.39166667))) (car (find-face-id (position -10.02886751 0.10833333 0.36666667))) (car (find-face-id (position -10.02886751 0.39166667 0.13333333))) (car (find-face-id (position -10.02886751 0.23333333 0.09166667))) (car (find-face-id (position -10.02886751 0.09166667 0.23333333))) (car (find-face-id (position -10.02886751 0.29166667 0.13333333))) (car (find-face-id (position -10.02886751 0.13333333 0.29166667))) (car (find-face-id (position -10.02886751 0.10833333 0.26666667))) (car (find-face-id (position -10.02886751 0.26666667 0.10833333))) (car (find-face-id (position -10.02886751 0.06666667 0.40833333))) (car (find-face-id (position -10.02886751 0.40833333 0.06666667))) (car (find-face-id (position -10.02886751 0.19166667 0.13333333))) (car (find-face-id (position -10.02886751 0.16666667 0.10833333))) (car (find-face-id (position -10.02886751 0.10833333 0.16666667))) (car (find-face-id (position -10.02886751 0.13333333 0.19166667))) (car (find-face-id (position -10.02886751 0.09166667 0.13333333))) (car (find-face-id (position -10.02886751 0.06666667 0.30833333))) (car (find-face-id (position -10.02886751 0.30833333 0.06666667))) (car (find-face-id (position -10.02886751 0.13333333 0.09166667))) (car (find-face-id (position -10.02886751 0.06666667 0.20833333))) (car (find-face-id (position -10.02886751 0.20833333 0.06666667))) (car (find-face-id (position -10.02886751 0.10833333 0.06666667))) (car (find-face-id (position -10.02886751 0.06666667 0.10833333))) (car (find-face-id (position -10.02886751 0.39166667 0.43333333))) (car (find-face-id (position -10.02886751 0.43333333 0.39166667))) (car (find-face-id (position -10.02886751 0.40833333 0.36666667))) (car (find-face-id (position -10.02886751 0.36666667 0.40833333)))) "T_contact")
#endif

(sdegeo:define-contact-set "B_contact" 4  (color:rgb 1 1 0 ) "##")

(sdegeo:set-current-contact-set "B_contact")

(sdegeo:define-3d-contact (list (car (find-face-id (position 0 0.25 0.25)))) "B_contact")

(sdegeo:define-contact-set "sideContact1" 4  (color:rgb 1 0 1 ) "##")

(sdegeo:define-contact-set "sideContact2" 4  (color:rgb 0 1 0 ) "##")

(sdegeo:define-contact-set "sideContact3" 4  (color:rgb 0 1 1 ) "##")

(sdegeo:define-contact-set "sideContact4" 4  (color:rgb 1 1 1 ) "##")

(sdegeo:set-current-contact-set "sideContact1")

(sdegeo:define-3d-contact (list (car (find-face-id (position -5.0 0.25 0.5)))) "sideContact1")

(sdegeo:set-current-contact-set "sideContact2")

(sdegeo:define-3d-contact (list (car (find-face-id (position -5.0 0.5 0.25)))) "sideContact2")

(sdegeo:set-current-contact-set "sideContact3")

(sdegeo:define-3d-contact (list (car (find-face-id (position -5.0 0.25 0)))) "sideContact3")

(sdegeo:set-current-contact-set "sideContact4")

(sdegeo:define-3d-contact (list (car (find-face-id (position -5.0 0 0.25)))) "sideContact4")



(sdedr:define-constant-profile "ConstantProfileDefinition_p" "BoronActiveConcentration" 1e16)

(sdedr:define-constant-profile-material "ConstantProfilePlacement_p" "ConstantProfileDefinition_p" "Silicon")

(sdedr:define-refinement-size "RefinementDefinition_1" 2 0.08 0.08 2 0.08 0.08)

(sdedr:define-refinement-placement "RefinementPlacement_si" "RefinementDefinition_1" (list "material" "Silicon" ) )

(sdedr:define-refinement-placement "RefinementPlacement_gas" "RefinementDefinition_1" (list "material" "Gas" ) )

(sdedr:define-refeval-window "RefEvalWin_1" "Cuboid"  (position -8 0 0)  (position -10.2 0.5 0.5) )


(sdedr:define-multibox-size "MultiboxDefinition_si" 2 0.05 0.05 0.02 0.05 0.05 1.2 1 1 )

(sdedr:define-multibox-placement "MultiboxPlacement_si" "MultiboxDefinition_si" "RefEvalWin_1" )


(sde:build-mesh "snmesh" "-a -c boxmethod" "n@node@")
