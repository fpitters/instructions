#setdep @previous@
#-----------------------------------------
# $Id: //tcad/support/2013.12/examples/getting-started/emw/simple2d-sdevice/emw_eml.cmd#1 $
# Author: Gergoe Letay
#setdep @previous@
Globals {
  GridFile       = "@tdr@"
  ParameterFile  = "@parameter@"
  InspectFile    = "@plot@"
  LogFile        = "@log@"
  TotalTimeSteps = 10000
  OverSampling = 2
}

ComplexRefractiveIndex {
  WavelengthDep = {Real,Imag}
}

Boundary {
  Type = periodicOblique
  Sides = {X, Y}
} 

Boundary {     
  Type = CPML
  Sides = {Z} 
  SigmaMax = 5e5
  Thickness =  25      
}

PlaneWaveExcitation {
  BoxCorner1 = (0,0,11)    
  BoxCorner2 = (0.5,0.5,11)
  Theta      = 180
  Psi        = 0                                 
  Wavelength = 500 * nm
  Intensity  = 0.1 * W/cm2
  Nrise      =  4   * number of signal periods until full power
}

Extractor {
  Name     = "n@node@_g"
  Quantity = {absorbedPhotonDensity, OpticalGeneration}
  GridFile = "n@node|sde@_msh.tdr"
}

Extractor {
  Name     = "n@node@_gtensor"
  Quantity = {absorbedPhotonDensity, Region}
}

Detector {
  Tolerance = 1e-3
}



