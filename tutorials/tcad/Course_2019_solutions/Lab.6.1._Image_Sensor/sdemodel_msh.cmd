Title "Untitled"

Controls {
}

IOControls {
	outputFile = "sdemodel"
	EnableSections
}

Definitions {
	Constant "ConstantProfileDefinition_sub" {
		Species = "BoronActiveConcentration"
		Value = 1e+15
	}
	Constant "ConstantProfileDefinition_poly" {
		Species = "ArsenicActiveConcentration"
		Value = 1e+19
	}
	AnalyticalProfile "AnalyticalProfileDefinition_n" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+19, ValueAtDepth = 5e+15, Depth = 0.1)
		LateralFunction = Gauss(Factor = 0.8)
	}
	AnalyticalProfile "AnalyticalProfileDefinition_nw" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+17, ValueAtDepth = 1e+16, Depth = 0.07)
		LateralFunction = Gauss(Factor = 0.8)
	}
	AnalyticalProfile "AnalyticalProfileDefinition_pcap" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+19, ValueAtDepth = 5e+15, Depth = 0.03)
		LateralFunction = Gauss(Factor = 0.8)
	}
	Refinement "RefinementDefinition_sub" {
		MaxElementSize = ( 0.05 0.05 )
		MinElementSize = ( 0.005 0.005 )
		RefineFunction = MaxLenInt(Interface("Silicon","Oxide"), Value=0.005, factor=1.2, DoubleSide)
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
	}
	Refinement "RefinementDefinition_nitride" {
		MaxElementSize = ( 0.05 0.05 )
		MinElementSize = ( 0.05 0.05 )
	}
}

Placements {
	Constant "ConstantProfilePlacement_sub" {
		Reference = "ConstantProfileDefinition_sub"
		EvaluateWindow {
			Element = region ["region_sub"]
		}
	}
	Constant "ConstantProfilePlacement_poly" {
		Reference = "ConstantProfileDefinition_poly"
		EvaluateWindow {
			Element = region ["region_polysi"]
		}
	}
	AnalyticalProfile "AnalyticalProfilePlacement_n" {
		Reference = "AnalyticalProfileDefinition_n"
		ReferenceElement {
			Element = Line [(0 1.4) (0 1.6)]
			Direction = positive
		}
	}
	AnalyticalProfile "AnalyticalProfilePlacement_nw" {
		Reference = "AnalyticalProfileDefinition_nw"
		ReferenceElement {
			Element = Line [(0.1 0.1) (0.1 0.95)]
		}
	}
	AnalyticalProfile "AnalyticalProfilePlacement_pcap" {
		Reference = "AnalyticalProfileDefinition_pcap"
		ReferenceElement {
			Element = Line [(0 0) (0 1.05)]
			Direction = positive
		}
	}
	Refinement "RefinementPlacement_sub" {
		Reference = "RefinementDefinition_sub"
		RefineWindow = region ["region_sub"]
	}
	Refinement "RefinementPlacement_nitride" {
		Reference = "RefinementDefinition_nitride"
		RefineWindow = material ["Si3N4"]
	}
}

