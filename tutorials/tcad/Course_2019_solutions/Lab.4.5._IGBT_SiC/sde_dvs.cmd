
(sdegeo:create-rectangle (position 0 0 0)  (position -100 4 0) "SiliconCarbide" "region_sub")

(sdegeo:create-rectangle (position -97 3.5 0)  (position -100 4 0) "SiliconCarbide" "region_subdel")

(sdegeo:delete-region (list (car (find-body-id (position -98.5 3.75 0)))))

(sdegeo:create-polygon (list  (position -100 3.5 0)  (position -100 3.55 0)  (position -97.05 3.55 0)  (position -97.05 4 0)  (position -97 4 0)  (position -97 3.5 0)  (position -100 3.5 0) ) "Oxide" "region_oxide")

(sdegeo:fillet-2d (list (car (find-vertex-id (position -97.05 3.55 0)))) 0.2)

(sdegeo:fillet-2d (list (car (find-vertex-id (position -97 3.5 0)))) 0.2)


(sdedr:define-refeval-window "RefEvalWin_p+" "Rectangle"  (position -100 0 0) (position -99.5 1 0))

(sdedr:define-refeval-window "RefEvalWin_n+_cap" "Rectangle"  (position -100 1 0) (position -99.5 3.5 0))

(sdedr:define-refeval-window "RefEvalWin_p-" "Rectangle"  (position -97.5 0 0) (position -99.5 3.5 0))

(sdedr:define-refeval-window "RefEvalWin_n-" "Rectangle"  (position -97.5 0 0) (position -0.5 4 0))

(sdedr:define-refeval-window "RefEvalWin_p+_bot" "Rectangle"  (position 0 0 0) (position -0.5 4 0))


(sdedr:define-constant-profile "ConstantProfileDefinition_p+" "AluminumActiveConcentration" 1e20)

(sdedr:define-constant-profile-placement "ConstantProfilePlacement_p+" "ConstantProfileDefinition_p+" "RefEvalWin_p+")

(sdedr:define-constant-profile "ConstantProfileDefinition_n+_cap" "NitrogenActiveConcentration" 1e20)

(sdedr:define-constant-profile-placement "ConstantProfilePlacement_n+_cap" "ConstantProfileDefinition_n+_cap" "RefEvalWin_n+_cap")

(sdedr:define-constant-profile "ConstantProfileDefinition_p-" "AluminumActiveConcentration" 4e17)

(sdedr:define-constant-profile-placement "ConstantProfilePlacement_p-" "ConstantProfileDefinition_p-" "RefEvalWin_p-")

(sdedr:define-constant-profile "ConstantProfileDefinition_n-" "NitrogenActiveConcentration" 1e15)

(sdedr:define-constant-profile-placement "ConstantProfilePlacement_n-" "ConstantProfileDefinition_n-" "RefEvalWin_n-")

(sdedr:define-constant-profile "ConstantProfileDefinition_p+_bot" "AluminumActiveConcentration" 1e20)

(sdedr:define-constant-profile-placement "ConstantProfilePlacement_p+_bot" "ConstantProfileDefinition_p+_bot" "RefEvalWin_p+_bot")


(sdegeo:define-contact-set "Gate" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:define-contact-set "Collector" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:define-contact-set "Emitter" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:set-current-contact-set "Gate")

(sdegeo:set-contact-edges (list (car (find-edge-id (position -97.10857864 3.60857864 0))) (car (find-edge-id (position -97.05 3.875 0))) (car (find-edge-id (position -98.625 3.55 0)))) "Gate")

(sdegeo:set-current-contact-set "Collector")

(sdegeo:set-contact-edges (list (car (find-edge-id (position 0 2 0)))) "Collector")

(sdegeo:insert-vertex (position -100 2.8 0))

(sdegeo:set-current-contact-set "Emitter")

(sdegeo:set-contact-edges (list (car (find-edge-id (position -100 1.4 0)))) "Emitter")



(sdedr:define-refeval-window "RefEvalWin_global" "Rectangle"  (position -102.2406 -1.4504 0) (position 1.4768 5.0088 0))

(sdedr:define-refinement-size "RefinementDefinition_1" 1 0.2 0.01 0.01 )

(sdedr:define-refinement-placement "RefinementPlacement_global" "RefinementDefinition_1" (list "window" "RefEvalWin_global" ) )

(sdedr:define-refinement-function "RefinementDefinition_1" "DopingConcentration" "MaxTransDiff" 1)

(sdedr:define-refinement-function "RefinementDefinition_1" "MaxLenInt" "Oxide" "SiliconCarbide" 0.01 1.2 "DoubleSide")


#(sdedr:define-refeval-window "RefEvalWin_ava" "Rectangle"  (position -97 0 0) (position 0 3.5 0))

#(sdedr:define-multibox-size "MultiboxDefinition_ava" 0.2 1 0.01 0.01 1 -1.2 )

#(sdedr:define-multibox-placement "MultiboxPlacement_ava" "MultiboxDefinition_ava" "RefEvalWin_ava" )


(sde:build-mesh "snmesh" "-a -c boxmethod" "n@node@")

