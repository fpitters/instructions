

(sdegeo:create-cuboid (position 0 0 0)  (position -300 200 200) "Silicon" "region_substrate")

(sdegeo:create-cuboid (position -300 0 0)  (position -300.6 200 200) "Oxide" "region_oxide")

(sdegeo:create-cuboid (position -300 50 50)  (position -304 150 150) "Aluminum" "region_aluminum")


(sdedr:define-constant-profile "ConstantProfileDefinition_substrate" "PhosphorusActiveConcentration" 1e12)

(sdedr:define-constant-profile-region "ConstantProfilePlacement_substrate" "ConstantProfileDefinition_substrate" "region_substrate")


(sdedr:define-refeval-window "RefEvalWin_p" "Rectangle"  (position -300 26 26)  (position -300 174 174) )

(sdedr:define-analytical-profile-placement "AnalyticalProfilePlacement_p" "AnalyticalProfileDefinition_p" "RefEvalWin_p" "Both" "NoReplace" "Eval")

(sdedr:define-gaussian-profile "AnalyticalProfileDefinition_p" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 1e18 "ValueAtDepth" 1e+12 "Depth" 6 "Gauss"  "Factor" 0.8)



(sdedr:define-refeval-window "RefEvalWin_n" "Rectangle"  (position 0 0 0)  (position 0 200 200) )

(sdedr:define-analytical-profile-placement "AnalyticalProfilePlacement_n" "AnalyticalProfileDefinition_n" "RefEvalWin_n" "Both" "NoReplace" "Eval")

(sdedr:define-gaussian-profile "AnalyticalProfileDefinition_n" "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 1e+12 "Depth" 10 "Gauss"  "Factor" 0.5)


(sdedr:define-refeval-window "RefEvalWin_global" "Cuboid"  (position 0 0 0)  (position -310 200 200) )

(sdedr:define-refinement-size "RefinementDefinition_global" 30 30 30 0.5 0.5 0.5 )

(sdedr:define-refinement-placement "RefinementPlacement_global" "RefinementDefinition_global" (list "window" "RefEvalWin_global" ) )

(sdedr:define-refinement-function "RefinementDefinition_global" "DopingConcentration" "MaxTransDiff" 1)

(sdedr:define-refinement-function "RefinementDefinition_global" "MaxLenInt" "Silicon" "Oxide" 0.1 1.8 "DoubleSide")


(sdedr:define-refeval-window "RefEvalWin_rad" "Cuboid"  (position 0 10 10)  (position -310 40 40) )

(sdedr:define-refinement-size "RefinementDefinition_rad" 4 4 4 2 2 2 )

(sdedr:define-refinement-placement "RefinementPlacement_rad" "RefinementDefinition_rad" (list "window" "RefEvalWin_rad" ) )




(sdegeo:define-contact-set "top_contact" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:define-contact-set "bottom_contact" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:define-3d-contact (list (car (find-face-id (position 0 100 100)))) "bottom_contact")

(sdegeo:define-3d-contact (list (car (find-face-id (position -304 100 100)))) "top_contact")

(sdeio:save-tdr-bnd "all" (string-append "n@node@_msh" ".bnd"))

(sde:build-mesh "snmesh" "-a -c boxmethod" "n@node@")