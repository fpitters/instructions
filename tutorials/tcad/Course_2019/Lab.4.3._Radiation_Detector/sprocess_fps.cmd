#setdep @previous@

AdvancedCalibration

init tdr= n@node|sde@_msh
implant Boron dose=4e15<cm-2> energy=40 
diffuse time= 10<s> temperature= 1000

struct tdr= n@node@