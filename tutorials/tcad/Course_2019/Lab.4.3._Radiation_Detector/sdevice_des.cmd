#setdep @previous@

File{
   Grid      = "@tdr@"
   Plot      = "@tdrdat@"
   Current   = "@plot@"
   Output    = "@log@"
}


Electrode{
   { Name="top_contact"       Voltage=0.0 }
   { Name="bottom_contact"        Voltage=0.0 }
}

Physics{  
   Mobility(
      DopingDep
      HighFieldsaturation
      Enormal
   )
   Recombination(
      SRH
      Auger
   )
   HeavyIon (
     Direction= (-1, 0, 0)
     Location= (0, 25, 25)
     Time= 2e-10
     Length= [1e-2 2e-2 3e-2]     *unit in centimeter
     Wt_hi= [5e-4 5e-4 5e-4] 	 	     *unit in centimeter
     LET_f= [2e7 2.5e7 3e7]
   )
}

Plot{
   eDensity hDensity
   TotalCurrent/Vector eCurrent/Vector hCurrent/Vector
   eMobility hMobility
   eVelocity hVelocity
   ElectricField/Vector Potential SpaceCharge
   Doping DonorConcentration AcceptorConcentration
   HeavyIonChargeDensity
   HeavyIonGeneration
   AvalancheGeneration
}

Math {
   Extrapolate
}

Solve {

   Coupled{ Poisson }
   Coupled{ Poisson Electron Hole}

   
   Quasistationary(
      InitialStep= 0.1 Increment= 2 Decrement= 4 
      MinStep= 1e-5 MaxStep= 10
      Goal{ Name="bottom_contact" Voltage= 80  }
   ){ Coupled{ Poisson Electron Hole} }
   
  NewCurrentFile="trans_"
  Transient (
    InitialTime= 0 FinalTime= 2e-8
    InitialStep= 1e-11 Increment= 1.2
    Minstep= 1e-12 MaxStep= 1e-8
  ){
    Coupled { Poisson Electron Hole}
    Plot (FilePrefix = "n@node@"
    		Time = ( 2.2e-10; 3e-10; 4e-10; 5e-10)
    		NoOverwrite
    )
  }

}
