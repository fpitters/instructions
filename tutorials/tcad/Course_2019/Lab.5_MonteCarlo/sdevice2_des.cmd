File {
  Grid=     "@tdr@"
  Output=   "@log@"
  Parameter="@parameter@" 
  Load=  "n@node|-2@_des.sav"
  MonteCarloOut = "n@node@_mc"
  Plot= "@tdrdat@"
}

Electrode {
	{ name="source" voltage=0.0 }
	{ name="drain"  voltage=-0.8 }
	{ name="gate"   voltage=-0.8 Workfunction = 4.58  } 
}

Plot {
hMCAvalanche
hMCCurrent
hMCCurrentBackward
hMCCurrentForward
hMCDensity
hMCDensityBackward
hMCDensityForward
hMCEnergy
}

MonteCarlo {
  SurfScattRatio = 0.85
  CurrentErrorBar = 2.0
  MinCurrentComput = 10
  DrainContact = "drain" 
  SelfConsistent(FrozenQF)
  Window = Cuboid[ (-0.05,-0.1 -0.1)
  (0.05, 0.1 0.1) ]
  FinalTime = 5e-06
  Plot { Range=(0,20e-06) intervals = 500 }
-InternalCurrentPlot
}

Physics {
  Mobility( PhuMob HighFieldSaturation Enormal )
  EffectiveIntrinsicDensity ( Slotboom NoFermi )
  hQuantumPotential
  Recombination(SRH)
}

Math {
  Number_of_Threads = 8
  Method=blocked
  Submethod=pardiso
  Extrapolate
  RelErrControl
  Digits=6
  ErrRef(Electron)=1.e10
  ErrRef(Hole)=1.e10
  NotDamped=20
  Iterations=30
  ExitOnFailure
-CheckUndefinedModels
  RHSmin= 1e-10

}

solve {
Coupled {poisson hole hQuantumPotential}
NewCurrentPrefix="n@node@_MCRamping_"
QuasiStationary(Goal{name="gate" voltage=-0.05}
maxstep=0.077 doZero) 
{ Plugin(iterations=0 breakOnFailure) {
Coupled {poisson  hole hQuantumPotential}
Save(filePrefix="n@node@_beforeMC")
montecarlo
Load(filePrefix="n@node@_beforeMC")}}
}

