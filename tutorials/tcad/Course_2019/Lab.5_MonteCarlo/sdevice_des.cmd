File {
  Grid=     "@tdr@"
  Current=  "@plot@" 
  Save=     "@save@"
  Output=   "@log@"
  Plot=     "@tdrdat@"
  Parameter="@parameter@"
}

Electrode {
  { Name="source" Voltage= 0.0 }
  { Name="drain"  Voltage= 0.0 }
  { Name="gate"   Voltage= 0.0 Workfunction = 4.58}
}

Math {
  Number_of_Threads = 8
  Method=blocked
  Submethod=pardiso
  Extrapolate
  RelErrControl
  Digits=6
  ErrRef(Electron)=1.e8
  ErrRef(Hole)=1.e8
  NotDamped=11
  Iterations=10
  ExitOnFailure
-CheckUndefinedModels
  RHSmin= 1e-10
}

Plot { 
  eDensity hDensity
  TotalCurrent/Vector eCurrent/Vector hCurrent/Vector
  eMobility hMobility
  eVelocity hVelocity
  eQuasiFermi hQuasiFermi
  eTemperature Temperature * hTemperature
  ElectricField/Vector Potential SpaceCharge
  Doping DonorConcentration AcceptorConcentration
  SRH Band2Band * Auger
  AvalancheGeneration eAvalancheGeneration hAvalancheGeneration
  eGradQuasiFermi/Vector hGradQuasiFermi/Vector
  eEparallel hEparallel eENormal hENormal
  BandGap 
  BandGapNarrowing
  Affinity
  ConductionBand ValenceBand
  eBarrierTunneling hBarrierTunneling * BarrierTunneling
  eTrappedCharge  hTrappedCharge
} 

Physics {
  Mobility( PhuMob HighFieldSaturation Enormal )
  EffectiveIntrinsicDensity ( Slotboom NoFermi )
  hQuantumPotential
  Recombination(SRH)
  hMultivalley(MLDA kpDOS -density)
}

Solve {
  Coupled(Iterations=100 LineSearchDamping = 1e-4){ Poisson hQuantumPotential} 
  Coupled { Poisson hQuantumPotential hole}

  Quasistationary ( 
   Goal { Name="drain" Voltage= -0.8 }
    Initialstep=0.01 Maxstep=0.2 Minstep=1e-8 Increment=1.4
){
 Coupled { Poisson hQuantumPotential hole} } 

  Quasistationary ( 
   Goal { Name="gate" Voltage= -0.8 }
    Initialstep=0.001 Maxstep=0.02 Minstep=1e-8 Increment=1.4
){
 Coupled { Poisson hQuantumPotential hole} } 
}


