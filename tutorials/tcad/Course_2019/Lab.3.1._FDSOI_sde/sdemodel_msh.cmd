Title ""

Controls {
}

Definitions {
	Constant "ConstantProfileDefinition_sub" {
		Species = "BoronActiveConcentration"
		Value = 1e+16
	}
	Constant "ConstantProfileDefinition_nisi" {
		Species = "ArsenicActiveConcentration"
		Value = 1e+16
	}
	Constant "ConstantProfileDefinition_bg" {
		Species = "BoronActiveConcentration"
		Value = 1e+19
	}
	AnalyticalProfile "APD_source" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 1e+16, Depth = 0.03)
		LateralFunction = Gauss(Factor = 0.5)
	}
	AnalyticalProfile "APD_drain" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 1e+16, Depth = 0.03)
		LateralFunction = Gauss(Factor = 0.5)
	}
	Refinement "RefinementDefinition_sub_lump_1" {
		MaxElementSize = ( 0.03 0.01 )
		MinElementSize = ( 0.03 0.005 )
	}
	Refinement "RefinementDefinition_Ni" {
		MaxElementSize = ( 0.005 0.001 )
		MinElementSize = ( 0.004 0.001 )
	}
	Refinement "RefinementDefinition_SiO2" {
		MaxElementSize = ( 0.008 0.001 )
		MinElementSize = ( 0.004 0.001 )
	}
	Refinement "RefinementDefinition_bg" {
		MaxElementSize = ( 0.008 0.001 )
		MinElementSize = ( 0.004 0.001 )
	}
	Refinement "RefinementDefinition_nisi" {
		MaxElementSize = ( 0.005 0.003 )
		MinElementSize = ( 0.003 0.001 )
	}
	Refinement "RefinementDefinition_chal" {
		MaxElementSize = ( 0.001 0.001 )
		MinElementSize = ( 0.001 0.001 )
	}
}

Placements {
	Constant "ConstantProfilePlacement_sub" {
		Reference = "ConstantProfileDefinition_sub"
		EvaluateWindow {
			Element = region ["region_sub"]
		}
	}
	Constant "ConstantProfilePlacement_nisi" {
		Reference = "ConstantProfileDefinition_nisi"
		EvaluateWindow {
			Element = region ["region_NiSi"]
		}
	}
	Constant "ConstantProfilePlacement_bg" {
		Reference = "ConstantProfileDefinition_bg"
		EvaluateWindow {
			Element = region ["region_bg"]
		}
	}
	AnalyticalProfile "APP_source" {
		Reference = "APD_source"
		ReferenceElement {
			Element = Line [(-0.02 -0.31) (-0.2 -0.31)]
		}
	}
	AnalyticalProfile "APP_drain" {
		Reference = "APD_drain"
		ReferenceElement {
			Element = Line [(0.02 -0.31) (0.2 -0.31)]
		}
	}
	Refinement "RefinementPlacement_sub_lump_1" {
		Reference = "RefinementDefinition_sub_lump_1"
		RefineWindow = region ["region_sub_lump_1"]
	}
	Refinement "RefinementPlacement_sub_lump_2" {
		Reference = "RefinementDefinition_sub_lump_1"
		RefineWindow = region ["region_sub_lump_2"]
	}
	Refinement "RefinementPlacement_Ni " {
		Reference = "RefinementDefinition_Ni"
		RefineWindow = material ["Si3N4"]
	}
	Refinement "RefinementPlacement_SiO2" {
		Reference = "RefinementDefinition_SiO2"
		RefineWindow = material ["SiO2"]
	}
	Refinement "RefinementPlacement_bg" {
		Reference = "RefinementDefinition_bg"
		RefineWindow = region ["region_bg"]
	}
	Refinement "RefinementPlacement_nisi" {
		Reference = "RefinementDefinition_nisi"
		RefineWindow = material ["NickelSilicide"]
	}
	Refinement "RefinementPlacement_chal" {
		Reference = "RefinementDefinition_chal"
		RefineWindow = Rectangle [(-0.015 -0.292) (0.015 -0.3039)]
	}
}

