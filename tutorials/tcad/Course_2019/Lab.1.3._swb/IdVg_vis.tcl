#setdep @node|sdevice@

set N @node|sdevice@
set COLORS  [list green blue red orange magenta violet brown]
set color   [lindex  $COLORS [expr @node:index@%[llength $COLORS] ]]

# Create plot if it does not exist and set properties
if {[llength [list_plots Plot_1D]]==0} {
  create_plot -1d -name Plot_1D
  select_plots Plot_1D
  set_plot_prop -hide_title -show_legend
  set_axis_prop -axis x -title "Gate Voltage (V)" \
	-title_font_size 24 -scale_font_size 22 -type linear 
  set_axis_prop -axis y -title "Drain Current (A/<greek>m</greek>m)" \
	-title_font_size 24 -scale_font_size 22 -type linear
  set_legend_prop -font_size 16 -font_att bold
}

# Load and plot IV curve
load_file IdVg_n@node|sdevice@_des.plt -name PLT($N)
create_curve -name IdVg($N) -dataset PLT($N) \
	-axisX "gate InnerVoltage" -axisY "drain TotalCurrent" 
set_curve_prop IdVg($N) -label "IdVg(n$N)" \
	-color $color -line_style solid -line_width 3

# - Extraction
# ----------------------------------------------------------------------#
load_library extract

#----------------------------------------------------------------------#
set Vgs [get_variable_data "gate OuterVoltage"  -dataset PLT($N)]
set Ids [get_variable_data "drain TotalCurrent" -dataset PLT($N)]
ext::ExtractVtgm     out=Vtgm  name=Vtgm  v= $Vgs i= $Ids 
ext::ExtractExtremum out=Id    name=Id    x= $Vgs y= $Ids type=max
##ext::ExtractSS       out=SS    name=SS    v= $Vgs i= $Ids vo= 0.01
ext::ExtractSS       out=SS    name=SS    v= $Vgs i= $Ids vo= [expr $Vtgm/3.0]
ext::ExtractGm       out=gm    name=gm    v= $Vgs i= $Ids

