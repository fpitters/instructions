
line x location= 0.0      spacing= 1.0<nm>  tag=SiTop        
line x location=50.0<nm>  spacing=10.0<nm>                    
line x location= 0.5<um>  spacing=50.0<nm>                      
line x location= 1.0<um>  spacing= 0.2<um>   tag=SiBottom 

line y location=0.0       spacing=50.0<nm>  tag=Mid         
line y location=0.40<um>  spacing=50.0<nm>  tag=Right

region Silicon xlo=SiTop xhi=SiBottom ylo=Mid yhi=Right

init !DelayFullD 

implant  Boron  dose=@dose_B@   energy= @im_energy@ tilt=0 rotation=0

struct tdr= n@node@  !Gas !interfaces


