#setdep @previous@

AdvancedCalibration

init tdr=n@node|sde@_msh

#mgoals accuracy= 1e-6
pdbSet Grid SnMesh min.normal.size 0.001
pdbSet Grid SnMesh normal.growth.ratio.3d 1.2

implant Phosphorus dose=4e15 energy=30
diffuse time=10<min> temp=1000<C>

refinebox xrefine= {0.001  0.5  10}  yrefine= 0.1 zrefine= 0.1 silicon
grid remesh 

struct smesh=n@node@  Gas
