#setdep @previous@

#IOControls {
#	inputFile = n1_msh.tdr
#}

Tensor {
	Mesh {
		MaxCellSize direction "z" 0.005
		MaxCellSize material "Silicon" 0.005
		
		MaxCellSize material direction "Silicon" "z"  0.005
		
		minCellSize direction "x" 0.001
		minCellSize direction "y" 0.001
		minCellSize direction "z" 0.001
	}
#	EMW {
		
#		parameter filename= @parameter@
#		ComplexRefractiveIndex WavelengthDep Real Imag
#		wavelength = 0.5
#		NPWX= 30
#		NPWY= 30
#		NPWZ= 30
#		Grading off
#	}
}
