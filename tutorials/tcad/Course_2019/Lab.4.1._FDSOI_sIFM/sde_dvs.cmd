(sdegeo:create-polygon (list  (position 0 -0.1 0)  (position -0.3 -0.1 0)  (position -0.3 -0.085 0)  (position -0.31 -0.08 0)  (position -0.31 -0.02 0)  (position -0.3 -0.01 0)  (position -0.3 0.01 0)  (position -0.31 0.02 0)  (position -0.31 0.08 0)  (position -0.3 0.085 0)  (position -0.3 0.1 0)  (position 0 0.1 0)  (position 0 -0.1 0) ) "Silicon" "region_sub")

(sdegeo:create-rectangle (position -0.3 -0.015 0)  (position -0.3339 -0.005 0) "Si3N4" "region_Ni_left")

(sdegeo:create-rectangle (position -0.3 0.015 0)  (position -0.3339 0.005 0) "Si3N4" "region_Ni_right")

(sdegeo:fillet-2d (list (car (find-vertex-id (position -0.3339 -0.015 0)))) 0.008)

(sdegeo:fillet-2d (list (car (find-vertex-id (position -0.3339 0.015 0)))) 0.008)

(sdegeo:create-rectangle (position -0.262 -0.1 0)  (position -0.282 0.1 0) "Silicon" "region_bg")

(sdegeo:create-rectangle (position -0.282 -0.1 0)  (position -0.292 0.1 0) "SiO2" "region_insulator")

(sdegeo:create-polygon (list  (position -0.202 -0.1 0)  (position -0.3 -0.1 0)  (position -0.3 -0.085 0)  (position -0.202 -0.09 0)  (position -0.202 -0.1 0) ) "SiO2" "region_STI_l")

(sdegeo:create-polygon (list  (position -0.202 0.1 0)  (position -0.3 0.1 0)  (position -0.3 0.085 0)  (position -0.202 0.09 0)  (position -0.202 0.1 0) ) "SiO2" "region_STI_r")


(sdegeo:create-rectangle (position -0.3 -0.01 0)  (position -0.3014 0.01 0) "SiO2" "region_tox")
(sdegeo:create-rectangle (position -0.3014 -0.01 0)  (position -0.3039 0.01 0) "HfO2" "region_HfO")


(sde:separate-lumps)

(sdegeo:define-contact-set "source" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:define-contact-set "drain" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:define-contact-set "substrate" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:define-contact-set "backgate" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:define-contact-set "gate" 4  (color:rgb 1 0 0 ) "##" )


(sdegeo:set-current-contact-set "gate")
(sdegeo:set-contact-edges (list (car (find-edge-id (position -0.3189 -0.005 0))) (car (find-edge-id (position -0.3039 0.0 0))) (car (find-edge-id (position -0.3189 0.005 0)))) "gate")

(sdegeo:set-current-contact-set "substrate")
(sdegeo:set-contact-edges (list (car (find-edge-id (position 0.0 0 0)))) "substrate")

(sdegeo:set-current-contact-set "source")
(sdegeo:set-contact-edges (list (car (find-edge-id (position -0.31 -0.05 0)))) "source")

(sdegeo:set-current-contact-set "drain")
(sdegeo:set-contact-edges (list (car (find-edge-id (position -0.31 0.05 0)))) "drain")

(sdegeo:set-current-contact-set "backgate")
(sdegeo:set-contact-edges (list (car (find-edge-id (position -0.262 0.0 0)))) "backgate")

(sdeio:save-tdr-bnd (get-body-list) "n1_bnd.tdr")

(sdedr:define-constant-profile "ConstantProfileDefinition_sub_lump_1" "BoronActiveConcentration" 1e16)

(sdedr:define-constant-profile-region "ConstantProfilePlacement_sub_lump_1" "ConstantProfileDefinition_sub_lump_1" "region_sub_lump_1")

(sdedr:define-constant-profile "ConstantProfileDefinition_sub_lump_2" "BoronActiveConcentration" 1e16)

(sdedr:define-constant-profile-region "ConstantProfilePlacement_sub_lump_2" "ConstantProfileDefinition_sub_lump_2" "region_sub_lump_2")


(sdedr:define-constant-profile "ConstantProfileDefinition_bg" "BoronActiveConcentration" 1e+19)

(sdedr:define-constant-profile-region "ConstantProfilePlacement_bg" "ConstantProfileDefinition_bg" "region_bg")


(sdedr:define-refeval-window "RefEvalWin_drain" "Line"  (position -0.31 0.02 0) (position -0.31 0.2 0))
(sdedr:define-refeval-window "RefEvalWin_source" "Line"  (position -0.31 -0.02 0) (position -0.31 -0.2 0))


(sdedr:define-analytical-profile-placement "APP_source" "APD_source" "RefEvalWin_source" "Both" "NoReplace" "Eval")

(sdedr:define-gaussian-profile "APD_source" "ArsenicActiveConcentration" "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 1e16 "Depth" 0.03 "Gauss"  "Factor" 0.5)


(sdedr:define-analytical-profile-placement "APP_drain" "APD_drain" "RefEvalWin_drain" "Both" "NoReplace" "Eval")

(sdedr:define-gaussian-profile "APD_drain" "ArsenicActiveConcentration" "PeakPos" 0  "PeakVal" 1e+20 "ValueAtDepth" 1e+16 "Depth" 0.03 "Gauss"  "Factor" 0.5)


(sdedr:define-refeval-window "RefEvalWin_global" "Rectangle"  (position 0 -0.1 0) (position -0.35 0.1 0))

(sdedr:define-refinement-size "RefinementDefinition_global" 0.05 0.05 0.05 0.05 )

(sdedr:define-refinement-placement "RefinementPlacement_global" "RefinementDefinition_global" (list "window" "RefEvalWin_global" ) )


(sdedr:define-refinement-size "RefinementDefinition_sub_lump_1" 0.01 0.01 0.0002 0.0002 )

(sdedr:define-refinement-placement "RefinementPlacement_sub_lump_1" "RefinementDefinition_sub_lump_1" (list "region" "region_sub_lump_1" ) )

(sdedr:define-refinement-function "RefinementDefinition_sub_lump_1" "DopingConcentration" "MaxTransDiff" 1.0)

(sdedr:define-refinement-function "RefinementDefinition_sub_lump_1" "MaxLenInt" "Silicon" "SiO2" 0.0001 1.4 "DoubleSide" )


(sdedr:define-refinement-size "RefinementDefinition_bg" 0.005 0.01 0.001 0.004 )

(sdedr:define-refinement-placement "RefinementPlacement_bg" "RefinementDefinition_bg" (list "region" "region_bg" ) )


(sde:build-mesh "" "n@node@")




