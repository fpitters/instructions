Title "Untitled"

Controls {
}

Definitions {
	Constant "ConstantProfileDefinition_sub_lump_1" {
		Species = "BoronActiveConcentration"
		Value = 1e+16
	}
	Constant "ConstantProfileDefinition_sub_lump_2" {
		Species = "BoronActiveConcentration"
		Value = 1e+16
	}
	Constant "ConstantProfileDefinition_bg" {
		Species = "BoronActiveConcentration"
		Value = 1e+19
	}
	AnalyticalProfile "APD_source" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 1e+16, Depth = 0.03)
		LateralFunction = Gauss(Factor = 0.5)
	}
	AnalyticalProfile "APD_drain" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 1e+16, Depth = 0.03)
		LateralFunction = Gauss(Factor = 0.5)
	}
	Refinement "RefinementDefinition_sub_lump_2" {
		MaxElementSize = ( 0.05 0.04 )
		MinElementSize = ( 0.05 0.005 )
	}
	Refinement "RefinementDefinition_Ni" {
		MaxElementSize = ( 0.01 0.002 )
		MinElementSize = ( 0.004 0.001 )
	}
	Refinement "RefinementDefinition_SiO2" {
		MaxElementSize = ( 0.01 0.005 )
		MinElementSize = ( 0.004 0.001 )
	}
	Refinement "RefinementDefinition_bg" {
		MaxElementSize = ( 0.01 0.005 )
		MinElementSize = ( 0.004 0.001 )
	}
	Refinement "RefinementDefinition_chal" {
		MaxElementSize = ( 0.001 0.001 )
		MinElementSize = ( 0.001 0.001 )
	}
	Refinement "RefinementDefinition_sub_lump_1" {
		MaxElementSize = ( 0.005 0.005 )
		MinElementSize = ( 0.0002 0.0002 )
		RefineFunction = MaxLenInt(Interface("Silicon","SiO2"), Value=0.001, factor=1.2, DoubleSide)
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 0.5)
	}
}

Placements {
	Constant "ConstantProfilePlacement_sub_lump_1" {
		Reference = "ConstantProfileDefinition_sub_lump_1"
		EvaluateWindow {
			Element = region ["region_sub_lump_1"]
		}
	}
	Constant "ConstantProfilePlacement_sub_lump_2" {
		Reference = "ConstantProfileDefinition_sub_lump_2"
		EvaluateWindow {
			Element = region ["region_sub_lump_2"]
		}
	}
	Constant "ConstantProfilePlacement_bg" {
		Reference = "ConstantProfileDefinition_bg"
		EvaluateWindow {
			Element = region ["region_bg"]
		}
	}
	AnalyticalProfile "APP_source" {
		Reference = "APD_source"
		ReferenceElement {
			Element = Line [(-0.02 -0.31) (-0.2 -0.31)]
		}
	}
	AnalyticalProfile "APP_drain" {
		Reference = "APD_drain"
		ReferenceElement {
			Element = Line [(0.02 -0.31) (0.2 -0.31)]
		}
	}
	Refinement "RefinementPlacement_sub_lump_2" {
		Reference = "RefinementDefinition_sub_lump_2"
		RefineWindow = region ["region_sub_lump_2"]
	}
	Refinement "RefinementPlacement_Ni " {
		Reference = "RefinementDefinition_Ni"
		RefineWindow = material ["Si3N4"]
	}
	Refinement "RefinementPlacement_SiO2" {
		Reference = "RefinementDefinition_SiO2"
		RefineWindow = material ["SiO2"]
	}
	Refinement "RefinementPlacement_bg" {
		Reference = "RefinementDefinition_bg"
		RefineWindow = region ["region_bg"]
	}
	Refinement "RefinementPlacement_chal" {
		Reference = "RefinementDefinition_chal"
		RefineWindow = Rectangle [(-0.015 -0.292) (0.015 -0.3039)]
	}
	Refinement "RefinementPlacement_sub_lump_1" {
		Reference = "RefinementDefinition_sub_lump_1"
		RefineWindow = region ["region_sub_lump_2"]
		RefineWindow = region ["region_sub_lump_1"]
	}
}

