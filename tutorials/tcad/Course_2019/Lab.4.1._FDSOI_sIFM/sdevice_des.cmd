

Device FDSOI {

File {
	Grid=      "@tdr@"
	Parameter= "@parameter@"
	Current=   "@plot@"
	Plot=      "@tdrdat@"

}

Electrode {
   { Name="source"         Voltage= 0.0 }
   { Name="drain"          Voltage= 0.0 }
   { Name="gate"           Voltage= 0.0 WorkFunction= 4.5}
   { Name="substrate"      Voltage= 0.0 }
   { Name="backgate"       Voltage= 0.0 }
}

Physics{
#   Hydrodynamic 
   eQuantumPotential 
   hQuantumPotential 
   Mobility(
      DopingDependence
      Thinlayer
      HighFieldsaturation
   ) 
   
   RandomizedVariation "rdf" ( Doping)
}

}

File {
	Output= "@log@"
}

Plot {
  eDensity hDensity 
  Current eCurrent  hCurrent
  Potential  SpaceCharge  ElectricField
  eMobility  hMobility  eVelocity  hVelocity
  Doping DonorConcentration AcceptorConcentration
}

Math {
	Extrapolate
	Notdamped=50
	Iterations=15
	ExitOnFailure
	Digits=5
	ErrEff(electron)=1e10
	ErrEff(hole)=1e10
	# eDrForceRefDens= 1e12  * Damping parameters 
	# hDrForceRefDens= 1e12  *   for high-field mobility driving force
	# RelTermMinDensity= 1e3 * Stabilization parameters for temp. relax. term.
	# RelTermMinDensityZero= 1e8 
	# ExitOnFailure
	
	RandomizedVariation "rdf" (
    NumberOfSamples = 200
    Randomize = 0
    )


}


File {
  Output= "@log@"
}


System {
  Vsource_pset vdrain (ndrain 0) { dc= 0 }
  Vsource_pset vgate (ngate 0) { dc= 0 }
  Vsource_pset vbackgate (nbackgate 0) { dc= 0 }
  FDSOI DUT ("drain"= ndrain "source"= 0 "gate"= ngate "backgate"= nbackgate "substrate"= 0)
  Plot "n@node@_sys" ( v(ngate)  i(vdrain, ndrain) )

}


Solve {
*- Creating initial guess:
   Coupled(Iterations= 100){ Poisson eQuantumPotential hQuantumPotential} 
   Coupled {Poisson Electron Hole eQuantumPotential hQuantumPotential}
   
*- Ramp up the backgate voltage :   
   Quasistationary( 
      InitialStep= 1e-3 Increment= 1.2 
      MinStep= 1e-3 MaxStep= 0.2 
      Goal { parameter= vbackgate.dc Voltage= @back_bias@ } 
   ){ Coupled {Poisson Electron Hole eQuantumPotential hQuantumPotential} }
   
*- Ramp to drain voltage
   Quasistationary( 
      InitialStep= 1e-2 Increment= 1.2 
      MinStep= 1e-4 MaxStep= 0.2 
      Goal { parameter= vdrain.dc Voltage= 1.5} 
   ){ Coupled {Poisson Electron Hole eQuantumPotential hQuantumPotential} }

*- Vg sweep  
   NewCurrentPrefix= "IdVg_"
   Quasistationary( 
      InitialStep= 1e-1 Increment= 1.2 
      MinStep= 1e-4 MaxStep= 0.2 
      Goal { parameter= vgate.dc Voltage= 1.5 }
   ){ ACCoupled (
   		StartFrequency= 0 EndFrequency= 0
   		NumberOfPoints= 1 Decade
   		Node(ndrain ngate)
   		ObservationNode(ndrain)
   		ACExtract= "n@node@_sIFM"
   		)
   	   {Poisson Electron Hole eQuantumPotential hQuantumPotential} 
       
   	}
   
}
