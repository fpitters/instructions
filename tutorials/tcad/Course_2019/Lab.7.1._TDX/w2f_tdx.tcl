
set FIDr [open n6_opticalgeneration.csv "r"]

set inp "n4_opt_des.tdr"

set file_data [read $FIDr]
set data [split $file_data "\n"]

TdrFileOpen $inp

set iv 0
set flag 0
set i 1
set ng [TdrFileGetNumGeometry $inp]
for {set ig 0} {$ig < $ng} {incr ig} {
	set ns [TdrGeometryGetNumState $inp $ig]
	set nr [TdrGeometryGetNumRegion $inp $ig]
	for {set is 0} {$is < $ns} {incr is} {
	for {set ir 0} {$ir < $nr} {incr ir} {		
	set nd [TdrRegionGetNumDataset $inp $ig $ir $is]
		for {set id 0} {$id < $nd} {incr id} { 
			if {[string match OpticalGeneration [TdrDatasetGetQuantity $inp $ig $ir $is $id]]} {
				set nv [TdrDatasetGetNumValue $inp $ig $ir $is $id]
				for {set iv 0} {$iv < $nv} {incr iv} {
					set opv [lindex $[lindex $data $i] 4]
					TdrDataSetComponent $inp $ig $ir $is $id $iv 0 0 $opv
					incr i
				}
				break
			}
		}

	}

	}
}


TdrFileSave $inp 
TdrFileClose $inp				
close $FIDr
