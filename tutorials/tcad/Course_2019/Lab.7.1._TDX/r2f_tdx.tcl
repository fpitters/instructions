set inp "n6_des.tdr"
set outf [open "n6_opticalgeneration.csv" "w"]
TdrFileOpen $inp


puts $outf "Material Region X Y OpticalGeneration"
set ng [TdrFileGetNumGeometry $inp]
for {set ig 0} {$ig< $ng} {incr ig} {
	set ns [TdrGeometryGetNumState $inp $ig]
	set nr [TdrGeometryGetNumRegion $inp $ig]
	for {set is 0} {$is < $ns} {incr is} {
	for {set ir 0} {$ir < $nr} {incr ir} {
		set rname [TdrRegionGetName $inp $ig $ir]
		set mat [TdrRegionGetMaterial $inp $ig $ir]
		
		set nd [TdrRegionGetNumDataset $inp $ig $ir $is]
		for {set id 0} {$id < $nd} {incr id} {
			if {[string match OpticalGeneration [TdrDatasetGetQuantity $inp $ig $ir $is $id]]} {
				break
			}
		}
		if {$id!=$nd} {
			set nv [TdrDatasetGetNumValue $inp $ig $ir $is $id]
			for {set iv 0} {$iv < $nv} {incr iv} {
				set val [TdrDataGetComponent $inp $ig $ir $is $id $iv 0 0]
				set cx [TdrDataGetCoordinate $inp $ig $ir $is $id $iv 0]
				set cy [TdrDataGetCoordinate $inp $ig $ir $is $id $iv 1]
				puts $outf "$mat $rname [format %.4f $cx] [format %.4f $cy] $val"
			}
		}
	}	
	}
}
	
TdrFileClose $inp
close $outf



