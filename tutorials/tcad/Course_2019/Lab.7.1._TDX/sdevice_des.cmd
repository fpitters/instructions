#setdep @previous@

File {
  Grid = "n@node|sde@_msh.tdr"
#if @illumination@ == on  
  OpticalGenerationInput = "n@node|emw@_g_eml.tdr"
#endif
  Plot    = "@tdrdat@"
  Current = "@plot@"
  Output  = "@log@"
}

Electrode {   
  { name="s_contact" voltage=(0.0 at 0, 1.0 at 0.1) }
  { name="g_contact" voltage=(0.0 at 0.0, 1.5 at 0.1, 1.5 at 0.4, 0 at 0.5, 0 at 0.8, 1.5 at 0.9, 1.5 at 1.2, 0 at 1.3) }
  { name="bot_contact" voltage=0.0}
}

Plot{
   eDensity hDensity
   ElectricField/Vector
   TotalCurrent/Vector eCurrent/Vector hCurrent/Vector
   Doping DonorConcentration AcceptorConcentration
   OpticalGeneration
   AbsorbedPhotonDensity
   ConductionBand ValenceBand
}

Physics {
  Mobility (
    DopingDep
    HighFieldsaturation( GradQuasiFermi )
  )
   Recombination(
      SRH
      Auger
   )
#if @illumination@ == on
  Optics (
    ComplexRefractiveIndex (WavelengthDep(Real Imag))
    OpticalGeneration (
      ReadFromFile(DatasetName = OpticalGeneration)
    )
  )
#endif
}

Math {
   Extrapolate
   ErrRef(Electron)=1.e12
   ErrRef(Hole)=1.e12
}

CurrentPlot { 
   ElectrostaticPotential ( (0.1, 0.6) )
}


Solve {
  Coupled(Iterations=100){Poisson}
  Coupled(Iterations=100){Poisson Electron Hole}

    Transient (
      InitialTime = 0 FinalTime = 1.3
      Increment = 1.5 Decrement = 4
    ) {	Coupled {Poisson Electron Hole} }
      
} 
