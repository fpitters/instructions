

(sdegeo:create-rectangle (position 0 0 0)  (position 2.5 1.5 0) "Gas" "region_gas")

(sdegeo:create-circle (position 1.2 0.6 0)  0.6 "Si3N4" "region_lens")

(sdegeo:create-rectangle (position 1.4 0 0)  (position 1.45 1.5 0) "Si3N4" "region_nitride")

(sdegeo:create-rectangle (position 0 0 0)  (position 1.4 1.5 0) "Oxide" "region_ox")

(sdegeo:create-rectangle (position 0 0 0)  (position 1 1.5 0) "Silicon" "region_sub")

(sdegeo:create-rectangle (position 0 0 0)  (position -0.25 1.5 0) "Oxide" "gate_oxide")

(sdegeo:create-rectangle (position -0.01 1.15 0)  (position -0.2 1.35 0) "PolySi" "region_polysi")

(sdedr:define-constant-profile "ConstantProfileDefinition_sub" "BoronActiveConcentration" 1e15)

(sdedr:define-constant-profile-region "ConstantProfilePlacement_sub" "ConstantProfileDefinition_sub" "region_sub")

(sdedr:define-constant-profile "ConstantProfileDefinition_poly" "ArsenicActiveConcentration" 1e+19)

(sdedr:define-constant-profile-region "ConstantProfilePlacement_poly" "ConstantProfileDefinition_poly" "region_polysi")


(sdedr:define-refeval-window "RefEvalWin_1" "Line"  (position 0 1.4 0) (position 0 1.6 0))

(sdedr:define-analytical-profile-placement "AnalyticalProfilePlacement_n" "AnalyticalProfileDefinition_n" "RefEvalWin_1" "Negative" "NoReplace" "Eval")

(sdedr:define-gaussian-profile "AnalyticalProfileDefinition_n" "ArsenicActiveConcentration" "PeakPos" 0  "PeakVal" 1e19 "ValueAtDepth" 5e15 "Depth" 0.1 "Gauss"  "Factor" 0.8)


(sdedr:define-refeval-window "RefEvalWin_2" "Line"  (position 0.1 0.1 0) (position 0.1 0.95 0))

(sdedr:define-analytical-profile-placement "AnalyticalProfilePlacement_nw" "AnalyticalProfileDefinition_nw" "RefEvalWin_2" "Both" "NoReplace" "Eval")

(sdedr:define-gaussian-profile "AnalyticalProfileDefinition_nw" "ArsenicActiveConcentration" "PeakPos" 0  "PeakVal"  1e17 "ValueAtDepth" 1e16 "Depth" 0.07 "Gauss"  "Factor" 0.8)


(sdedr:define-refeval-window "RefEvalWin_3" "Line"  (position 0 0 0) (position 0 1.05 0))

(sdedr:define-analytical-profile-placement "AnalyticalProfilePlacement_pcap" "AnalyticalProfileDefinition_pcap" "RefEvalWin_3" "Negative" "NoReplace" "Eval")

(sdedr:define-gaussian-profile "AnalyticalProfileDefinition_pcap" "BoronActiveConcentration" "PeakPos" 0  "PeakVal"  1e19 "ValueAtDepth" 5e15 "Depth" 0.03 "Gauss"  "Factor" 0.8)



(sdedr:define-refinement-size "RefinementDefinition_sub" 0.05 0.05 5e-3 5e-3 )

(sdedr:define-refinement-placement "RefinementPlacement_sub" "RefinementDefinition_sub" (list "region" "region_sub" ) )

(sdedr:define-refinement-function "RefinementDefinition_sub" "MaxLenInt" "Silicon" "Oxide" 5e-3 1.2 "DoubleSide")

(sdedr:define-refinement-function "RefinementDefinition_sub" "DopingConcentration" "MaxTransDiff" 1.0)


(sdedr:define-refinement-size "RefinementDefinition_nitride" 0.05 0.05 0.05 0.05 )

(sdedr:define-refinement-placement "RefinementPlacement_nitride" "RefinementDefinition_nitride" (list "material" "Si3N4" ) )


# (sdedr:define-refinement-size "RefinementDefinition_poly" 0.1 0.1 0.1 0.1 )

# (sdedr:define-refinement-placement "RefinementPlacement_poly" "RefinementDefinition_poly" (list "region" "region_polysi" ) )

#  (sdedr:define-refinement-function "RefinementDefinition_poly" "MaxLenInt" "Oxide" "PolySi" 1e-3 1.1 "DoubleSide")



(sdegeo:define-contact-set "s_contact" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:define-contact-set "g_contact" 4  (color:rgb 1 1 0 ) "##")

(sdegeo:define-contact-set "bot_contact" 4  (color:rgb 1 0 1 ) "##")

(sdegeo:insert-vertex (position 0 1.42 0))

(sdegeo:set-current-contact-set "g_contact")

(sdegeo:set-contact-edges (list (car (find-edge-id (position -0.2 1.3 0)))) "g_contact")

(sdegeo:set-current-contact-set "s_contact")

(sdegeo:set-contact-edges (list (car (find-edge-id (position 0 1.45 0)))) "s_contact")

(sdegeo:set-current-contact-set "bot_contact")

(sdegeo:set-contact-edges (list (car (find-edge-id (position 1 0.75 0)))) "bot_contact")


(sde:build-mesh "" "n@node@")

