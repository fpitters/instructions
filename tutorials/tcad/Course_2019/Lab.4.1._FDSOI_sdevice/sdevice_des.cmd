File {
	Grid=      "@tdr@"
	Parameter= "@parameter@"
	Current=   "@plot@"
	Plot=      "@tdrdat@"
	Output=    "@log@"
}

Electrode {
   { Name="source"         Voltage= 0.0 }
   { Name="drain"          Voltage= 0.0 }
   { Name="gate"           Voltage= 0.0 WorkFunction= 4.5}
   { Name="substrate"      Voltage= 0.0 }
   { Name="backgate"       Voltage= 0.0 }
}

Physics{
#   Hydrodynamic 
   eQuantumPotential 
   hQuantumPotential 
   Mobility(
      DopingDependence
      Thinlayer
      HighFieldsaturation
   )         
}

Plot {
  eDensity hDensity 
  Current eCurrent  hCurrent
  Potential  SpaceCharge  ElectricField
  eMobility  hMobility  eVelocity  hVelocity
  Doping DonorConcentration AcceptorConcentration
}

Math {
	Extrapolate
	Notdamped=50
	Iterations=15
	ExitOnFailure
	Digits=5
	ErrEff(electron)=1e10
	ErrEff(hole)=1e10
	# eDrForceRefDens= 1e12  * Damping parameters 
	# hDrForceRefDens= 1e12  *   for high-field mobility driving force
	# RelTermMinDensity= 1e3 * Stabilization parameters for temp. relax. term.
	# RelTermMinDensityZero= 1e8 
	# ExitOnFailure

}

Solve {
*- Creating initial guess:
   Coupled(Iterations= 100){ Poisson eQuantumPotential hQuantumPotential} 
   Coupled {Poisson Electron Hole eQuantumPotential hQuantumPotential}
   
*- Ramp up the backgate voltage :   
   Quasistationary( 
      InitialStep= 1e-3 Increment= 1.2 
      MinStep= 1e-3 MaxStep= 0.2 
      Goal { Name= "backgate" Voltage= @back_bias@ } 
   ){ Coupled {Poisson Electron Hole eQuantumPotential hQuantumPotential} }
   
*- Ramp to drain voltage
   Quasistationary( 
      InitialStep= 1e-2 Increment= 1.2 
      MinStep= 1e-4 MaxStep= 0.2 
      Goal { Name= "drain" Voltage= 1.5} 
   ){ Coupled {Poisson Electron Hole eQuantumPotential hQuantumPotential} }

*- Vg sweep   
   Quasistationary( 
      InitialStep= 1e-1 Increment= 1.2 
      MinStep= 1e-4 MaxStep= 0.2 
      Goal { Name= "gate" Voltage= 1.5 }
   ){ Coupled {Poisson Electron Hole eQuantumPotential hQuantumPotential} 
       
   	}
   
}
