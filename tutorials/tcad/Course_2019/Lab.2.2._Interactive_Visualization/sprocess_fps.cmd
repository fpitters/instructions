math coord.ucs

line x location= 0.0      spacing= 1.0<nm>  tag=SiTop        
line x location= 50.0<nm>  spacing=10.0<nm>                    
line x location= 0.5<um>  spacing=50.0<nm>                      
line x location= 2<um>  spacing= 0.2<um>   tag=SiBottom                  

line y location=0.0       spacing=50.0<nm>  tag=Mid         
line y location=0.40<um>  spacing=50.0<nm>  tag=Right  


region Silicon xlo=SiTop xhi=SiBottom ylo=Mid yhi=Right
init concentration=1e15 field=Phosphorus  !DelayFullD 

implant  Boron  dose=2.0e13<cm-2>  energy=200<keV> tilt=0 rotation=0  
implant  Boron  dose=1.0e13<cm-2>  energy= 80<keV> tilt=0 rotation=0  
implant  Boron  dose=2.0e12<cm-2>  energy= 25<keV> tilt=0 rotation=0

diffuse temperature=1050 time=10.0<s>

grid set.min.normal.size=1<nm> set.normal.growth.ratio.2d=1.5

diffuse temperature=850 time=10.0<min> O2

deposit material= {PolySilicon} type=anisotropic time=1 rate= {0.18}

mask name=gate_mask left=-1 right=90<nm>
etch material= {PolySilicon} type=anisotropic time=1 rate= {0.2} mask=gate_mask
etch material= {Oxide}       type=anisotropic time=1 rate= {0.1}

diffuse temperature=900 time=10.0<min>  O2 

refinebox Silicon min= {0.0 0.05} max= {0.1 0.12} xrefine= {0.01 0.01 0.01} yrefine= {0.01 0.01 0.01} add
refinebox remesh


implant Arsenic dose=4e14<cm-2> energy=10 tilt=0 rotation=0  
diffuse temperature=1050 time=0.1<s>

implant Boron dose=0.25e13<cm-2> energy=20<keV> tilt=30 rotation=0            
implant Boron dose=0.25e13<cm-2> energy=20<keV> tilt=30 rotation=90   
implant Boron dose=0.25e13<cm-2> energy=20<keV> tilt=30 rotation=180  
implant Boron dose=0.25e13<cm-2> energy=20<keV> tilt=30 rotation=270 

diffuse temperature=1050 time=5.0<s>

deposit material= {Nitride} type=isotropic   time=1 rate= {0.06}
etch    material= {Nitride} type=anisotropic time=1 rate= {0.084} isotropic.overetch=0.01
etch    material= {Oxide}   type=anisotropic time=1 rate= {0.01} 

refinebox Silicon min= {0.04 0.12} max= {0.18 0.4} xrefine= {0.01 0.01 0.01}  yrefine= {0.05 0.05 0.05} add
refinebox remesh

implant Arsenic dose=5e15<cm-2> energy=40 tilt=7 rotation=-90
diffuse temperature=1050<C> time=10.0<s>

transform reflect left

contact box Silicon name = source xlo= -0.02  ylo = -0.4   xhi = 0.02   yhi = -0.2    
contact box Silicon name = drain  xlo= -0.01  ylo = 0.4   xhi = 0.01   yhi = 0.2
contact box Polysilicon name = gate  xlo= -0.175  ylo = -0.07   xhi = -0.2   yhi = 0.07
contact bottom name= substrate

struct tdr= n@node@_NMOS !Gas

 
