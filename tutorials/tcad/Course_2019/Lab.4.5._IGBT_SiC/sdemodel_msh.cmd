Title ""

Controls {
}

IOControls {
	outputFile = "./sdemodel"
	EnableSections
}

Definitions {
	Constant "ConstantProfileDefinition_p+" {
		Species = "AluminumActiveConcentration"
		Value = 1e+20
	}
	Constant "ConstantProfileDefinition_n+_cap" {
		Species = "NitrogenActiveConcentration"
		Value = 1e+20
	}
	Constant "ConstantProfileDefinition_p-" {
		Species = "AluminumActiveConcentration"
		Value = 4e+17
	}
	Constant "ConstantProfileDefinition_n-" {
		Species = "NitrogenActiveConcentration"
		Value = 1e+15
	}
	Constant "ConstantProfileDefinition_p+_bot" {
		Species = "AluminumActiveConcentration"
		Value = 1e+20
	}
	Refinement "RefinementDefinition_1" {
		MaxElementSize = ( 1 0.2 )
		MinElementSize = ( 0.01 0.01 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
		RefineFunction = MaxLenInt(Interface("Oxide","SiliconCarbide"), Value=0.01, factor=1.2, DoubleSide)
	}
	Multibox "MultiboxDefinition_ava"
 {
		MaxElementSize = ( 0.2 1 )
		MinElementSize = ( 0.01 0.01 )
		Ratio = ( 1 -1.1 )
	}
}

Placements {
	Constant "ConstantProfilePlacement_p+" {
		Reference = "ConstantProfileDefinition_p+"
		EvaluateWindow {
			Element = Rectangle [(-100 0) (-99.5 1)]
		}
	}
	Constant "ConstantProfilePlacement_n+_cap" {
		Reference = "ConstantProfileDefinition_n+_cap"
		EvaluateWindow {
			Element = Polygon [ (-100 1) (-99.5 1) (-99.5 3.5) (-100 3.5) (-100 2.8)]
		}
	}
	Constant "ConstantProfilePlacement_p-" {
		Reference = "ConstantProfileDefinition_p-"
		EvaluateWindow {
			Element = Rectangle [(-97.5 0) (-99.5 3.5)]
		}
	}
	Constant "ConstantProfilePlacement_n-" {
		Reference = "ConstantProfileDefinition_n-"
		EvaluateWindow {
			Element = Rectangle [(-97.5 0) (-0.5 4)]
		}
	}
	Constant "ConstantProfilePlacement_p+_bot" {
		Reference = "ConstantProfileDefinition_p+_bot"
		EvaluateWindow {
			Element = Rectangle [(0 0) (-0.5 4)]
		}
	}
	Refinement "RefinementPlacement_global" {
		Reference = "RefinementDefinition_1"
		RefineWindow = Rectangle [(-102.2406 -1.4504) (1.4768 5.0088)]
	}
	Multibox "MultiboxPlacement_ava" {
		Reference = "MultiboxDefinition_ava"
		RefineWindow = Rectangle [(-97 0) (0 4)]
	}
}

