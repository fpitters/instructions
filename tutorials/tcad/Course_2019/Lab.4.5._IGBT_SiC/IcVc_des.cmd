File {
	Grid=      "@tdr|sde@" 
	Parameter= "@parameter@"
	Plot=      "@tdrdat@"
	Current=   "@plot@"
	Output=    "@log@"
}


Electrode {
  { Name= "Gate"  Voltage= (0.0 at 0, @Vg@ at 1) material=PolySi(N)}
  { Name= "Emitter" Voltage=0.0 }
  { Name= "Collector" Voltage=(0.0 at 1, 10 at 10) }
}

Physics {
	Recombination (
		SRH(DopingDep)
		Auger
	)	
	Aniso ( 
		Mobility
	)	
	Mobility (
		DopingDependence
		HighFieldSaturation
		Enormal
	)	
	IncompleteIonization
	EffectiveIntrinsicDensity ( oldSlotboom)

}

Math {
	
	ExtendedPrecision(80)
	Iterations= 15
	ErrRef(electron) = 1e-10
	ErrRef(hole) = 1e-10
	Extrapolate
	Transient= BE
	TensorGridAniso
	RHSmin= 1e-30
	RHSmax= 1e30
	RHSFactor= 1e30
	CDensityMin= 1e-30
	
	eMobilityAveraging= ElementEdge 
	hMobilityAveraging= ElementEdge 
	ElementAvalancheMinAngle=0. 
	
	Method = Super
	ExitOnFailure
}


Plot {
   TotalCurrentDensity/vector
   eDensity hDensity
   eCurrent hCurrent
   ElectricField/vector
   eQuasiFermi hQuasiFermi
   egradQuasiFermi hgradQuasiFermi
   Potential Doping SpaceCharge
   SRH Auger
   AvalancheGeneration
   eAvalanche hAvalanche
   eMobility hMobility
   DonorConcentration AcceptorConcentration
   Doping
   eVelocity hVelocity
   BarrierTunneling
   ConductionBandEnergy ValenceBandEnergy BandGap
   eQuasiFermi hQuasiFermi
}

Solve {
	Coupled(Iterations= 10000 LineSearchDamping= 1e-4){ Poisson }
    Coupled(Iterations= 10000 LineSearchDamping= 1e-4){ Poisson Electron Hole}
	Transient (
		InitialTime= 0 InitialStep= 1e-8 Minstep = 1e-10 Increment= 1.4 Decrement= 2
		Maxstep= 0.2 FinalTime= 10
		){ Coupled { Poisson Electron Hole } }
}









