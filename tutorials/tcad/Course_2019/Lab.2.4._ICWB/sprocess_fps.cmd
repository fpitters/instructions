math coord.ucs

#-----------Enter icwb command------------#
icwb filename= "NMOS_lyt.mac" scale=1e-3
icwb domain= SIM3D 


fset Ymin [icwb bbox left ]
fset Ymax [icwb bbox right]
fset Zmin [icwb bbox back ]
fset Zmax [icwb bbox front]


line x location= 0.0      spacing= 1.0<nm>  tag=SiTop        
line x location= 0.05     spacing= 0.01                    
line x location= 0.5      spacing= 0.05                      
line x location= 2        spacing= 0.2   
line x location= 5        spacing= 2   tag=SiBottom 

line y location= $Ymin   spacing= 100.0<nm> tag= SiLeft
line y location= $Ymax   spacing= 100.0<nm> tag= SiRight
line z location= $Zmin   spacing= 100.0<nm> tag= SiFront
line z location= $Zmax   spacing= 100.0<nm> tag= SiBack


region silicon xlo= SiTop xhi= SiBottom  ylo= SiLeft yhi= SiRight  zlo= SiFront  zhi= SiBack
init concentration=1e15 field=Phosphorus  !DelayFullD 

#---------Create PBASE mask here----------#
icwb.create.mask layer.name= PBASE name= p_base polarity= negative
deposit resist thickness= 5
etch resist mask= p_base thickness= 5.5 anisotropic

implant  Boron  dose=2.0e13<cm-2>  energy=200<keV> tilt=0 rotation=0  
implant  Boron  dose=1.0e13<cm-2>  energy= 80<keV> tilt=0 rotation=0  
implant  Boron  dose=2.0e12<cm-2>  energy= 25<keV> tilt=0 rotation=0

strip resist

diffuse temperature=1050 time=10.0<s>

grid set.min.normal.size=1<nm> set.normal.growth.ratio.2d=1.5

deposit Oxide thickness= 3<nm>
deposit material= {PolySilicon} type=anisotropic time=1 rate= {0.18}


#---------Create POLY mask here-----------#
icwb.create.mask layer.name= POLY name= g_poly polarity= positive
deposit resist thickness= 5
etch resist mask= g_poly thickness= 6 anisotropic
etch PolySilicon mask= g_poly thickness= 0.2 anisotropic 
etch Oxide  mask= g_poly thickness= 0.1 anisotropic
strip resist

deposit Oxide thickness= 8<nm>

refinebox Silicon min= {0.0 $Ymin/2 $Zmin} max= {0.3 $Ymax $Zmax} xrefine= {0.01 0.02 0.05} yrefine= {0.1 0.1 0.1} zrefine= {0.1 0.1 0.1} add
refinebox remesh

implant Arsenic dose=4e14<cm-2> energy=10 tilt=0 rotation=0
diffuse temperature=1050 time=0.1<s>

implant Boron dose=0.25e13<cm-2> energy=20<keV> tilt=30 rotation=0            
implant Boron dose=0.25e13<cm-2> energy=20<keV> tilt=30 rotation=90   
implant Boron dose=0.25e13<cm-2> energy=20<keV> tilt=30 rotation=180  
implant Boron dose=0.25e13<cm-2> energy=20<keV> tilt=30 rotation=270 


diffuse temperature=1050 time=5.0<s>

deposit material= {Nitride} type=isotropic   time=1 rate= {0.06}
etch    material= {Nitride} type=anisotropic time=1 rate= {0.084} isotropic.overetch=0.01
etch    material= {Oxide}   type=anisotropic time=1 rate= {0.01} 

implant Arsenic dose=5e15<cm-2> energy=10 tilt=0 rotation= 0
diffuse temperature=1050<C> time=10.0<s>

#-----------Create CONTACT mask ---------#
icwb.create.mask layer.name= CONTACT name= c_contact polarity= negative
deposit Aluminum anisotropic thickness= 0.1 mask= c_contact


struct tdr= n@node@_NMOS !Gas

