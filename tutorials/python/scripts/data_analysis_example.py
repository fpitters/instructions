#! /usr/local/bin/python3

import numpy as np
import pandas as pd
import scipy.optimize as opt
import matplotlib.pyplot as plt

from matplotlib import cm, rcdefaults



# Getting data into python
# -----------------------------

## Various ways of getting data in
dat_arange = np.arange(0, 20, 2)
dat_array = np.array([[1,1], [2,4], [3,9], [4,16], [5,25]])
dat_txt = np.genfromtxt('../data/test_data.txt')
dat_csv = pd.read_csv('../data/test_data.csv')

## print something
print("I know two digits of Pi: \t %.2f.\nOther people know many more: \t %.30f." % (np.pi, np.pi))
print("\n")


# Dei=fine a function
# -----------------------------

def ffit(x, a, b, c):
    return a/(x-b) + c


# Analyse the data
# -----------------------------

print("Some random array manipulations.")

for i in range(len(dat_arange)):
    if (i == 7):
        dat_arange[i] += 1
    
for d in dat_arange:    
    if (d%2 == 1):
        print("%d" % d)
    
## fit


# Plotting the results
# -----------------------------

# Style list available at
# https://matplotlib.org/3.1.3/gallery/style_sheets/style_sheets_reference.html
plt.style.use('fast') 

## graph
x = np.arange(1, 30, 0.5)
plt.plot(x, ffit(x, 1, 0.5, 2.0), ls=':', marker='s', label='line 1')
plt.plot(x, ffit(x, 1, 0.5, 1.8), ls=':', marker='o', label='line 2')
plt.plot(x, ffit(x, 1, 0.5, 1.6), ls=':', marker='^', label='line 3')
plt.plot(x, ffit(x, 1, 0.5, 1.4), ls=':', marker='v', label='line 4')
plt.legend(loc='upper right')
plt.xlabel('x label [unit]')
plt.ylabel('y label [unit]')
plt.savefig('../' + 'graph.pdf', bbox_inches='tight')
plt.clf()

## graph with colormap
number_of_lines = 4
cm_subsection = np.linspace(0., 1., number_of_lines) 
colors = [ cm.cividis(x) for x in cm_subsection ]
plt.plot(x, ffit(x, 1, 0.5, 2.0), ls=':', marker='s', color=colors[0], label='line 1')
plt.plot(x, ffit(x, 1, 0.5, 1.8), ls=':', marker='o', color=colors[1], label='line 2')
plt.plot(x, ffit(x, 1, 0.5, 1.6), ls=':', marker='^', color=colors[2], label='line 3')
plt.plot(x, ffit(x, 1, 0.5, 1.4), ls=':', marker='v', color=colors[3], label='line 4')
plt.legend(loc='upper right')
plt.xlabel('x label [unit]')
plt.ylabel('y label [unit]')
plt.savefig('../' + 'graph_with_cmap.pdf', bbox_inches='tight')
plt.clf()


## hist
x1 = np.random.normal(loc=50, scale=5, size=10000)
x2 = np.random.normal(loc=48, scale=10, size=10000)
plt.hist(x1, bins=50, range=(0, 100), histtype='step', label='dist1') 
plt.hist(x2, bins=50, range=(0, 100), histtype='step', label='dist2') 
plt.legend(loc='upper right')
plt.xlabel('x label [unit]')
plt.ylabel('y label [unit]')
plt.savefig('../' + 'hist.pdf', bbox_inches='tight')
plt.clf()

## map
plt.hist2d(x1, x2, bins=100, range=[(0, 100),(0, 100)], cmin=1, cmap='cividis') 
plt.xlabel('x label [unit]')
plt.ylabel('y label [unit]')
plt.colorbar(label='z label [unit]')
plt.savefig('../' + 'map.pdf', bbox_inches='tight')
plt.clf()

