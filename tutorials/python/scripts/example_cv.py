#! /usr/local/bin/python3

import numpy as np
import scipy.optimize as opt
import matplotlib.pyplot as plt


## Load example data
dat = np.genfromtxt('../data/data_cv.txt')

## function to get depletion voltage
def extract_depletion_voltage(volts, caps, caps_err, v1_low, v1_up, v2_low, v2_up):
    """
    Returns depletion voltage in [V] from CV curve.

    volts    ... volts in [V]
    caps     ... capacitance in [F]
    caps_err ... capacitance error in [F]
    v1_low   ... lower fit range for first fit in [V]
    v1_up    ... upper fit range for first fit in [V]
    v2_low   ... lower fit range for second fit in [V]
    v2_up    ... upper fit range for second fit in [V]
    """

    x = volts
    y = 1. / caps**2
    y_err = 2*caps_err / caps**3

    x1 = []
    x2 = []
    y1 = []
    y2 = []
    for i in range(len(x)):
        if (x[i] >= v1_low and x[i] <= v1_up):
            x1.append(x[i])
            y1.append(y[i])
        if (x[i] >= v2_low and x[i] <= v2_up):
            x2.append(x[i])
            y2.append(y[i])

    r1 = np.polyfit(x1, y1, 1)
    r2 = np.polyfit(x2, y2, 1)

    vdep = (r2[1] - r1[1]) / (r1[0] - r2[0])

    return vdep, r1, r2


## Plot some data
cv = 0
dep = 1

func = [cv, dep]
names = ['cv', 'dep']

for i in range(0, len(func)):
    f = func[i]
    fn = names[i]

    if f == cv:
        ## Single cv curves with errorbars
        v1 = np.array([v for v in dat if v[1] == 30])[:, 0]
        v2 = np.array([v for v in dat if v[1] == 31])[:, 0]
        c1 = np.array([v for v in dat if v[1] == 30])[:, 2]
        c2 = np.array([v for v in dat if v[1] == 31])[:, 2]
        e1 = np.array([v for v in dat if v[1] == 30])[:, 3]
        e2 = np.array([v for v in dat if v[1] == 31])[:, 3]

        plt.errorbar(v1, c1, yerr=e1, ls='--', marker='s', label='pad 30')
        plt.errorbar(v2, c2, yerr=e2, ls=':', marker='d', label='pad 31')
        plt.legend(loc='upper right')

        TITLE = "Capacitance vs Voltage"
        XLABEL = 'Voltage [V]'
        YLABEL = 'Capacitance [pF]'


    elif f == dep:
        ## Single 1/c^2 curve with errorbars
        v1 = np.array([v for v in dat if v[1] == 30])[:, 0]
        c1 = np.array([v for v in dat if v[1] == 30])[:, 2]
        e1 = np.array([v for v in dat if v[1] == 30])[:, 3]
        plt.errorbar(v1, 1./c1**2, yerr=e1*2./c1**3, ls='--', marker='s', ms=8, label='data pad 30')
        
        ## Two linear fits to the two (presumable) linear regions
        ## Regions are set by hand and last data point at 450V is ignored
        vdep, r1, r2 = extract_depletion_voltage(v1, c1, e1, 25, 160, 230, 420)
        plt.plot(v1, r1[1]+v1*r1[0], ls='-', c='r', label='linear fit')
        plt.plot(v1, r2[1]+v1*r2[0], ls='-', c='r')
        
        print("Sensor fully depleted at: %dV" % vdep)
        
        plt.ylim([0, 0.0008])
        plt.legend(loc='lower right')
        
        TITLE = "1/C^2 vs Voltage"
        XLABEL = 'Voltage [V]'
        YLABEL = '1/C^2 [a.U.]'


    else:
        pass

    plt.xlabel(XLABEL)
    plt.ylabel(YLABEL)
    plt.title(TITLE)
    
    ## Save and reset fig for next plot
    plt.savefig('../' + fn + '.pdf', bbox_inches='tight')
    plt.clf()
