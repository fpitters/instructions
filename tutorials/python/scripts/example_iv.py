#! /usr/local/bin/python3

import numpy as np
import scipy.optimize as opt
import matplotlib.pyplot as plt

## Load example data
dat = np.genfromtxt('../data/data_iv.txt')

## Plot some data
iv = 0
iv_tot = 1

func = [iv, iv_tot]
names = ['iv', 'iv_tot']

for i in range(0, len(func)):
    f = func[i]
    fn = names[i]

    if f == iv:
        ## Single iv curves with errorbars
        v1 = np.array([v for v in dat if v[1] == 30])[:, 0]
        v2 = np.array([v for v in dat if v[1] == 31])[:, 0]
        i1 = np.array([v for v in dat if v[1] == 30])[:, 2]
        i2 = np.array([v for v in dat if v[1] == 31])[:, 2]
        e1 = np.array([v for v in dat if v[1] == 30])[:, 3]
        e2 = np.array([v for v in dat if v[1] == 31])[:, 3]

        plt.errorbar(v1, i1, yerr=e1, ls='--', marker='s', label='pad 30')
        plt.errorbar(v2, i2, yerr=e2, ls=':', marker='d', label='pad 31')
        plt.legend(loc='lower right')

        TITLE = "Pad Current vs Voltage"
        XLABEL = 'Voltage [V]'
        YLABEL = 'Current [nA]'


    elif f == iv_tot:
        ## Average the total current over some pads
        vtot = np.array([v for v in dat if v[1] == 30])[:, 0]
        itot = np.mean([np.array([v for v in dat if v[1] == i])[:, 4] for i in range(100, 130, 1)], axis=0)
        
        plt.plot(vtot, itot, ls='-', marker='s', ms=8, label='average of last 30 pads')
        plt.legend(loc='lower right')
        
        TITLE = "Total Current vs Voltage"
        XLABEL = 'Voltage [V]'
        YLABEL = 'Total Current [nA]'


    else:
        pass

    plt.xlabel(XLABEL)
    plt.ylabel(YLABEL)
    plt.title(TITLE)
    
    ## Save and reset fig for next plot
    plt.savefig('../' + fn + '.pdf', bbox_inches='tight')
    plt.clf()
