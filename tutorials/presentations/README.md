## Presentations 101

Some basic remarks on how to present scientific findings in a useful way. Instructions are intended for a working group meeting, NOT a conference or a public audience.

----

### How to create a useful presentation

Before you start 
- What is the aim of this presentation? What do you want to communicate?
- Who is your audience and how much background knowledge will they have? Are students present? 

Introduction: 
- What is the purpose of this presentation? What am I going to talk about?
- Is this talk a follow up to a previous presentation?

Setup:
- How does my measurement work? What is special about my setup that other setups might not have?
- How do I obtain the quantities I show? How do I obtain my uncertainties?

Results:
- Diagrams, results, uncertainties and conclusions/open questions.
- Do include uncertainties if somehow possible!
- Quote results and uncertainties with a useful number of digits, e.g. (3.17 +/- 0.18) m or (3.2 +/- 0.2) m and not (3.1532  +/- 0.1748) m.
- Think critical about your results!!

Outlook and Discussion:
- What are the next steps?



-----

### How to create a useful plot

Before you start 
- Why is that plot useful for my audience? What do I want to tell them and what will they learn from it?
- Is this really the best way to deliver my message?
- You might want to add a title or caption to your figure that explains the main take away.

Font size:
- Don't use too small fonts that no one in the back of the room can see!

Legend:
- Do include a legend!
- Use different colours, markers and/or linestyles for different curves. More people than you think are colour blind, so different colours are not sufficient.

Colours:
- If you want to show a trend in a 2D histogram, a binary colour map (e.g. greyscale, yellow to blue, etc.) is usually the best way to go.
- Same is usually true if too many lines are overlayed in one plot that represent a particular change of conditions (e.g. low to high bias voltage).
- Rainbow colour maps are seldom a useful presentation.

Axis:
- Do include a axis labels! This includes the observable that is shown but also the unit (e.g. current [mA]).
- Use a proper axis range. If all your measurement points are between 513 und 567, a range of 0 to 1000 will probably be less useful than 500 to 600. There might be exceptions to this though. Again, think about what people should learn from your plot. 
- Use proper units that people can relate to (e.g. 420 nA or 4.20E-7 A but not 0.00000042 A).

Markers:
- Use markers not just lines! People want to see where exactly your data points are. Lines can be useful to guide the eye and spot trends but can also be distracting. 

Uncertainties:
- Do show uncertainties on your data points if possible!
