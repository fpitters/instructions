/********************************************************************
* File: run.cc
* ------------------------
*
* Description:
* Template run
*
* Notes:
*
* Version:
* Author: Florian Pitters
*
*******************************************************************/

#include "hello.h"
#include "common.h"
#include "analysis.h"

using namespace std;


int main(int argc,  char **argv)
{
	bool ret = 0;
	bool fReadConfig = false;
	bool fPrintHelp = false;
	bool fAlgorithm = false;
	bool fVal = false;

	string algorithm = "";
	string configFile = "config/example.cfg";
	string val = "1";


	// Read arguments and determine what to do
	if (argc == 1){
		fPrintHelp = true;
	}
	else {
		for (int i = 1; i < argc; i += 2) {
			string option(argv[i]);
			if (option.compare("-h") == 0) fPrintHelp = true;
			if (option.compare("-?") == 0) fPrintHelp = true;
			if (option.compare("-a") == 0) fAlgorithm = true;
			if (option.compare("-c") == 0) fReadConfig = true;
			if (option.compare("-val") == 0) fVal = true;

			string argument(argv[i+1]);
			if (option.compare("-a") == 0) algorithm = argv[i+1];
			if (option.compare("-c") == 0) configFile = argv[i+1];
			if (option.compare("-val") == 0) val = argv[i+1];
		}
	}

	// Print help
	if (fPrintHelp) {
		cout << "Usage: " << argv[0] << " -a <algorithm> " << endl;
		cout << "\n" << endl;

		cout << "Avalable options:" << endl;
		cout << " -h           Display this help" << endl;
		cout << " -?           Display this help" << endl;
		cout << " -a           Determine algorithm to use" << endl;
		cout << " -c           Determine config file to read" << endl;
		cout << " -val         An example input variable" << endl;
		cout << "\n" << endl;

		cout << "Example commands:" << endl;
		cout << "./bin/run -a hello_world" << endl;
		cout << "./bin/run -a test -val 16" << endl;
		cout << "\n\n" << endl;
	}


	// Process arguments
	if (fAlgorithm) {
		if (algorithm == "hello_world") {
			hello_world();
		}
		else if (algorithm == "test"){
			ret = analyse();
		}
		else
			cout << "not a valid algorithm" << endl;

		printf("---->  Success!\n\n");
	}

	return 0;
}
