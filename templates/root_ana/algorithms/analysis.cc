/********************************************************************
 * File: analyse.cc
 * ------------------------
 *
 * Description:
 * Analyse something.
 *
 *
 * Version:
 * Author: Florian Pitters
 *
 *******************************************************************/

#include "analysis.h"


int analyse() {

    styleCommon();
    styleMap();
    styleGraph();

    Color_t kOneBlue = (new TColor(0/256., 43/256., 128/256.))->GetNumber();
    Color_t kOneMagenta = (new TColor(199/256., 68/256., 24/256.))->GetNumber();
    Color_t kOneOrange = (new TColor(221/256., 138/256., 13/256.))->GetNumber();
    Color_t kOneGreen = (new TColor(81/256., 153/256., 28/256.))->GetNumber();
    Color_t kOneCyan = (new TColor(24/256., 156/256., 199/256.))->GetNumber();
    Color_t kOneRed = (new TColor(109/256., 15/256., 14/256.))->GetNumber();

    Color_t kOneAzure = (new TColor(0/256., 153/256., 153/256.))->GetNumber();
    Color_t kOneOrange2 = (new TColor(221/256., 162/256., 13/256.))->GetNumber();
    Color_t kOneBlue2 = (new TColor(35/256., 75/256., 133/256.))->GetNumber();

    std::vector<Marker_t> markers = {kFullSquare, kFullCircle, kFullTriangleUp, kFullTriangleDown, kFullDiamond, kFullCrossX, kFullDoubleDiamond};
    std::vector<Style_t> styles = {1, 2, 4, 6, 7, 10};
    std::vector<Color_t> colours = {kOneBlue, kOneMagenta, kOneOrange, kOneGreen, kOneCyan, kOneOrange};



    // Preperations
    // ---------------------------------------

    int event;
    double val1, val2;
    std::string out_file;

    std::vector<float> x = {1, 2, 3, 4, 5, 6};
    std::vector<float> errx = {0, 0, 0, 0, 0, 0};
    std::vector<float> erry = {2, 1.8, 1.5, 1.3, 1.0, 1.0};
    std::vector<float> y1 = {18, 12, 8, 5, 4, 3};
    std::vector<float> y2 = {19, 13, 9, 6, 5, 4};
    std::vector<float> y3 = {20, 14, 10, 7, 6, 5};
    std::vector<float> y4 = {21, 15, 11, 8, 7, 6};
    std::vector<float> y5 = {22, 16, 12, 9, 8, 7};
    std::vector<float> y6 = {23, 17, 13, 10, 9, 8};



    // Analysis
    // --------------------------------------

    auto hist = new TH1F("hist", "hist", 100, 0, 500);
    auto hist2 = new TH1F("hist2", "hist2", 100, 0, 500);
    auto map = new TH2F("map", "map", 100, 0, 500, 100, 0, 500);

    TRandom rnd;
    for (int i = 0; i < 10000; ++i) {
        val1 = rnd.Gaus(250, 20);
        hist->Fill(val1);
        val2 = rnd.Gaus(260, 40);
        hist2->Fill(val2);
        map->Fill(val1, val2);
    }


    // Print
    // ---------------------------------------

    styleGraph();
    out_file = res_path + "hist.pdf";
    printHist(hist, out_file, "Title", "x value (unit)", "y value (unit)",-1.0, -1.0, 1.5);

    styleMap();
    out_file = res_path + "map.pdf";
    printMap(map, out_file, "Title", "x value (unit)", "y value (unit)", "z value (unit)", -1.0, -1.0, 1.1, 1.1);


    // Multigraph
    styleGraph();
    out_file = res_path + "mg.pdf";
    TCanvas *canv_mg = new TCanvas("mg", "mg");
    TMultiGraph *mg_mg = new TMultiGraph();
    TLegend *lg_mg = new TLegend(0.68, 0.61, 0.88, 0.88); //
    addErrGraph(mg_mg, lg_mg, x.size(), &x[0], &y6[0], &errx[0], &erry[0], colours.at(5), markers.at(5), 1.2, 1, "lp", " label 6");
    addErrGraph(mg_mg, lg_mg, x.size(), &x[0], &y5[0], &errx[0], &erry[0], colours.at(4), markers.at(4), 1.2, 1, "lp", " label 5");
    addErrGraph(mg_mg, lg_mg, x.size(), &x[0], &y4[0], &errx[0], &erry[0], colours.at(3), markers.at(3), 1.2, 1, "lp", " label 4");
    addErrGraph(mg_mg, lg_mg, x.size(), &x[0], &y3[0], &errx[0], &erry[0], colours.at(2), markers.at(2), 1.2, 1, "lp", " label 1");
    addErrGraph(mg_mg, lg_mg, x.size(), &x[0], &y2[0], &errx[0], &erry[0], colours.at(1), markers.at(1), 1.2, 1, "lp", " label 2");
    addErrGraph(mg_mg, lg_mg, x.size(), &x[0], &y1[0], &errx[0], &erry[0], colours.at(0), markers.at(0), 1.2, 1, "lp", " label 3");
    //mg_mg->Draw("A CP PLC PMC PFC");
    mg_mg->Draw("A");
    mg_mg->SetTitle("Title");
    mg_mg->GetXaxis()->SetTitle("x label (unit)");
    mg_mg->GetYaxis()->SetTitle("y label (unit)");
    mg_mg->GetYaxis()->SetTitleOffset(1.1);
    mg_mg->GetXaxis()->SetLimits(0, 7);
    mg_mg->SetMinimum(0);
    mg_mg->SetMaximum(25);
    lg_mg->Draw();
    canv_mg->SaveAs(out_file.c_str());

    // Multihist
    styleGraph();
    out_file = res_path + "hist.pdf";
    TCanvas *canv_hist = new TCanvas("hist", "hist");
    THStack *hs_hist = new THStack();
    TLegend *lg_hist = new TLegend(0.68, 0.79, 0.88, 0.88);
    addHist(hs_hist, lg_hist, hist, colours.at(1), markers.at(1), 1, "lp", " label 6", 1);
    addHist(hs_hist, lg_hist, hist2, colours.at(0), markers.at(0), 1, "lp", " label 5", 1);
    hs_hist->Draw("nostack E PLC PMC");
    hs_hist->SetTitle("Title");
    hs_hist->GetXaxis()->SetTitle("x label (unit)");
    hs_hist->GetYaxis()->SetTitle("y label (unit)");
    hs_hist->GetYaxis()->SetTitleOffset(1.3);
    hs_hist->GetXaxis()->SetLimits(0, 500);
    lg_hist->Draw();
    canv_hist->SaveAs(out_file.c_str());

    auto gr1 = std::make_shared<TGraph> (x.size(), &x[0], &y1[0]);
    test_smart_pointer(gr1);

    TGraph gr2(x.size(), &x[0], &y1[0]);
	test_call_by_reference(gr2);

    return 0;
}
