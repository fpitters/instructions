/********************************************************************
 * File: common.cc
 * ------------------------
 *
 * Description:
 * Common functions.
 *
 * Version:
 * Author: Florian Pitters
 *
 *******************************************************************/

#include "common.h"

using namespace std;

// --------------------------------------------------------
// Defintions
// --------------------------------------------------------


void test_smart_pointer(std::shared_ptr<TGraph> gr) {
    auto canv = new TCanvas("a", "a");
    //auto canv = std::make_shared<TCanvas>("a", "a");
    gr->Draw("APC");
    gr->SetTitle("title");
    gr->GetXaxis()->SetTitle("x");
    gr->GetYaxis()->SetTitle("y");
    gr->SetMarkerStyle(20);
    gr->SetMarkerSize(0.8);
    canv->SaveAs("~/test.pdf");

	delete canv;
}

void test_call_by_reference(TGraph& gr) {
    auto canv = new TCanvas("a", "a");
    gr.Draw("APC");
    gr.SetTitle("title");
    gr.GetXaxis()->SetTitle("x");
    gr.GetYaxis()->SetTitle("y");
    gr.SetMarkerStyle(20);
    gr.SetMarkerSize(0.8);
    canv->SaveAs("~/test2.pdf");

	delete canv;
}


/* gauss */
float gauss(float* x, float* par) {
    float A, mu, sigma;
    static float g;

    A = par[0];
    mu = par[1];
    sigma = par[2];

    g = A * exp(-pow(x[0] - mu, 2) / (2. * pow(sigma, 2)));

    return g;
}

float mean(std::vector<float> vals){
	float m = 0;
	float n = 0;
	for (int i = 0; i < vals.size(); i++){
		if (!isnan(vals.at(i))){
			m += vals.at(i);
			n += 1;
		}
	}
	m /= n;

	return m;
}

float var(std::vector<float> vals) {
	float v = 0;
	float m = 0;
	float n = 0;
	m = mean(vals);

	for (int i = 0; i < vals.size(); i++){
		if (!isnan(vals.at(i))){
			v += pow((m - vals.at(i)), 2);
			n += 1;
		}
	}

	v /= float(n);

	return v;
}

float mean_last_x(std::vector<float> vals, int x) {
	float m = 0;
	float n = 0;

    if (vals.size() < x || x == 0){
        return -1;
    }

	for (int i = 0; i < x; i++){
		if (!isnan(vals.at(vals.size()-1-i))) {
			m += vals.at(vals.size()-1-i);
			n += 1;
		}
	}
	m /= n;

	return m;
}

float var_last_x(std::vector<float> vals, int x) {
	float v = 0;
	float m = 0;
	float n = 0;

    if (vals.size() < x || x == 0){
        return -1;
    }
	m = mean_last_x(vals, x);

	for (int i = 0; i < x; i++){
		if (!isnan(vals.at(vals.size()-1-i))){
			v += pow(m - vals.at(vals.size()-1-i), 2);
			n += 1.;
		}
	}

	v /= n;

	return v;
}


int find_first_above_x(std::vector<float> vals, float thr, bool reverse)
{
	int it = 0;
	if (reverse == true){
		for (int i = vals.size()-1; i < 0; i--){
			it++;
			if (vals.at(i) > thr){ break; }
		}
		it = vals.size()-1 - it;
	}
	else {
		for (int i = 0; i < vals.size(); i++){
			it++;
			if (vals.at(i) > thr){ break; }
		}
	}

	return it;
}

int find_extremum(std::vector<float> vals, int direction) {
	int it = 0;
	float vmax = 0;
	for (int i = 0; i < vals.size(); i++){
		if (direction > 0){
			if (vals.at(i) > vmax) { vmax = vals.at(i); it = i; }
		}
		else {
			if (vals.at(i) < vmax) { vmax = vals.at(i); it = i; }
		}
	}

	return it;
}

void sort_array(std::vector<float> vals) {
    int i, j;
    float max;

    max = vals.at(0);
    for(j = 0; j < vals.size() + 1; j++) {
        max = vals.at(j);
        for(i = j; i < vals.size() + 1; i++) {
            if(vals.at(i) > max) {
                max = vals.at(i);
                vals.at(i) = vals.at(j);
            }
            vals.at(j) = max;
        }
    }
}


double find_closest_val(std::vector<double> vals, double x) {
    int j = 0;
    for(int i = 0; i < vals.size(); i++) {
        if(abs(x - vals.at(i)) < abs(x - vals.at(j))) {
            j = i;
        }
    }
    return vals.at(j);
}

int find_closest_bin(std::vector<double> vals, double x) {
    int j = 0;
    for(int i = 0; i < vals.size(); i++) {
        if(abs(x - vals.at(i)) < abs(x - vals.at(j))) {
            j = i;
        }
    }
    return j;
}




// ------------------------------------------------------------------------------
// File functions
// ------------------------------------------------------------------------------

int read_file(std::string fn, char delim, std::vector<std::vector<float>> &dat, bool info)
{
	std::ifstream f;
	f.open(fn);
	dat.clear();

	// check if file is open
	if (!f.is_open()) {
		std::cout << "Cannot open input file:\n\t" << fn << std::endl;
		return 1;
	}
	else {
		if (info == 1) {
			std::cout << "Reading data from file:\n\t" << fn << std::endl;
		}
	}

	// read file line by line
	int i = 0;
	std::string line;
	while (!f.eof()) {
	 	std::getline(f, line);

		// check if line is empty or a comment
		// if not write to output vector
		if (line.size() > 0 && isdigit(line.at(0))) {
			std::stringstream ss(line);
			std::string word;
			std::vector<float> row;
			while (std::getline(ss, word, delim)) {
				i += 1;
				row.push_back(stof(word));
			}
			dat.push_back(row);
		}
    }

	// debug info
	if (info == 1) {
		std::cout << "Number of elements found: " << i << " Number of Rows found: " << dat.size() << std::endl;
	}

	f.close();

	return 0;
}



int write_file(std::string fn, char delim, std::vector<std::vector<float>> &dat, std::string hd, bool info)
{
	std::ofstream f;
	f.open(fn, std::ofstream::trunc);

	// check if file is open
	if (!f.is_open()) {
		std::cout << "Cannot open output file:\n\t" << fn << std::endl;
		exit(1);
	}
	else {
		std::cout << "Writing data to file:\n\t" << fn << std::endl;
	}

	// write header
	if (hd != "") {
		f << hd << std::endl;
	}
	f << std::setprecision(6);

	// write data line by line
	int k = 0;
	int nrow = dat.size();
	int ncol = dat[0].size();
	for (int j = 0; j < nrow; j++) {
        for (int i = 0; i < ncol; i++) {
            f << dat[j][i] << delim;
			k += 1;
        }
		if (j != (nrow-1)){
			f << std::endl;
		}
    }

	// debug info
	if (info == 1) {
		std::cout << "Number of elements wrote: " << k << " Number of Rows wrote: " << dat.size() << std::endl;
	}

	f.close();

	return 0;
}




// ------------------------------------------------------------------------------
// Printing functions
// ------------------------------------------------------------------------------


void printMap(TH2* hist2d, string fn, string title, string titleX, string titleY, string titleZ, float limX, float limY, float offsetY,
              float offsetZ, string option) {
    auto canv = new TCanvas(fn.c_str(), fn.c_str());
    hist2d->Draw(option.c_str());
    hist2d->SetTitle(title.c_str());
    hist2d->GetXaxis()->SetTitle(titleX.c_str());
    hist2d->GetYaxis()->SetTitle(titleY.c_str());
    hist2d->GetZaxis()->SetTitle(titleZ.c_str());
    hist2d->GetXaxis()->SetTitleOffset(1.1);
    hist2d->GetYaxis()->SetTitleOffset(offsetY);
    hist2d->GetZaxis()->SetTitleOffset(offsetZ);
    hist2d->SetMarkerSize(0.1);
    if(limX != -1) hist2d->SetMinimum(limX);
    if(limY != -1) hist2d->SetMaximum(limY);
	canv->SaveAs(fn.c_str());
	delete canv;
}

void printHist(TH1* hist1d, string fn, string title, string titleX, string titleY, float limX, float limY, float offset, string option) {
    auto canv = new TCanvas(fn.c_str(), fn.c_str());
    hist1d->Draw(option.c_str());
    hist1d->SetTitle(title.c_str());
    hist1d->GetXaxis()->SetTitle(titleX.c_str());
    hist1d->GetYaxis()->SetTitle(titleY.c_str());
    hist1d->GetXaxis()->SetTitleOffset(1.1);
    hist1d->GetYaxis()->SetTitleOffset(offset);
    if(limX != -1 || limY != -1) hist1d->SetAxisRange(limX, limY);
    canv->SaveAs(fn.c_str());
	delete canv;
}

void printGraph(TGraphErrors* gr, string fn, string title, string titleX, string titleY, float limX, float limY, float offset, string option) {
    auto canv = new TCanvas(fn.c_str(), fn.c_str());
    gr->Draw(option.c_str());
    gr->SetTitle(title.c_str());
    gr->GetXaxis()->SetTitle(titleX.c_str());
    gr->GetYaxis()->SetTitle(titleY.c_str());
    gr->GetYaxis()->SetTitleOffset(offset);
    gr->SetMarkerStyle(20);
    gr->SetMarkerSize(0.8);
    if(limX != -1.0) gr->SetMinimum(limX);
    if(limY != -1.0) gr->SetMaximum(limY);
	canv->SaveAs(fn.c_str());
	delete canv;
}

void printGraph(TGraph* gr, string fn, string title, string titleX, string titleY, float limX, float limY, float offset, string option) {
    auto canv = new TCanvas(fn.c_str(), fn.c_str());
    gr->Draw(option.c_str());
    gr->SetTitle(title.c_str());
    gr->GetXaxis()->SetTitle(titleX.c_str());
    gr->GetYaxis()->SetTitle(titleY.c_str());
    gr->GetYaxis()->SetTitleOffset(offset);
    gr->SetMarkerStyle(20);
    gr->SetMarkerSize(0.8);
    if(limX != -1.0) gr->SetMinimum(limX);
    if(limY != -1.0) gr->SetMaximum(limY);
	canv->SaveAs(fn.c_str());
	delete canv;
}

void addGraph(TMultiGraph* mg, TLegend* lg, int nentries, float* x, float* y,
        Color_t c, Style_t m, Size_t ms, Style_t ls, string style, string label) {
    TGraph* gr = new TGraph(nentries, x, y);
    gr->SetLineColor(c);
    gr->SetLineStyle(ls);
    gr->SetMarkerColor(c);
    gr->SetMarkerStyle(m);
    gr->SetMarkerSize(ms);
    mg->Add(gr);
    lg->AddEntry(gr, label.c_str(), style.c_str());
}

void addErrGraph(TMultiGraph* mg, TLegend* lg, int nentries, float* x, float* y, float* x_err, float* y_err,
        Color_t c, Style_t m, Size_t ms, Style_t ls, string style, string label) {
    TGraphErrors* gr = new TGraphErrors(nentries, x, y, x_err, y_err);
    gr->SetLineColor(c);
    gr->SetLineStyle(ls);
    gr->SetMarkerColor(c);
    gr->SetMarkerStyle(m);
    gr->SetMarkerSize(ms);
    gr->SetFillStyle(3002);
    gr->SetFillColor(1);
    mg->Add(gr);
    lg->AddEntry(gr, label.c_str(), style.c_str());
}

void addFittedGraph(TMultiGraph* mg, TLegend* lg, int nentries, float* x, float* y,
        Color_t c, Style_t m, Size_t ms, Style_t ls, string style, string label, string func, Float_t fit_low, Float_t fit_up) {
    TGraph* gr = new TGraph(nentries, x, y);
    gr->SetLineColor(c);
    gr->SetLineStyle(ls);
    gr->SetMarkerColor(c);
    gr->SetMarkerStyle(m);
    gr->SetMarkerSize(ms);
    gr->Fit(func.c_str(), "me", "", fit_low, fit_up);
    mg->Add(gr);
    lg->AddEntry(gr, label.c_str(), style.c_str());
}

void addScatterGraph(TLegend* lg, TH2* scatter,
        Color_t c, Style_t m, Size_t ms, string style, string label) {
    scatter->SetLineColor(c);
    scatter->SetMarkerStyle(m);
    scatter->SetMarkerColor(c);
    scatter->SetMarkerSize(ms);
    scatter->Draw("SAME");
    lg->AddEntry(scatter, label.c_str(), style.c_str());
}

void addHist(THStack* hs, TLegend* lg, TH1F* hist1d,
        Color_t c, Style_t m, Style_t ls, string style, string label, Float_t scale) {
    hist1d->SetLineColor(c);
    hist1d->SetLineStyle(ls);
    hist1d->SetStats(0);
    hist1d->Scale(scale, "nosw2");
    // hist1d->SetFillColorAlpha(kBlue - 3, 0.3);
    // hist1d->SetFillStyle(1001);
    // hist1d->Draw(style);
    hs->Add(hist1d);
    lg->AddEntry(hist1d, label.c_str(), style.c_str());
}


void styleMap() {

    /* set the margins */
    gStyle->SetPadBottomMargin(0.15);
    gStyle->SetPadTopMargin(0.10);
    gStyle->SetPadRightMargin(0.22);
    gStyle->SetPadLeftMargin(0.14);

    /* stats */
    gStyle->SetOptStat(0);
    gStyle->SetOptFit(111);

    /* palette */
    gStyle->SetPalette(113);  // 51 = Jet, 56 = Temperature, 112 = Viridis, 113 = Cividis

}


void styleGraph() {

    /* margins */
    gStyle->SetPadBottomMargin(0.15);
    gStyle->SetPadTopMargin(0.10);
    gStyle->SetPadRightMargin(0.10);
    gStyle->SetPadLeftMargin(0.18);

    /* stats */
    gStyle->SetOptStat(1111);
    gStyle->SetOptFit(111);

    /* palette */
    gStyle->SetPalette(113);  // 51 = Jet, 56 = Temperature, 112 = Viridis, 113 = Cividis

}



void styleCommon() {

    /* default white background for all plots */
    gROOT->SetStyle("Plain");

    /* canvas */
    gStyle->SetPaperSize(20, 25);
    gStyle->SetCanvasDefH(600);
    gStyle->SetCanvasDefW(800);

    // gStyle->SetCanvasColor(kWhite);
    // gStyle->SetFrameFillColor(kWhite);
    // gStyle->SetStatColor(kWhite);
    // gStyle->SetPadColor(kWhite);
    // gStyle->SetFillColor(kWhite);
    // gStyle->SetTitleFillColor(kWhite);
    gStyle->SetDrawBorder(0); // no yellow border around histogram
    gStyle->SetCanvasBorderMode(0); // remove border of canvas

    /* offsets */
    gStyle->SetLabelOffset(0.015, "xyz");
    gStyle->SetTitleOffset(1.1, "x");
    gStyle->SetTitleOffset(1.1, "yz");

    /* default text size */
    gStyle->SetTextSize(0.05);
    gStyle->SetTitleSize(0.055, "xyz");
    gStyle->SetLabelSize(0.050, "xyz");
    gStyle->SetStatFontSize(0.035);
    gStyle->SetLegendTextSize(0.040);

    /* fonts */
    int font = 42;
    gStyle->SetTitleFont(font);
    gStyle->SetStatFont(font);
    gStyle->SetTextFont(font);
    gStyle->SetLabelFont(font, "xyz");
    gStyle->SetTitleFont(font, "xyz");
    gStyle->SetTitleBorderSize(0);
    gStyle->SetStatBorderSize(1);
    gStyle->SetLegendFont(font);

    /* title and stuff */
    gStyle->SetOptTitle(0);  // remove histogramm title
    gStyle->SetOptDate(0); // print date on canvas

    /* stat box */
    gStyle->SetStatBorderSize(2);
    gStyle->SetStatFormat("6.4f");
    gStyle->SetFitFormat("6.4f");
    gStyle->SetStatH(0.20);
    gStyle->SetStatW(0.18);

    /* legend box */
    gStyle->SetLegendBorderSize(0);
    gStyle->SetLegendFillColor(kWhite);

    /* frame */
    gStyle->SetFrameLineWidth(2);

    /* histogrammes */
    gStyle->SetHistLineWidth(2);
    gStyle->SetHistLineColor(kBlack);
    gStyle->SetHistLineStyle(0);

    /* graphs */
    gStyle->SetLineStyle(1);
    gStyle->SetLineWidth(2);

    /* fits */
    gStyle->SetFuncWidth(2);
    gStyle->SetFuncColor(kRed);

    /* errorbars */
    gStyle->SetEndErrorSize(0);
    gStyle->SetTickLength(0.02, "xyz");

    /* markers */
    gStyle->SetMarkerStyle(kFullCircle);
    gStyle->SetMarkerSize(1);

    /* grid */
    gStyle->SetPadGridX(0);
    gStyle->SetPadGridY(0);
    gStyle->SetGridStyle(3);

    /* ticks */
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetNdivisions(506, "xyz");

    /* palette */
    gStyle->SetPalette(113);  // 51 = Jet, 56 = Temperature, 112 = Viridis, 113 = Cividis

    gROOT->ForceStyle();
}
