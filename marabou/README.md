# Instructions for Marabou and Gams
Instructions on how to setup Marabou for Gams

## Prepare Ubuntu
I will assume Ubuntu is used and will build everything in $HOME/Applications
```bash
cd ~
mkdir Applications
sudo apt-get install git make cmake automake build-essential dkms linux-headers-$(uname -r)
```

## Install ROOT
Instructions from https://root.cern.ch/building-root.
```bash
cd ~/Applications
sudo apt-get install dpkg-dev g++ gcc binutils libx11-dev libxpm-dev libxft-dev libxext-dev
sudo apt-get install gfortran libssl-dev libpcre3-dev xlibmesa-glu-dev libglew1.5-dev libftgl-dev libmysqlclient-dev libfftw3-dev libcfitsio-dev graphviz-dev libavahi-compat-libdnssd-dev libldap2-dev python-dev libxml2-dev libkrb5-dev libgsl0-dev libqt4-dev
git clone http://github.com/root-project/root.git && cd root
mkdir build && cd build
cmake ../ -DCMAKE_INSTALL_PREFIX=../
make install -j4
```

Add rootsys variable
```bash
export ROOTSYS=$HOME/Applications/root
```

Source environment 
```bash
source $ROOTSYS/bin/thisroot.sh
```

## Install Marabou
Instructions modified from https://svn.physik.uni-muenchen.de/repos/marabou/html/howTo/Marabou/HowToInstall.html. Docs are on https://www-old.mll-muenchen.de/marabou/htmldoc/.

Get source from svn repo
```bash
cd ~/Applications
sudo apt-get install subversion
svn co https://svn.physik.uni-muenchen.de/repos/marabou/trunk/marabou marabou && cd marabou
svn up
svn info
```

Get ppc-pro from garching server. No idea where this comes from.
```bash
scp -r gams@gar-sv-login01.garching.physik.uni-muenchen.de:/project/mll-code/ppc-pro ~/Applications/ppc-pro
```

Prepare iniMrbPath in $HOME/Applications/marabou
```
SYSMODE=y
ROOTSYS=$HOME/Applications/root                           # where your ROOT installation resides
MARABOU=$HOME/Applications/marabou                        # normally your current working directory
MARABOU_PPCDIR=$HOME/Applications/ppc-pro                 # where libs and includes for the PPC reside
MARABOU_INSTALL_PATH=$HOME/Applications/marabou/install
```
Note that the iniMrbPath overwrites the ROOT environment. I left it like this but make sure the ROOTSYS variable is identical.

Source environment and compile
```bash
source iniMrbPath
make install -j4
```

After the installation is done, edit iniMrbPath again
```
SYSMODE=n
```

## Set up new working directory
Set up new working directory by cloning the files folder from this repo. These files are specific to one experimental setup.
```bash
cd ~/Documents
mkdir example && cd example
git clone https://github.com/twentycrooks/instructions.git test
mv test/marabou_for_johanna/files/* ./
rm -rf test
cp $MARABOU/iniMrbPath ./
```

Copy the iniMrbPath from the installation folder.
```bash
cp $MARABOU/iniMrbPath ./
```

Once everything is set, copy your data files to the folder. Then run
```bash
source iniMrbPath
root Config.C
make -f GamsAnalyze.mk clean all
C_analyze
```
There should be no error messages anymore.

