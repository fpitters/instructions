///////////////////////////////////////////////////////////
//  Macro for colors and bells                           //
///////////////////////////////////////////////////////////

 const char setblack[]	=	"\033[30m";
 const char setred[]		=	"\033[31m";
 const char setgreen[]	=	"\033[32m";
 const char setyellow[]	=	"\033[33m";
 const char setblue[] 	=	"\033[34m";
 const char setmagenta[]=	"\033[35m";
 const char setcyan[] 	=	"\033[36m";
 const char bell[] 		=	"\007\007\007";
