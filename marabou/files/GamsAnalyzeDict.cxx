// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME GamsAnalyzeDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "GamsAnalyze.h"
#include "GamsAnalyzeGlobals.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_TMrbIOSpec(void *p = 0);
   static void *newArray_TMrbIOSpec(Long_t size, void *p);
   static void delete_TMrbIOSpec(void *p);
   static void deleteArray_TMrbIOSpec(void *p);
   static void destruct_TMrbIOSpec(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TMrbIOSpec*)
   {
      ::TMrbIOSpec *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TMrbIOSpec >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TMrbIOSpec", ::TMrbIOSpec::Class_Version(), "TMrbIOSpec.h", 31,
                  typeid(::TMrbIOSpec), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TMrbIOSpec::Dictionary, isa_proxy, 4,
                  sizeof(::TMrbIOSpec) );
      instance.SetNew(&new_TMrbIOSpec);
      instance.SetNewArray(&newArray_TMrbIOSpec);
      instance.SetDelete(&delete_TMrbIOSpec);
      instance.SetDeleteArray(&deleteArray_TMrbIOSpec);
      instance.SetDestructor(&destruct_TMrbIOSpec);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TMrbIOSpec*)
   {
      return GenerateInitInstanceLocal((::TMrbIOSpec*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TMrbIOSpec*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TUsrEvent(void *p = 0);
   static void *newArray_TUsrEvent(Long_t size, void *p);
   static void delete_TUsrEvent(void *p);
   static void deleteArray_TUsrEvent(void *p);
   static void destruct_TUsrEvent(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TUsrEvent*)
   {
      ::TUsrEvent *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TUsrEvent >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TUsrEvent", ::TUsrEvent::Class_Version(), "TUsrEvent.h", 34,
                  typeid(::TUsrEvent), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TUsrEvent::Dictionary, isa_proxy, 4,
                  sizeof(::TUsrEvent) );
      instance.SetNew(&new_TUsrEvent);
      instance.SetNewArray(&newArray_TUsrEvent);
      instance.SetDelete(&delete_TUsrEvent);
      instance.SetDeleteArray(&deleteArray_TUsrEvent);
      instance.SetDestructor(&destruct_TUsrEvent);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TUsrEvent*)
   {
      return GenerateInitInstanceLocal((::TUsrEvent*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TUsrEvent*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TUsrEvtStart(void *p = 0);
   static void *newArray_TUsrEvtStart(Long_t size, void *p);
   static void delete_TUsrEvtStart(void *p);
   static void deleteArray_TUsrEvtStart(void *p);
   static void destruct_TUsrEvtStart(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TUsrEvtStart*)
   {
      ::TUsrEvtStart *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TUsrEvtStart >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TUsrEvtStart", ::TUsrEvtStart::Class_Version(), "TUsrEvtStart.h", 29,
                  typeid(::TUsrEvtStart), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TUsrEvtStart::Dictionary, isa_proxy, 4,
                  sizeof(::TUsrEvtStart) );
      instance.SetNew(&new_TUsrEvtStart);
      instance.SetNewArray(&newArray_TUsrEvtStart);
      instance.SetDelete(&delete_TUsrEvtStart);
      instance.SetDeleteArray(&deleteArray_TUsrEvtStart);
      instance.SetDestructor(&destruct_TUsrEvtStart);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TUsrEvtStart*)
   {
      return GenerateInitInstanceLocal((::TUsrEvtStart*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TUsrEvtStart*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TUsrEvtStop(void *p = 0);
   static void *newArray_TUsrEvtStop(Long_t size, void *p);
   static void delete_TUsrEvtStop(void *p);
   static void deleteArray_TUsrEvtStop(void *p);
   static void destruct_TUsrEvtStop(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TUsrEvtStop*)
   {
      ::TUsrEvtStop *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TUsrEvtStop >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TUsrEvtStop", ::TUsrEvtStop::Class_Version(), "TUsrEvtStop.h", 29,
                  typeid(::TUsrEvtStop), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TUsrEvtStop::Dictionary, isa_proxy, 4,
                  sizeof(::TUsrEvtStop) );
      instance.SetNew(&new_TUsrEvtStop);
      instance.SetNewArray(&newArray_TUsrEvtStop);
      instance.SetDelete(&delete_TUsrEvtStop);
      instance.SetDeleteArray(&deleteArray_TUsrEvtStop);
      instance.SetDestructor(&destruct_TUsrEvtStop);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TUsrEvtStop*)
   {
      return GenerateInitInstanceLocal((::TUsrEvtStop*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TUsrEvtStop*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TMrbAnalyze(void *p = 0);
   static void *newArray_TMrbAnalyze(Long_t size, void *p);
   static void delete_TMrbAnalyze(void *p);
   static void deleteArray_TMrbAnalyze(void *p);
   static void destruct_TMrbAnalyze(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TMrbAnalyze*)
   {
      ::TMrbAnalyze *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TMrbAnalyze >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TMrbAnalyze", ::TMrbAnalyze::Class_Version(), "TMrbAnalyze.h", 48,
                  typeid(::TMrbAnalyze), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TMrbAnalyze::Dictionary, isa_proxy, 4,
                  sizeof(::TMrbAnalyze) );
      instance.SetNew(&new_TMrbAnalyze);
      instance.SetNewArray(&newArray_TMrbAnalyze);
      instance.SetDelete(&delete_TMrbAnalyze);
      instance.SetDeleteArray(&deleteArray_TMrbAnalyze);
      instance.SetDestructor(&destruct_TMrbAnalyze);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TMrbAnalyze*)
   {
      return GenerateInitInstanceLocal((::TMrbAnalyze*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TMrbAnalyze*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TMrbSubevent_Caen(void *p = 0);
   static void *newArray_TMrbSubevent_Caen(Long_t size, void *p);
   static void delete_TMrbSubevent_Caen(void *p);
   static void deleteArray_TMrbSubevent_Caen(void *p);
   static void destruct_TMrbSubevent_Caen(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TMrbSubevent_Caen*)
   {
      ::TMrbSubevent_Caen *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TMrbSubevent_Caen >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TMrbSubevent_Caen", ::TMrbSubevent_Caen::Class_Version(), "GamsAnalyze.h", 79,
                  typeid(::TMrbSubevent_Caen), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TMrbSubevent_Caen::Dictionary, isa_proxy, 4,
                  sizeof(::TMrbSubevent_Caen) );
      instance.SetNew(&new_TMrbSubevent_Caen);
      instance.SetNewArray(&newArray_TMrbSubevent_Caen);
      instance.SetDelete(&delete_TMrbSubevent_Caen);
      instance.SetDeleteArray(&deleteArray_TMrbSubevent_Caen);
      instance.SetDestructor(&destruct_TMrbSubevent_Caen);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TMrbSubevent_Caen*)
   {
      return GenerateInitInstanceLocal((::TMrbSubevent_Caen*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TMrbSubevent_Caen*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TUsrSevtDet(void *p = 0);
   static void *newArray_TUsrSevtDet(Long_t size, void *p);
   static void delete_TUsrSevtDet(void *p);
   static void deleteArray_TUsrSevtDet(void *p);
   static void destruct_TUsrSevtDet(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TUsrSevtDet*)
   {
      ::TUsrSevtDet *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TUsrSevtDet >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TUsrSevtDet", ::TUsrSevtDet::Class_Version(), "GamsAnalyze.h", 117,
                  typeid(::TUsrSevtDet), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TUsrSevtDet::Dictionary, isa_proxy, 4,
                  sizeof(::TUsrSevtDet) );
      instance.SetNew(&new_TUsrSevtDet);
      instance.SetNewArray(&newArray_TUsrSevtDet);
      instance.SetDelete(&delete_TUsrSevtDet);
      instance.SetDeleteArray(&deleteArray_TUsrSevtDet);
      instance.SetDestructor(&destruct_TUsrSevtDet);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TUsrSevtDet*)
   {
      return GenerateInitInstanceLocal((::TUsrSevtDet*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TUsrSevtDet*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TUsrSevtSsca(void *p = 0);
   static void *newArray_TUsrSevtSsca(Long_t size, void *p);
   static void delete_TUsrSevtSsca(void *p);
   static void deleteArray_TUsrSevtSsca(void *p);
   static void destruct_TUsrSevtSsca(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TUsrSevtSsca*)
   {
      ::TUsrSevtSsca *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TUsrSevtSsca >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TUsrSevtSsca", ::TUsrSevtSsca::Class_Version(), "GamsAnalyze.h", 188,
                  typeid(::TUsrSevtSsca), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TUsrSevtSsca::Dictionary, isa_proxy, 4,
                  sizeof(::TUsrSevtSsca) );
      instance.SetNew(&new_TUsrSevtSsca);
      instance.SetNewArray(&newArray_TUsrSevtSsca);
      instance.SetDelete(&delete_TUsrSevtSsca);
      instance.SetDeleteArray(&deleteArray_TUsrSevtSsca);
      instance.SetDestructor(&destruct_TUsrSevtSsca);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TUsrSevtSsca*)
   {
      return GenerateInitInstanceLocal((::TUsrSevtSsca*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TUsrSevtSsca*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TUsrEvtDevt(void *p = 0);
   static void *newArray_TUsrEvtDevt(Long_t size, void *p);
   static void delete_TUsrEvtDevt(void *p);
   static void deleteArray_TUsrEvtDevt(void *p);
   static void destruct_TUsrEvtDevt(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TUsrEvtDevt*)
   {
      ::TUsrEvtDevt *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TUsrEvtDevt >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TUsrEvtDevt", ::TUsrEvtDevt::Class_Version(), "GamsAnalyze.h", 251,
                  typeid(::TUsrEvtDevt), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TUsrEvtDevt::Dictionary, isa_proxy, 4,
                  sizeof(::TUsrEvtDevt) );
      instance.SetNew(&new_TUsrEvtDevt);
      instance.SetNewArray(&newArray_TUsrEvtDevt);
      instance.SetDelete(&delete_TUsrEvtDevt);
      instance.SetDeleteArray(&deleteArray_TUsrEvtDevt);
      instance.SetDestructor(&destruct_TUsrEvtDevt);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TUsrEvtDevt*)
   {
      return GenerateInitInstanceLocal((::TUsrEvtDevt*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TUsrEvtDevt*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TUsrEvtXstop(void *p = 0);
   static void *newArray_TUsrEvtXstop(Long_t size, void *p);
   static void delete_TUsrEvtXstop(void *p);
   static void deleteArray_TUsrEvtXstop(void *p);
   static void destruct_TUsrEvtXstop(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TUsrEvtXstop*)
   {
      ::TUsrEvtXstop *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TUsrEvtXstop >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TUsrEvtXstop", ::TUsrEvtXstop::Class_Version(), "GamsAnalyze.h", 311,
                  typeid(::TUsrEvtXstop), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TUsrEvtXstop::Dictionary, isa_proxy, 4,
                  sizeof(::TUsrEvtXstop) );
      instance.SetNew(&new_TUsrEvtXstop);
      instance.SetNewArray(&newArray_TUsrEvtXstop);
      instance.SetDelete(&delete_TUsrEvtXstop);
      instance.SetDeleteArray(&deleteArray_TUsrEvtXstop);
      instance.SetDestructor(&destruct_TUsrEvtXstop);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TUsrEvtXstop*)
   {
      return GenerateInitInstanceLocal((::TUsrEvtXstop*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TUsrEvtXstop*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr TMrbIOSpec::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TMrbIOSpec::Class_Name()
{
   return "TMrbIOSpec";
}

//______________________________________________________________________________
const char *TMrbIOSpec::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TMrbIOSpec*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TMrbIOSpec::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TMrbIOSpec*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TMrbIOSpec::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TMrbIOSpec*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TMrbIOSpec::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TMrbIOSpec*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TUsrEvent::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TUsrEvent::Class_Name()
{
   return "TUsrEvent";
}

//______________________________________________________________________________
const char *TUsrEvent::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvent*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TUsrEvent::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvent*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TUsrEvent::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvent*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TUsrEvent::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvent*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TUsrEvtStart::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TUsrEvtStart::Class_Name()
{
   return "TUsrEvtStart";
}

//______________________________________________________________________________
const char *TUsrEvtStart::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtStart*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TUsrEvtStart::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtStart*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TUsrEvtStart::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtStart*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TUsrEvtStart::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtStart*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TUsrEvtStop::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TUsrEvtStop::Class_Name()
{
   return "TUsrEvtStop";
}

//______________________________________________________________________________
const char *TUsrEvtStop::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtStop*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TUsrEvtStop::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtStop*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TUsrEvtStop::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtStop*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TUsrEvtStop::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtStop*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TMrbAnalyze::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TMrbAnalyze::Class_Name()
{
   return "TMrbAnalyze";
}

//______________________________________________________________________________
const char *TMrbAnalyze::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TMrbAnalyze*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TMrbAnalyze::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TMrbAnalyze*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TMrbAnalyze::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TMrbAnalyze*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TMrbAnalyze::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TMrbAnalyze*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TMrbSubevent_Caen::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TMrbSubevent_Caen::Class_Name()
{
   return "TMrbSubevent_Caen";
}

//______________________________________________________________________________
const char *TMrbSubevent_Caen::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TMrbSubevent_Caen*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TMrbSubevent_Caen::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TMrbSubevent_Caen*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TMrbSubevent_Caen::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TMrbSubevent_Caen*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TMrbSubevent_Caen::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TMrbSubevent_Caen*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TUsrSevtDet::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TUsrSevtDet::Class_Name()
{
   return "TUsrSevtDet";
}

//______________________________________________________________________________
const char *TUsrSevtDet::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrSevtDet*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TUsrSevtDet::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrSevtDet*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TUsrSevtDet::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrSevtDet*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TUsrSevtDet::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrSevtDet*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TUsrSevtSsca::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TUsrSevtSsca::Class_Name()
{
   return "TUsrSevtSsca";
}

//______________________________________________________________________________
const char *TUsrSevtSsca::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrSevtSsca*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TUsrSevtSsca::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrSevtSsca*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TUsrSevtSsca::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrSevtSsca*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TUsrSevtSsca::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrSevtSsca*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TUsrEvtDevt::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TUsrEvtDevt::Class_Name()
{
   return "TUsrEvtDevt";
}

//______________________________________________________________________________
const char *TUsrEvtDevt::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtDevt*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TUsrEvtDevt::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtDevt*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TUsrEvtDevt::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtDevt*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TUsrEvtDevt::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtDevt*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TUsrEvtXstop::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TUsrEvtXstop::Class_Name()
{
   return "TUsrEvtXstop";
}

//______________________________________________________________________________
const char *TUsrEvtXstop::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtXstop*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TUsrEvtXstop::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtXstop*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TUsrEvtXstop::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtXstop*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TUsrEvtXstop::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TUsrEvtXstop*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void TMrbIOSpec::Streamer(TBuffer &R__b)
{
   // Stream an object of class TMrbIOSpec.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TMrbIOSpec::Class(),this);
   } else {
      R__b.WriteClassBuffer(TMrbIOSpec::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TMrbIOSpec(void *p) {
      return  p ? new(p) ::TMrbIOSpec : new ::TMrbIOSpec;
   }
   static void *newArray_TMrbIOSpec(Long_t nElements, void *p) {
      return p ? new(p) ::TMrbIOSpec[nElements] : new ::TMrbIOSpec[nElements];
   }
   // Wrapper around operator delete
   static void delete_TMrbIOSpec(void *p) {
      delete ((::TMrbIOSpec*)p);
   }
   static void deleteArray_TMrbIOSpec(void *p) {
      delete [] ((::TMrbIOSpec*)p);
   }
   static void destruct_TMrbIOSpec(void *p) {
      typedef ::TMrbIOSpec current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TMrbIOSpec

//______________________________________________________________________________
void TUsrEvent::Streamer(TBuffer &R__b)
{
   // Stream an object of class TUsrEvent.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TUsrEvent::Class(),this);
   } else {
      R__b.WriteClassBuffer(TUsrEvent::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TUsrEvent(void *p) {
      return  p ? new(p) ::TUsrEvent : new ::TUsrEvent;
   }
   static void *newArray_TUsrEvent(Long_t nElements, void *p) {
      return p ? new(p) ::TUsrEvent[nElements] : new ::TUsrEvent[nElements];
   }
   // Wrapper around operator delete
   static void delete_TUsrEvent(void *p) {
      delete ((::TUsrEvent*)p);
   }
   static void deleteArray_TUsrEvent(void *p) {
      delete [] ((::TUsrEvent*)p);
   }
   static void destruct_TUsrEvent(void *p) {
      typedef ::TUsrEvent current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TUsrEvent

//______________________________________________________________________________
void TUsrEvtStart::Streamer(TBuffer &R__b)
{
   // Stream an object of class TUsrEvtStart.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TUsrEvtStart::Class(),this);
   } else {
      R__b.WriteClassBuffer(TUsrEvtStart::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TUsrEvtStart(void *p) {
      return  p ? new(p) ::TUsrEvtStart : new ::TUsrEvtStart;
   }
   static void *newArray_TUsrEvtStart(Long_t nElements, void *p) {
      return p ? new(p) ::TUsrEvtStart[nElements] : new ::TUsrEvtStart[nElements];
   }
   // Wrapper around operator delete
   static void delete_TUsrEvtStart(void *p) {
      delete ((::TUsrEvtStart*)p);
   }
   static void deleteArray_TUsrEvtStart(void *p) {
      delete [] ((::TUsrEvtStart*)p);
   }
   static void destruct_TUsrEvtStart(void *p) {
      typedef ::TUsrEvtStart current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TUsrEvtStart

//______________________________________________________________________________
void TUsrEvtStop::Streamer(TBuffer &R__b)
{
   // Stream an object of class TUsrEvtStop.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TUsrEvtStop::Class(),this);
   } else {
      R__b.WriteClassBuffer(TUsrEvtStop::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TUsrEvtStop(void *p) {
      return  p ? new(p) ::TUsrEvtStop : new ::TUsrEvtStop;
   }
   static void *newArray_TUsrEvtStop(Long_t nElements, void *p) {
      return p ? new(p) ::TUsrEvtStop[nElements] : new ::TUsrEvtStop[nElements];
   }
   // Wrapper around operator delete
   static void delete_TUsrEvtStop(void *p) {
      delete ((::TUsrEvtStop*)p);
   }
   static void deleteArray_TUsrEvtStop(void *p) {
      delete [] ((::TUsrEvtStop*)p);
   }
   static void destruct_TUsrEvtStop(void *p) {
      typedef ::TUsrEvtStop current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TUsrEvtStop

//______________________________________________________________________________
void TMrbAnalyze::Streamer(TBuffer &R__b)
{
   // Stream an object of class TMrbAnalyze.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TMrbAnalyze::Class(),this);
   } else {
      R__b.WriteClassBuffer(TMrbAnalyze::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TMrbAnalyze(void *p) {
      return  p ? new(p) ::TMrbAnalyze : new ::TMrbAnalyze;
   }
   static void *newArray_TMrbAnalyze(Long_t nElements, void *p) {
      return p ? new(p) ::TMrbAnalyze[nElements] : new ::TMrbAnalyze[nElements];
   }
   // Wrapper around operator delete
   static void delete_TMrbAnalyze(void *p) {
      delete ((::TMrbAnalyze*)p);
   }
   static void deleteArray_TMrbAnalyze(void *p) {
      delete [] ((::TMrbAnalyze*)p);
   }
   static void destruct_TMrbAnalyze(void *p) {
      typedef ::TMrbAnalyze current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TMrbAnalyze

//______________________________________________________________________________
void TMrbSubevent_Caen::Streamer(TBuffer &R__b)
{
   // Stream an object of class TMrbSubevent_Caen.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TMrbSubevent_Caen::Class(),this);
   } else {
      R__b.WriteClassBuffer(TMrbSubevent_Caen::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TMrbSubevent_Caen(void *p) {
      return  p ? new(p) ::TMrbSubevent_Caen : new ::TMrbSubevent_Caen;
   }
   static void *newArray_TMrbSubevent_Caen(Long_t nElements, void *p) {
      return p ? new(p) ::TMrbSubevent_Caen[nElements] : new ::TMrbSubevent_Caen[nElements];
   }
   // Wrapper around operator delete
   static void delete_TMrbSubevent_Caen(void *p) {
      delete ((::TMrbSubevent_Caen*)p);
   }
   static void deleteArray_TMrbSubevent_Caen(void *p) {
      delete [] ((::TMrbSubevent_Caen*)p);
   }
   static void destruct_TMrbSubevent_Caen(void *p) {
      typedef ::TMrbSubevent_Caen current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TMrbSubevent_Caen

//______________________________________________________________________________
void TUsrSevtDet::Streamer(TBuffer &R__b)
{
   // Stream an object of class TUsrSevtDet.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TUsrSevtDet::Class(),this);
   } else {
      R__b.WriteClassBuffer(TUsrSevtDet::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TUsrSevtDet(void *p) {
      return  p ? new(p) ::TUsrSevtDet : new ::TUsrSevtDet;
   }
   static void *newArray_TUsrSevtDet(Long_t nElements, void *p) {
      return p ? new(p) ::TUsrSevtDet[nElements] : new ::TUsrSevtDet[nElements];
   }
   // Wrapper around operator delete
   static void delete_TUsrSevtDet(void *p) {
      delete ((::TUsrSevtDet*)p);
   }
   static void deleteArray_TUsrSevtDet(void *p) {
      delete [] ((::TUsrSevtDet*)p);
   }
   static void destruct_TUsrSevtDet(void *p) {
      typedef ::TUsrSevtDet current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TUsrSevtDet

//______________________________________________________________________________
void TUsrSevtSsca::Streamer(TBuffer &R__b)
{
   // Stream an object of class TUsrSevtSsca.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TUsrSevtSsca::Class(),this);
   } else {
      R__b.WriteClassBuffer(TUsrSevtSsca::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TUsrSevtSsca(void *p) {
      return  p ? new(p) ::TUsrSevtSsca : new ::TUsrSevtSsca;
   }
   static void *newArray_TUsrSevtSsca(Long_t nElements, void *p) {
      return p ? new(p) ::TUsrSevtSsca[nElements] : new ::TUsrSevtSsca[nElements];
   }
   // Wrapper around operator delete
   static void delete_TUsrSevtSsca(void *p) {
      delete ((::TUsrSevtSsca*)p);
   }
   static void deleteArray_TUsrSevtSsca(void *p) {
      delete [] ((::TUsrSevtSsca*)p);
   }
   static void destruct_TUsrSevtSsca(void *p) {
      typedef ::TUsrSevtSsca current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TUsrSevtSsca

//______________________________________________________________________________
void TUsrEvtDevt::Streamer(TBuffer &R__b)
{
   // Stream an object of class TUsrEvtDevt.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TUsrEvtDevt::Class(),this);
   } else {
      R__b.WriteClassBuffer(TUsrEvtDevt::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TUsrEvtDevt(void *p) {
      return  p ? new(p) ::TUsrEvtDevt : new ::TUsrEvtDevt;
   }
   static void *newArray_TUsrEvtDevt(Long_t nElements, void *p) {
      return p ? new(p) ::TUsrEvtDevt[nElements] : new ::TUsrEvtDevt[nElements];
   }
   // Wrapper around operator delete
   static void delete_TUsrEvtDevt(void *p) {
      delete ((::TUsrEvtDevt*)p);
   }
   static void deleteArray_TUsrEvtDevt(void *p) {
      delete [] ((::TUsrEvtDevt*)p);
   }
   static void destruct_TUsrEvtDevt(void *p) {
      typedef ::TUsrEvtDevt current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TUsrEvtDevt

//______________________________________________________________________________
void TUsrEvtXstop::Streamer(TBuffer &R__b)
{
   // Stream an object of class TUsrEvtXstop.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TUsrEvtXstop::Class(),this);
   } else {
      R__b.WriteClassBuffer(TUsrEvtXstop::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TUsrEvtXstop(void *p) {
      return  p ? new(p) ::TUsrEvtXstop : new ::TUsrEvtXstop;
   }
   static void *newArray_TUsrEvtXstop(Long_t nElements, void *p) {
      return p ? new(p) ::TUsrEvtXstop[nElements] : new ::TUsrEvtXstop[nElements];
   }
   // Wrapper around operator delete
   static void delete_TUsrEvtXstop(void *p) {
      delete ((::TUsrEvtXstop*)p);
   }
   static void deleteArray_TUsrEvtXstop(void *p) {
      delete [] ((::TUsrEvtXstop*)p);
   }
   static void destruct_TUsrEvtXstop(void *p) {
      typedef ::TUsrEvtXstop current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TUsrEvtXstop

namespace {
  void TriggerDictionaryInitialization_GamsAnalyzeDict_Impl() {
    static const char* headers[] = {
"GamsAnalyze.h",
"GamsAnalyzeGlobals.h",
0
    };
    static const char* includePaths[] = {
"/home/gams/Applications/marabou/include",
"/home/gams/Applications/root/include",
"/home/gams/Applications/marabou/test/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "GamsAnalyzeDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate(R"ATTRDUMP([Analyze] I/O specifications)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$TMrbIOSpec.h")))  __attribute__((annotate("$clingAutoload$GamsAnalyze.h")))  TMrbIOSpec;
class __attribute__((annotate(R"ATTRDUMP([Analyze] User's event structure)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$TUsrEvent.h")))  __attribute__((annotate("$clingAutoload$GamsAnalyze.h")))  TUsrEvent;
class __attribute__((annotate(R"ATTRDUMP([Analyze] Event type "START ACQUISITION")ATTRDUMP"))) __attribute__((annotate("$clingAutoload$TUsrEvtStart.h")))  __attribute__((annotate("$clingAutoload$GamsAnalyze.h")))  TUsrEvtStart;
class __attribute__((annotate(R"ATTRDUMP([Analyze] Event type "STOP ACQUISITION")ATTRDUMP"))) __attribute__((annotate("$clingAutoload$TUsrEvtStop.h")))  __attribute__((annotate("$clingAutoload$GamsAnalyze.h")))  TUsrEvtStop;
class __attribute__((annotate(R"ATTRDUMP([Analyze] Describe user's analysis)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$TMrbAnalyze.h")))  __attribute__((annotate("$clingAutoload$GamsAnalyze.h")))  TMrbAnalyze;
class __attribute__((annotate(R"ATTRDUMP([Analyze] Base class for subevents: CAEN data stored in hit buffer)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$GamsAnalyze.h")))  TMrbSubevent_Caen;
class __attribute__((annotate(R"ATTRDUMP([Analyze] Store CAEN data in hit buffer)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$GamsAnalyze.h")))  TUsrSevtDet;
class __attribute__((annotate(R"ATTRDUMP([Analyze] Standard methods to store subevent data)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$GamsAnalyze.h")))  TUsrSevtSsca;
class __attribute__((annotate(R"ATTRDUMP([Analyze] Standard methods to handle event data)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$GamsAnalyze.h")))  TUsrEvtDevt;
class __attribute__((annotate(R"ATTRDUMP([Analyze] Standard methods to handle event data)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$GamsAnalyze.h")))  TUsrEvtXstop;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "GamsAnalyzeDict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
#include "GamsAnalyze.h"
#include "GamsAnalyzeGlobals.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"TMrbAnalyze", payloadCode, "@",
"TMrbIOSpec", payloadCode, "@",
"TMrbSubevent_Caen", payloadCode, "@",
"TUsrEvent", payloadCode, "@",
"TUsrEvtDevt", payloadCode, "@",
"TUsrEvtStart", payloadCode, "@",
"TUsrEvtStop", payloadCode, "@",
"TUsrEvtXstop", payloadCode, "@",
"TUsrSevtDet", payloadCode, "@",
"TUsrSevtSsca", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("GamsAnalyzeDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_GamsAnalyzeDict_Impl, {}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_GamsAnalyzeDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_GamsAnalyzeDict() {
  TriggerDictionaryInitialization_GamsAnalyzeDict_Impl();
}
