#ifndef __GamsAnalyze_h__
#define __GamsAnalyze_h__

//_________________________________________________[C++ CLASS DEFINITION FILE]
//////////////////////////////////////////////////////////////////////////////
// Name:           GamsAnalyze.h
// Purpose:        Define user-specific classes for data analysis
// Description:    GAMS experiment
//
// Author:         gams
// Revision:
// Date:           Sun Feb 17 11:13:39 2019
// URL:
// Keywords:
//////////////////////////////////////////////////////////////////////////////


#include <time.h>

#include "TEnv.h"
#include "TMrbLogger.h"
#include "TMrbString.h"
#include "TMrbNamedArray.h"
#include "TMrbVar.h"
#include "TMrbWdw.h"
#include "TF1.h"
#include "TF2.h"
#include "TArrayL64.h"

#include "TMrbAnalyze.h"
#include "TMrbTransport.h"

#include "TMrbLofData.h"

#include "GamsCommonIndices.h"

#include "RQ_OBJECT.h"


#ifndef __CaenDefs__
#define __CaenDefs__
// definitions for CAEN modules
// Vxxx -> common to all (?) types
// V965A -> special qdc defs

#define CAEN_Vxxx_SH_WC							8
#define CAEN_Vxxx_SH_CHN						16

#define CAEN_Vxxx_D_HDR							0x02000000
#define CAEN_Vxxx_D_DATA						0x00000000
#define CAEN_Vxxx_D_EOB							0x04000000
#define CAEN_Vxxx_D_INVALID						0x06000000

#define CAEN_Vxxx_M_MSERIAL						0xFF
#define CAEN_Vxxx_M_WC							0x3F
#define CAEN_Vxxx_M_CHN							0x3F
#define CAEN_Vxxx_M_ID							0x07000000
#define CAEN_Vxxx_M_HDR							0x07FFFFFF
#define CAEN_Vxxx_M_DATA 						0x07FFFFFF
#define CAEN_Vxxx_M_ADCDATA	 					0x00000FFF

#define CAEN_Vxxx_B_OVERFLOW	 				(0x1 << 12)
#define CAEN_Vxxx_B_UNDERTHRESH 				(0x1 << 13)

#define CAEN_Vxxx_N_CHANNELS					32
#define CAEN_Vxxx_N_MAXEVENTS					32

#endif


//______________________________________________________[C++ CLASS DEFINITION]
//////////////////////////////////////////////////////////////////////////////
// Name:           TMrbSubevent_Caen
// Purpose:        Base class for subevents containing data from CAEN VME modules
// Description:
// Keywords:
//////////////////////////////////////////////////////////////////////////////

class TMrbSubevent_Caen : public TObject {

	public:
		TMrbSubevent_Caen() {};				// default ctor
		~TMrbSubevent_Caen() {};			// default dtor

		Bool_t FillSubevent(const UShort_t * SevtData, Int_t SevtWC, Int_t TimeStamp);	// fill subevent from MBS data

		inline TUsrHitBuffer * AllocHitBuffer(const Char_t * Name, Int_t NofEntries, Int_t HighWater = 0) {	// allocate buffer space
			fHitBuffer.SetName(Name);
			fHitBuffer.AllocClonesArray(NofEntries, HighWater);
			return(&fHitBuffer);
		};
		inline TUsrHitBuffer * GetHitBuffer() { return(&fHitBuffer); };
		inline Bool_t HasHitBuffer() { return kTRUE; };

	protected:
		inline void SetTimeStamp(Int_t TimeStamp) { SetUniqueID(TimeStamp); };

	protected:
		TObject * fMyAddr;						//! ptr to subevent to be used in TTree::Branch() (online)
		TBranch * fBranch;						//! branch address (replay only)

		Int_t fTimeStamp;						// time stamp, same as fUniqueID

		TUsrHitBuffer fHitBuffer; 				// hit buffer to store subevent data

	ClassDef(TMrbSubevent_Caen, 1)			// [Analyze] Base class for subevents: CAEN data stored in hit buffer
};

//______________________________________________________[C++ CLASS DEFINITION]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtDet
// Purpose:        Define a user-specific subevent class
// Description:    det subevent
// Keywords:
//////////////////////////////////////////////////////////////////////////////

class TUsrSevtDet : public TMrbSubevent_Caen {

// declare all parent events as friends
// so user may address subevent data directly from event level
	friend class TUsrEvtDevt;

	public:
		TUsrSevtDet() {			// default ctor
			fSevtName = "det";
		};
		~TUsrSevtDet() {};			// default dtor

		inline const Char_t * GetName() const { return(fSevtName.Data()); };
		inline Int_t GetSerial() const { return(kMrbSevtDet); };

		inline Int_t GetNofParams() const { return(11); };
		inline Int_t GetNofModules() const { return(1); };

		Bool_t BookParams();					// book params
 		void Reset(Int_t InitValue = 0, Bool_t DataOnly = kFALSE);		// reset param data
 		Bool_t BookHistograms();				// book histograms
 		Bool_t FillHistograms();				// fill histograms
		Bool_t AddBranch(TTree * Tree);				// add a branch to the tree
		Bool_t InitializeBranch(TTree * Tree);		// init branch addresses (replay mode only)
		inline Int_t GetTimeStamp() const { return(fTimeStamp); };	// return time stamp (100 microsecs since start)


												// get param addr (method needed to access params from outside, eg. in macros)
		inline TUsrHit * GetAddrEt() { return(&et); };	// addr of et
		inline TUsrHit * GetAddrE1l() { return(&e1l); };	// addr of e1l
		inline TUsrHit * GetAddrE1r() { return(&e1r); };	// addr of e1r
		inline TUsrHit * GetAddrE2l() { return(&e2l); };	// addr of e2l
		inline TUsrHit * GetAddrE2r() { return(&e2r); };	// addr of e2r
		inline TUsrHit * GetAddrE3() { return(&e3); };	// addr of e3
		inline TUsrHit * GetAddrE4() { return(&e4); };	// addr of e4
		inline TUsrHit * GetAddrE5() { return(&e5); };	// addr of e5
		inline TUsrHit * GetAddrDt() { return(&dt); };	// addr of dt
		inline TUsrHit * GetAddrTof() { return(&tof); };	// addr of tof
		inline TUsrHit * GetAddrPup() { return(&pup); };	// addr of pup

		inline Int_t GetIndex() const { return(kMrbSevtDet); };			// subevent index
		inline UInt_t GetIndexBit() const { return(kMrbSevtBitDet); };	// subevent index bit

	protected:
		TString fSevtName;						//! subevent name

// define private data members here
 		TUsrHit et; 				// det.et
 		TUsrHit e1l; 				// det.e1l
 		TUsrHit e1r; 				// det.e1r
 		TUsrHit e2l; 				// det.e2l
 		TUsrHit e2r; 				// det.e2r
 		TUsrHit e3; 				// det.e3
 		TUsrHit e4; 				// det.e4
 		TUsrHit e5; 				// det.e5
 		TUsrHit dt; 				// det.dt
 		TUsrHit tof; 				// det.tof
 		TUsrHit pup; 				// det.pup


	ClassDef(TUsrSevtDet, 1)			// [Analyze] Store CAEN data in hit buffer
};

//______________________________________________________[C++ CLASS DEFINITION]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtSsca
// Purpose:        Define a user-specific subevent class
// Description:    scaler subevent
// Keywords:
//////////////////////////////////////////////////////////////////////////////

class TUsrSevtSsca : public TObject {

// declare all parent events as friends
// so user may address subevent data directly from event level
	friend class TUsrEvtDevt;
	friend class TUsrEvtXstop;

	public:
		TUsrSevtSsca() {		// default ctor
			fSevtName = "ssca";
		};
		~TUsrSevtSsca() {};		// default dtor

		inline const Char_t * GetName() const { return(fSevtName.Data()); };
		inline Int_t GetSerial() const { return(kMrbSevtSsca); };

		inline Int_t GetNofParams() const { return(32); };
		inline Int_t GetNofModules() const { return(1); };

		Bool_t FillSubevent(const UShort_t * SevtData, Int_t SevtWC, Int_t TimeStamp);	// fill subevent from MBS data
		Bool_t BookParams();					// book params
 		void Reset(Int_t InitValue = 0, Bool_t DataOnly = kFALSE);		// reset param data
 		Bool_t BookHistograms();				// book histograms
 		Bool_t FillHistograms();				// fill histograms
		Bool_t AddBranch(TTree * Tree);				// add a branch to the tree
		Bool_t InitializeBranch(TTree * Tree);		// init branch addresses (replay mode only)
		inline Int_t GetTimeStamp() const { return(fTimeStamp); };	// return time stamp (100 microsecs since start)



												// get param addr (method needed to access params from outside, eg. in macros)
		inline Int_t * GetAddrScdata() { return(scdata); };	// addr of scdata
		inline Int_t GetScdata(Int_t Offset) const { return(scdata[Offset]); };	// addr of scdata[n]

		inline Int_t GetIndex() const { return(kMrbSevtSsca); };			// subevent index
		inline UInt_t GetIndexBit() const { return(kMrbSevtBitSsca); };	// subevent index bit

  protected:
		inline void SetTimeStamp(Int_t TimeStamp) { SetUniqueID(TimeStamp); };

	protected:
		TString fSevtName;							//! subevent name
		TObject * fMyAddr;						//! ptr to subevent to be used in TTree::Branch() (online)
		TBranch * fBranch;						//! branch address (replay only)

		Int_t fTimeStamp;						// time stamp, same as fUniqueID

// define private data members here
 		Int_t scdata[32];			// ssca.scdata


	ClassDef(TUsrSevtSsca, 1)		// [Analyze] Standard methods to store subevent data
};

//______________________________________________________[C++ CLASS DEFINITION]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtDevt
// Purpose:        Define a user-specific event class
// Description:    Event assigned to trigger 
//                 det event
// Keywords:
//////////////////////////////////////////////////////////////////////////////

class TUsrEvtDevt :
								public TUsrEvent {

	RQ_OBJECT("TUsrEvtDevt")

	public:
		TUsrEvtDevt() {			// ctor
			fType = 10; 					// event type (given by MBS)
			fSubtype = 1;					// ... subtype
			fTrigger = kMrbTriggerDevt; 	// trigger number associated with this event
			fNofEvents = 0; 				// reset event counter
			fNofEntries = 0;				// number of entries read from tree
			fEventNumber = 0;				// event number provided by MBS
			fReplayMode = kFALSE;			// normally online data acquisition
			this->SetupLofSubevents();
		};
		~TUsrEvtDevt() {};		// default dtor

		Int_t SetupLofSubevents();								// build list of subevents
		Bool_t FillEvent(const s_vehe *, MBSDataIO *);			// event filling function supplied by system
		Bool_t BookParams();									// book params for this event
		Bool_t BookHistograms();								// book histograms for this event
		Bool_t FillHistograms();								// fill histograms
		Bool_t FillRateHistograms();							// fill rate histograms
		Bool_t AllocHitBuffers();								// allocate hit buffers for subevents if needed
		Bool_t CreateTree();									// create a tree if needed
		Bool_t Analyze(Int_t EntryNo = 0);				// method to process this event
		Bool_t InitializeTree(TFile * RootFile); 				// init tree (replay mode only)
		Bool_t ReplayTreeData(TMrbIOSpec * IOSpec);				// replay tree data
		void Reset(Int_t InitValue = 0, Bool_t DataOnly = kFALSE);	// reset event data
		
		Int_t UnpackNextEvent(Int_t EntryNo);					// unpack next event

		inline UInt_t GetSubeventPattern() const { return(fSubeventPattern); };
		inline Bool_t ThereIsSubevent(UInt_t SubeventBit) const { return((fSubeventPattern & SubeventBit) != 0); };

		inline TUsrSevtDet * GetAddrDet() { return &det; };
		inline TUsrSevtSsca * GetAddrSsca() { return &ssca; };

// user-defined method(s)
		Bool_t AnalyzeEvent(TObjArray & EvtData);

	protected:
		UInt_t fSubeventPattern;
		TUsrSevtDet det;
		TUsrSevtSsca ssca;



	ClassDef(TUsrEvtDevt, 1) 	// [Analyze] Standard methods to handle event data
};
//______________________________________________________[C++ CLASS DEFINITION]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtXstop
// Purpose:        Define a user-specific event class
// Description:    Event assigned to trigger 
//                 stop acquisition, trigger 15, user-defined
// Keywords:
//////////////////////////////////////////////////////////////////////////////

class TUsrEvtXstop :
								public TUsrEvtStop {

	RQ_OBJECT("TUsrEvtXstop")

	public:
		TUsrEvtXstop() {			// ctor
			fType = 10; 					// event type (given by MBS)
			fSubtype = 1;					// ... subtype
			fTrigger = kMrbTriggerXstop; 	// trigger number associated with this event
			fNofEvents = 0; 				// reset event counter
			fNofEntries = 0;				// number of entries read from tree
			fEventNumber = 0;				// event number provided by MBS
			fReplayMode = kFALSE;			// normally online data acquisition
			this->SetupLofSubevents();
		};
		~TUsrEvtXstop() {};		// default dtor

		Int_t SetupLofSubevents();								// build list of subevents
		Bool_t FillEvent(const s_vehe *, MBSDataIO *);			// event filling function supplied by system
		Bool_t BookParams();									// book params for this event
		Bool_t BookHistograms();								// book histograms for this event
		Bool_t FillHistograms();								// fill histograms
		Bool_t FillRateHistograms();							// fill rate histograms
		Bool_t AllocHitBuffers();								// allocate hit buffers for subevents if needed
		Bool_t CreateTree();									// create a tree if needed
		Bool_t Analyze(Int_t EntryNo = 0);				// method to process this event
		Bool_t InitializeTree(TFile * RootFile); 				// init tree (replay mode only)
		Bool_t ReplayTreeData(TMrbIOSpec * IOSpec);				// replay tree data
		void Reset(Int_t InitValue = 0, Bool_t DataOnly = kFALSE);	// reset event data
		
		Int_t UnpackNextEvent(Int_t EntryNo);					// unpack next event

		inline UInt_t GetSubeventPattern() const { return(fSubeventPattern); };
		inline Bool_t ThereIsSubevent(UInt_t SubeventBit) const { return((fSubeventPattern & SubeventBit) != 0); };

		inline TUsrSevtSsca * GetAddrSsca() { return &ssca; };


	protected:
		UInt_t fSubeventPattern;
		TUsrSevtSsca ssca;



	ClassDef(TUsrEvtXstop, 1) 	// [Analyze] Standard methods to handle event data
};
#endif

