#define EXTERN	1
#include "GamsAnalyze.h"
#include "GamsAnalyzeGlobals.h"

extern TMrbAnalyze * gMrbAnalyze;	// user's base object
extern TMrbLogger * gMrbLog;		// message logger

extern Int_t nofTriggers;					// total number of triggers
extern Int_t nofEvents;					// total number of events

Bool_t TUsrEvtXstop::Analyze(Int_t EvtNo) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtReadout1::Analyze
// Purpose:        Task to evaluate scaler contents
//                 (Trigger 15)
// Arguments:      --
// Results:        kTRUE/kFALSE
// Exceptions:     
// Description:    User task for trigger 15 (stop acquisition)
// Keywords:       
//////////////////////////////////////////////////////////////////////////////

	if (gMrbAnalyze->TreeToBeWritten()) fTreeOut->Fill();


	gMrbLog->Out() << endl << "==========================================================" << endl;
	gMrbLog->Out() << Form("%-25s%10s%10s", "Scaler Contents:", "Channel", "Contents") << endl;
	for (Int_t i = 0; i < 32; i++) {
		if (ssca.scdata[i] > 0) gMrbLog->Out() << Form("%-25s%10d%10d", "", i, ssca.scdata[i]) << endl;
	}
	gMrbLog->Out() << "---------------------------------------------------------=" << endl;
	gMrbLog->Out() << "Triggers: " << nofTriggers << " Events: " << nofEvents << endl;
	gMrbLog->Out() << "==========================================================" << endl << endl;
	gMrbLog->Flush(this->ClassName(), "Analyze");
	return(kTRUE);
}
