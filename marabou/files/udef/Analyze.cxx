#define EXTERN	1
#include "GamsAnalyze.h"
#include "GamsAnalyzeGlobals.h"
#include "TMath.h"

extern TMrbAnalyze * gMrbAnalyze;	// user's base object
extern TMrbLogger * gMrbLog;		// message logger

Int_t nofTriggers;					// total number of triggers
Int_t nofEvents;					// total number of events

Bool_t TUsrEvtDevt::Analyze(Int_t EvtNo) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtDevt::Analyze
// Purpos2e:        Analyze data (Trigger 1)
// Arguments:      --
// Results:        kTRUE/kFALSE
// Exceptions:     
// Description:    Will be called by ProcessEvent()
//                 Loops over events in hit buffer and calls AnalyzeEvent().
// Keywords:       
//////////////////////////////////////////////////////////////////////////////

	if (gMrbAnalyze->TreeToBeWritten()) fTreeOut->Fill();

	if (nofTriggers == 0) {
		gMrbLog->Out() << endl << "==========================================================" << endl;
		gMrbLog->Out()  << Form("%-25s%10s%10s", "Scaler Offset:", "Channel", "Contents") << endl;
		for (Int_t i = 0; i < 32; i++) {
			if (ssca.scdata[i] > 0) gMrbLog->Out()  << Form("%-25s%10d%10d", "", i, ssca.scdata[i]) << endl;
		}
		gMrbLog->Out()  << "==========================================================" << endl << endl;
		gMrbLog->Flush(this->ClassName(), "Analyze");
	}

	nofTriggers++;				// count this trigger
	nofEvents += gMrbAnalyze->GetModuleListEntry(kMrbModuleAdc)->GetEventHits();	// count events

	Int_t nofParams = gMrbAnalyze->GetModuleListEntry(kMrbModuleAdc)->GetNofParams();	// number of subevent channels
	TUsrHBX * hbx = this->GetHBX(kMrbSevtDet);
	TObjArray data(nofParams);

	hbx->ResetIndex();
	TUsrHit * hit;
	Int_t evtNo = -1;
	while (hit = hbx->NextHit()) {
		if (evtNo == -1) {
			evtNo = hit->GetEventNumber();
			data.Clear();
		}
		if (hit->GetEventNumber() == evtNo) {
			data[hit->GetChannel()] = hit;
		} else {
			this->AnalyzeEvent(data);
			evtNo = -1;
		}
	}
	if (data.GetEntries() != 0) this->AnalyzeEvent(data);
	gMrbAnalyze->SetUpdateFlag();
	return(kTRUE);
}

Bool_t TUsrEvtDevt::AnalyzeEvent(TObjArray & Evt) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtDevt::AnalyzeEvent
// Purpose:        Analyze event data (Trigger 1)
// Arguments:      --
// Results:        kTRUE/kFALSE
// Exceptions:     
// Description:    User's analysis function for trigger 1
//                 Any sorting, filtering, and analyzing goes here.
// Keywords:       
//////////////////////////////////////////////////////////////////////////////

//--//+++++++++++++++++++++++++++++++++++++++++++++++++++++ special user code
	Float_t e1l, e1r, e2l, e2r;
	Float_t e1, de1, e2, de2;
	Float_t pos2, pos;
	Float_t dp;
	Bool_t  e1ok, e2ok;
	Float_t chi2;                             
	UInt_t  time;


//	(0) extract data

	Int_t detEt = Evt[kParEt] ? ((TUsrHit *) Evt[kParEt])->GetData() : 0;
	Int_t detE1l = Evt[kParE1l] ? ((TUsrHit *) Evt[kParE1l])->GetData() : 0;
	Int_t detE1r = Evt[kParE1r] ? ((TUsrHit *) Evt[kParE1r])->GetData() : 0;
	Int_t detE2l = Evt[kParE2l] ? ((TUsrHit *) Evt[kParE2l])->GetData() : 0;
	Int_t detE2r = Evt[kParE2r] ? ((TUsrHit *) Evt[kParE2r])->GetData() : 0;
	Int_t detE3 = Evt[kParE3] ? ((TUsrHit *) Evt[kParE3])->GetData() : 0;
	Int_t detE4 = Evt[kParE4] ? ((TUsrHit *) Evt[kParE4])->GetData() : 0;
	Int_t detE5 = Evt[kParE5] ? ((TUsrHit *) Evt[kParE5])->GetData() : 0;
	Int_t detDt = Evt[kParDt] ? ((TUsrHit *) Evt[kParDt])->GetData() : 0;
	Int_t detTof = Evt[kParTof] ? ((TUsrHit *) Evt[kParTof])->GetData() : 0;
	Int_t detPup = Evt[kParPup] ? ((TUsrHit *) Evt[kParPup])->GetData() : 0;

//	(1) fill histograms for all event params
//	    write raw data to root file if required

	hEt->Fill((Axis_t) detEt);
	hE1l->Fill((Axis_t) detE1l);
	hE1r->Fill((Axis_t) detE1r);
	hE2l->Fill((Axis_t) detE2l);
	hE2r->Fill((Axis_t) detE2r);
	hE3->Fill((Axis_t) detE3);
	hE4->Fill((Axis_t) detE4);
	hE5->Fill((Axis_t) detE5);
	hDt->Fill((Axis_t) detDt);
	hTof->Fill((Axis_t) detTof);
	hPup->Fill((Axis_t) detPup);

//	(2) sums & differences

   e1 = (detE1l + detE1r)/2;
	de1 = detE1l - detE1r;
	e2 = (detE2l + detE2r)/2;
	de2 = detE2l - detE2r;

//	(3) pos2ition  

    if(wraw->IsInside(detE2l) && wraw->IsInside(detE2r)) {
        pos2 = 1000. * ((de2/e2) + 2.);
		e1ok = kTRUE;
	} else {
		pos2 = 4095;
		e1ok = kFALSE;
	}

    if(wraw->IsInside(detE1l) && wraw->IsInside(detE1r)) {
        pos = 1000. * ((de1/e1) + 2.);
		e2ok = kTRUE;
	} else { 
		pos = 4095;
		e2ok = kFALSE;
	}

	if (e1ok && e2ok) {
        dp = (pos - pos2) / 2. + 2000;
	} else { 
		dp = 4095;
	}


   chi2  =    pow((detEt - pt[0]) /rmst[0] ,2)
				+ pow((e1    - p1[0]) /rms1[0] ,2)
				+ pow((e2    - p2[0]) /rms2[0] ,2)
				+ pow((detE3 - p3[0]) /rms3[0] ,2)
				+ pow((detE4 - p4[0]) /rms4[0] ,2)
				+ pow((detE5 - p5[0]) /rms5[0] ,2)  
				+ pow((detDt - pdt[0]) /rmsdt[0] ,2)
				+ pow((pos2  - pp2[0]) /rmsp2[0] ,2)
				+ pow((pos   - pp[0]) /rmsp[0] ,2)
				+ pow((dp    - pdp[0]) /rmsdp[0] ,2);

//	(6) time stamp

//	time = this->GetTimeStamp() / 10000; //Zeit seit sta ac
	time = det.fTimeStamp / 10000; //Zeit seit sta ac
   if (time > 16384) time = 16384;


//	(7) fill histos from CALCULATED data

	hE1->Fill((Axis_t) e1);
	hE2->Fill((Axis_t) e2);
	hP->Fill((Axis_t) pos);
	hP2->Fill((Axis_t) pos2);
	hDp->Fill((Axis_t) dp);
	hTime->Fill((Axis_t) time);
	hChi2->Fill((Axis_t) chi2);

	hEtp->Fill((Axis_t) pos, (Axis_t) detEt);
	hE1p->Fill((Axis_t) pos, (Axis_t) e1);
	hE2p->Fill((Axis_t) pos, (Axis_t) e2);
	hE3p->Fill((Axis_t) pos, (Axis_t) detE3);
	hE4p->Fill((Axis_t) pos, (Axis_t) detE4);
	hE5p->Fill((Axis_t) pos, (Axis_t) detE5);

    hE1E4->Fill((Axis_t) e1, (Axis_t) detE4);
    hE1E5->Fill((Axis_t) e1, (Axis_t) detE5);
    hE4E5->Fill((Axis_t) detE4, (Axis_t) detE5);

	hDtp->Fill((Axis_t) pos, (Axis_t) detDt);
	hTofp->Fill((Axis_t) pos, (Axis_t) detTof);
	hDpp->Fill((Axis_t) pos, (Axis_t) dp);
	hChi2p->Fill((Axis_t) pos, (Axis_t) chi2);

//	(8) fill histos after window check

	if (wtime->IsInside(time)
	&& wtof->IsInside(detTof)
	&& wet->IsInside(detEt)
	&&	we1->IsInside((Int_t) e1) // ansonsten Fehlermeldungen
	&&	we2->IsInside((Int_t) e2) // ansonsten Fehlermeldungen
	&&	we3->IsInside(detE3)
	&&	we4->IsInside(detE4)
	&&	we5->IsInside(detE5)
	&&	wdt->IsInside(detDt)
	&&	wdp->IsInside((Int_t) dp) // ansonsten Fehlermeldungen
	&&	wp->IsInside((Int_t) pos) // ansonsten Fehlermeldungen
    &&	wp2->IsInside((Int_t) pos2) // ansonsten Fehlermeldungen
	&&	wpup->IsInside(detPup)
	&&	wchi2->IsInside(chi2)) {
		hEtw->Fill((Axis_t) detEt);
		hE1w->Fill((Axis_t) e1);
		hE2w->Fill((Axis_t) e2);
		hE3w->Fill((Axis_t) detE3);
		hE4w->Fill((Axis_t) detE4);
		hE5w->Fill((Axis_t) detE5);
		hPw->Fill((Axis_t) pos); 

        hP2w->Fill((Axis_t) pos2);

		hDpw->Fill((Axis_t) dp);
		hDtw->Fill((Axis_t) detDt);
		hTofw->Fill((Axis_t) detTof);
        hTimew->Fill((Axis_t) time);
   	    hChi2w->Fill((Axis_t) chi2);

		hEtpw->Fill((Axis_t) pos, (Axis_t) detEt);
		hE1pw->Fill((Axis_t) pos, (Axis_t) e1);
		hE2pw->Fill((Axis_t) pos, (Axis_t) e2);
		hE3pw->Fill((Axis_t) pos, (Axis_t) detE3);
		hE4pw->Fill((Axis_t) pos, (Axis_t) detE4);
		hE5pw->Fill((Axis_t) pos, (Axis_t) detE5);

        hE1E4w->Fill((Axis_t) e1, (Axis_t) detE4);
        hE1E5w->Fill((Axis_t) e1, (Axis_t) detE5);
        hE4E5w->Fill((Axis_t) detE4, (Axis_t) detE5);

		hDtpw->Fill((Axis_t) pos, (Axis_t) detDt);
		hDppw->Fill((Axis_t) pos, (Axis_t) dp);
		hTofpw->Fill((Axis_t) pos, (Axis_t) detTof);
		hChi2pw->Fill((Axis_t) pos, (Axis_t) chi2);
	}

	return(kTRUE);
}
