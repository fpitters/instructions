#define EXTERN	1
#include "GamsAnalyze.h"
#include "GamsAnalyzeGlobals.h"

extern TMrbAnalyze * gMrbAnalyze;			// user's base object
extern TMrbLofUserVars * gMrbLofUserVars;	// user's vars & wdws
extern TMrbLogger * gMrbLog;				// message logger

extern Int_t nofTriggers;					// total number of triggers
extern Int_t nofEvents;						// total number of events

Bool_t TMrbAnalyze::InitializeUserCode(TMrbIOSpec * IOSpec, Bool_t BeforeHB) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:		   TMrbAnalyze::InitializeUserCode
// Purpose: 	   Initialize user code
// Arguments:	   TMrbIOSpec * IOSpec
// Results: 	   kTRUE/kFALSE
// Exceptions:     
// Description:    Called to initialize user code.
// Keywords:	   
//////////////////////////////////////////////////////////////////////////////

	if (BeforeHB) return(kTRUE);

	gMrbLofUserVars->ReadFromFile("vset.wdw");	//--// read initial window settings from file

	gMrbLofUserVars->ReadFromFile("vset.var");	//--// read initial variable settings from file


	gMrbLofUserVars->Print();							//--// output settings to cout

	nofTriggers = 0;                             // counts total number of triggers
	nofEvents = 0;                               // ... of events
}
