//~marabou/src/hpr/Cuts.cxx  line 292
{
   gROOT->Reset();
   gSystem->Load("/usr/local/marabou_$CERN_LEVEL/lib/libTMrbUtils.so");


//---------------------------------------------------------------------------------------
// Name des workfiles, in dem der cut abgespeichert ist:
//   TFile *cuts = new TFile("workfile.root");
//---------------------------------------------------------------------------------------
   TFile *cuts = new TFile("workfile.root");
//---------------------------------------------------------------------------------------   

//---------------------------------------------------------------------------------------
// Name des cuts in diesem workfile
// TMrbWindow2D *cutni = (TMrbWindow2D*)cuts->Get("cutname");
//---------------------------------------------------------------------------------------
   TMrbWindow2D *cutni = (TMrbWindow2D*)cuts->Get("cutname");
//---------------------------------------------------------------------------------------

   ofstream wstream;
   wstream.open("content.txt", ios::out);
      wstream << "run" << "  " << "ni" << "  " << "total" << endl;

   ifstream rstream;
   rstream.open("runs.txt", ios::in);
   TString run;
   int nruns;
   rstream >> nruns;


for (int k = 0; k < nruns; k++){
   rstream >> run;
   TFile *inf = new TFile(run.Data());

//---------------------------------------------------------------------------------------
// Alle Spektren, die ausgewertet werden sollen:
// TH2S * hist = (TH2S*)inf->Get("Spektrenname"); 
// z.B. TH2S * hist = (TH2S*)inf->Get("E4_Ort_W");
//---------------------------------------------------------------------------------------
   TH2S * hist = (TH2S*)inf->Get("E4_Ort_W");
   TH2S * hist1 = (TH2S*)inf->Get("E4_Ort");
//---------------------------------------------------------------------------------------

   cout << run << "--- Entries inside cut(s) -----------" << endl;

//---------------------------------------------------------------------------------------
// erstes Spektrum
//---------------------------------------------------------------------------------------
   TAxis* xaxis = hist->GetXaxis();
   TAxis* yaxis = hist->GetYaxis();
   Float_t entni;
   
      entni = 0;
      for(int i = 1; i <= hist->GetNbinsX(); i++){
         for(int j = 1; j <= hist->GetNbinsY(); j++){
            Axis_t xcent = xaxis->GetBinCenter(i);
            Axis_t ycent = yaxis->GetBinCenter(j);
            if(cutni->IsInside((float)xcent,(float)ycent))
            entni += hist->GetCellContent(i, j);
         }
      }
      cout << cutni->GetName() << ": Content:" << entni << endl; 

//---------------------------------------------------------------------------------------
// zweites Spektrum
//---------------------------------------------------------------------------------------
   TAxis* xaxis = hist1->GetXaxis();
   TAxis* yaxis = hist1->GetYaxis();
   Float_t entto;
   
      entto = 0;
      for(int i = 1; i <= hist1->GetNbinsX(); i++){
         for(int j = 1; j <= hist1->GetNbinsY(); j++){
            Axis_t xcent = xaxis->GetBinCenter(i);
            Axis_t ycent = yaxis->GetBinCenter(j);
            if(cutni->IsInside((float)xcent,(float)ycent))
            entto += hist1->GetCellContent(i, j);
         }
      }
      cout << cutni->GetName() << ": Content:" << entto << endl; 

//-----------------------------------------------------------------------------

//----------------------1-dim Spektrum-----------------------------------------------
//   TAxis* xaxis = hist1->GetXaxis();
//   TAxis* yaxis = hist1->GetYaxis();
//   Float_t ento;
   
//      cuto->Draw();
//      ento = 0;
  //    for(int i = 600; i <= 1100; i++){
//            ento += hist1->GetBinContent(i);
//      }
//      cout << "o: Content:" << ento << endl; 

//----------------------------------------------------------------------------


      wstream << run << "  " << entni << "  " << entto << "  " << endl;
}
   rstream.close();  
   wstream.close();  

};
//______________________________________________________________________________________
