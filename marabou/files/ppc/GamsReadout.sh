#!/bin/tcsh
#GamsReadout.sh: shell script to compile readout source GamsReadout.c

cd /project/mll-code/gams/Jan19-99Tc/ppc/
source /sys/gsi_lynx_env
set path = ( /bin /bin/ces /usr/bin /usr/local/bin /etc /usr/etc . ~/tools)
source /mbs/login v62
make -f GamsReadout.mk clean all
