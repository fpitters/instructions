void FH_setdefaults(const char *cname, const char* hname){

  gROOT->Reset();
// find pointer to HistPresent object
//  class HistPresent;
  HistPresent * mypres = (HistPresent*)gROOT->FindObject("mypres");
  if(!mypres){
     cout << "histogram presenter object not found" << endl;
     return 0;
  }
  TCanvas* canvas = (TCanvas*)gROOT->FindObject(cname);
  TH1* hist = 0;
  if(gROOT->GetVersionInt() >= 22306)
     TH1* hist = mypres->GetCurrentHist();   // assure backward comp
  else    hist = (TH1*)gROOT->FindObject(hname);

  if(!hist || !canvas){
     cout << "hist or canvas not found" << endl;
     return 0;
  }

   TString title = hist->GetTitle();
   TString name = hist->GetName();

//  global attributes

   gStyle->SetOptStat(1001111);    // draw a statistics box
                                   // Integral,(overf, underf,) mean, rms, entries, name

    gROOT->ForceStyle();            // overwrite attributes store with hists

   if(hist->InheritsFrom("TH2"))  // for 2 dim hists
   {

/*
//--------------------------------cuts---------------------------------
      TFile *cuts = new TFile("53Mn.root");
//--------------------------------detof---------------------------------
      if(title == "E3_Ort")	 // for E_FG_Tof histogram
      {
 	 TMrbWindow2D *cutni = (TMrbWindow2D*)cuts->Get("53Mn4;1");
 	 cout << "--- Entries inside cut(s) -----------" << endl;

 	 TAxis* xaxis = hist->GetXaxis();
 	 TAxis* yaxis = hist->GetYaxis();
 	 Float_t entni;

 	    cutni->Draw();
 	    entni = 0;
 	    for(int i = 1; i <= hist->GetNbinsX(); i++){
 	       for(int j = 1; j <= hist->GetNbinsY(); j++){
 		  Axis_t xcent = xaxis->GetBinCenter(i);
 		  Axis_t ycent = yaxis->GetBinCenter(j);
 		  if(cutni->IsInside((float)xcent,(float)ycent))
 		  entni += hist->GetCellContent(i, j);
 	       }
 	    }
 	    cout << cutni->GetName() << ": Content:" << entni << endl;

      }
      gDirectory = gROOT;





//--------------------------------cuts---------------------------------
      TFile *cuts = new TFile("53Mn.root");
//--------------------------------detof---------------------------------
      if(title == "E3_Ort_W")	 // for E_FG_Tof histogram
      {
 	 TMrbWindow2D *cutni = (TMrbWindow2D*)cuts->Get("53Mn4;1");
 	 cout << "--- Entries inside cut(s) -----------" << endl;

 	 TAxis* xaxis = hist->GetXaxis();
 	 TAxis* yaxis = hist->GetYaxis();
 	 Float_t entni;

 	    cutni->Draw();
 	    entni = 0;
 	    for(int i = 1; i <= hist->GetNbinsX(); i++){
 	       for(int j = 1; j <= hist->GetNbinsY(); j++){
 		  Axis_t xcent = xaxis->GetBinCenter(i);
 		  Axis_t ycent = yaxis->GetBinCenter(j);
 		  if(cutni->IsInside((float)xcent,(float)ycent))
 		  entni += hist->GetCellContent(i, j);
 	       }
 	    }
 	    cout << cutni->GetName() << ": Content:" << entni << endl;

      }
      gDirectory = gROOT;
*/

//   hist->SetMaximum(1);

      hist->SetDrawOption("box");
      hist->SetFillStyle(1);
//      hist->SetFillColor(1);
      canvas->Modified(kTRUE);
      canvas->SetLogz();                   // log scale
   } else {                          // options for 1 dim

      hist->SetDrawOption("hist");
//      hist->SetFillStyle(0);               // hollow
//      hist->SetLineColor(6);
      hist->SetFillStyle(1001);      // solid
      hist->SetFillColor(38);
   }
}
