//__________________________________________________[C++ CLASS IMPLEMENTATION]
//////////////////////////////////////////////////////////////////////////////
// Name:           GamsAnalyze.cxx
// Purpose:        Implement user-specific class methods
// Description:    GAMS experiment
//
// Header files:   GamsAnalyze.h     -- class definitions
// Author:         gams
// Revision:
// Date:           Sun Feb 17 11:13:39 2019
// URL:
// Keywords:
//////////////////////////////////////////////////////////////////////////////

#define _MARABOU_MAIN_

#define MAX_LLONG	9223372036854775807LL

#include "TEnv.h"

// class definitions
#include "GamsAnalyze.h"

// common index declarations for analysis and readout
#include "GamsCommonIndices.h"

#include "SetColor.h"

#include <pthread.h>  // pthread header file


// global objects
#include "GamsAnalyzeGlobals.h"




// implement class defs
ClassImp(TUsrEvtDevt)
ClassImp(TUsrEvtXstop)
ClassImp(TUsrSevtDet)
ClassImp(TUsrSevtSsca)
ClassImp(TMrbSubevent_Caen)

extern pthread_mutex_t global_data_mutex;		// global pthread mutex to protect TMapped data

extern TMrbLogger * gMrbLog;					// MARaBOU's logging mechanism

extern TMrbTransport * gMrbTransport;			// base class for MBS transport
extern TMrbAnalyze * gMrbAnalyze;				// base class for user's analysis

extern TUsrEvtXstop * gStopEvent;				// stop acquisition, trigger 15, user-defined
extern TUsrEvtStart * gStartEvent;				// start acquisition, trigger 14
extern TUsrDeadTime * gDeadTime;				// dead-time data

extern TMrbLofUserVars * gMrbLofUserVars;		// list of user vars/wdws

static Bool_t verboseMode;						// verbosity flag;
static Bool_t forcedDump = kFALSE;				// kTRUE -> dump records
static Int_t recNo = 0;

static TArrayI lofModuleIds;

Bool_t TMrbAnalyze::Initialize(TMrbIOSpec * IOSpec) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TMrbAnalyze::Initialize
// Purpose:        Instantiate user objects, initialize values
// Arguments:      TMrbIOSpec * IOSpec  -- i/o specifications
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Instantiates events, histgrams, windows etc. and
//                 initializes values.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	verboseMode = (Bool_t) gEnv->GetValue("TMrbAnalyze.VerboseMode", kFALSE);
	forcedDump = (Bool_t) gEnv->GetValue("TMrbAnalyze.ForcedDump", kFALSE);

// add user's environment defs from file .GamsConfig.rc
	Bool_t fileOk = this->AddResourcesFromFile(".GamsConfig.rc");
	if (verboseMode && fileOk) {
		gMrbLog->Out()	<< "Reading user's environment defs from file .GamsConfig.rc" << endl;
		gMrbLog->Flush(this->ClassName(), "Initialize", setblue);
	}

// initialize histogram arrays

// initialize user lists: modules, params & histos
 	gMrbAnalyze->InitializeLists(2, 64);
		lofModuleIds.Set(2);
	gMrbAnalyze->AddModuleToList("adc", "CAEN V785 ADC 32 x 12 bit", kMrbModuleAdc, 0, 32, 0);
	lofModuleIds[kMrbModuleAdc] = kMrbModIdCaen_v785;
	gMrbAnalyze->AddModuleToList("sca", "SIS 3820 scaler 32 x 32 bit", kMrbModuleSca, 32, 32, 0);
	lofModuleIds[kMrbModuleSca] = kMrbModIdSis_3820;


	gDirectory = gROOT;

// instantiate event classes and book histograms
	if (gDevt == NULL) {
		gDevt = new TUsrEvtDevt();		// event devt, trigger 1
 		Bool_t err = gDevt->IsZombie();
 		if (!err) {
			err |= !gDevt->BookParams();
			err |= !gDevt->BookHistograms();
			err |= !gDevt->AllocHitBuffers();
 		}
 		if (err) {
 			gMrbLog->Err()	<< "Something went wrong while creating event \"devt\"" << endl;
 			gMrbLog->Flush(this->ClassName(), "Initialize");
 		}
	}
		gStopEvent = new TUsrEvtXstop();		// event xstop, trigger 15
 		Bool_t err = gStopEvent->IsZombie();
 		if (!err) {
			err |= !gStopEvent->BookParams();
			err |= !gStopEvent->BookHistograms();
			err |= !gStopEvent->AllocHitBuffers();
 		}
 		if (err) {
 			gMrbLog->Err()	<< "Something went wrong while creating event \"xstop\"" << endl;
 			gMrbLog->Flush(this->ClassName(), "Initialize");
 		}


	Double_t cut_x0 = 0.; Double_t cut_y0 = 0.;
	if (wraw == NULL) wraw = new TMrbWindowI("wraw", 0, 0);
	if (wet == NULL) wet = new TMrbWindowI("wet", 0, 0);
	if (we1 == NULL) we1 = new TMrbWindowI("we1", 0, 0);
	if (we2 == NULL) we2 = new TMrbWindowI("we2", 0, 0);
	if (we3 == NULL) we3 = new TMrbWindowI("we3", 0, 0);
	if (we4 == NULL) we4 = new TMrbWindowI("we4", 0, 0);
	if (we5 == NULL) we5 = new TMrbWindowI("we5", 0, 0);
	if (wdt == NULL) wdt = new TMrbWindowI("wdt", 0, 0);
	if (wdp == NULL) wdp = new TMrbWindowI("wdp", 0, 0);
	if (wp == NULL) wp = new TMrbWindowI("wp", 0, 0);
	if (wp2 == NULL) wp2 = new TMrbWindowI("wp2", 0, 0);
	if (wpup == NULL) wpup = new TMrbWindowI("wpup", 0, 0);
	if (wtof == NULL) wtof = new TMrbWindowI("wtof", 0, 0);
	if (wtime == NULL) wtime = new TMrbWindowI("wtime", 0, 0);
	if (wchi2 == NULL) wchi2 = new TMrbWindowF("wchi2", 0.0, 0.0);




// initialize user code (objects, variables ...) - 1st call *BEFORE* booking histograms
	this->InitializeUserCode(IOSpec, kTRUE);

// book histograms defined by user
	if (hE1 == NULL) hE1 = new TH1F("hE1", "E1", 4096, 0.0, 4096.0);
	if (hE2 == NULL) hE2 = new TH1F("hE2", "E2", 4096, 0.0, 4096.0);
	if (hP == NULL) hP = new TH1F("hP", "Ort", 4096, 0.0, 4096.0);
	if (hP2 == NULL) hP2 = new TH1F("hP2", "Ort_2", 4096, 0.0, 4096.0);
	if (hDp == NULL) hDp = new TH1F("hDp", "x_Winkel", 4096, 0.0, 4096.0);
	if (hTime == NULL) hTime = new TH1F("hTime", "Time", 16384, 0.0, 16384.0);
	if (hChi2 == NULL) hChi2 = new TH1F("hChi2", "Chi2", 5001, 0.0, 1000.0);
	if (hEtp == NULL) hEtp = new TH2S("hEtp", "Etot_Ort", 64, 0.0, 4096.0, 256, 0.0, 4096.0);
	if (hE1p == NULL) hE1p = new TH2S("hE1p", "E1_Ort", 64, 0.0, 4096.0, 256, 0.0, 4096.0);
	if (hE2p == NULL) hE2p = new TH2S("hE2p", "E2_Ort", 64, 0.0, 4096.0, 256, 0.0, 4096.0);
	if (hE3p == NULL) hE3p = new TH2S("hE3p", "E3_Ort", 64, 0.0, 4096.0, 256, 0.0, 4096.0);
	if (hE4p == NULL) hE4p = new TH2S("hE4p", "E4_Ort", 64, 0.0, 4096.0, 256, 0.0, 4096.0);
	if (hE5p == NULL) hE5p = new TH2S("hE5p", "E5_Ort", 64, 0.0, 4096.0, 256, 0.0, 4096.0);
	if (hDtp == NULL) hDtp = new TH2S("hDtp", "y_Winkel_Ort", 64, 0.0, 4096.0, 256, 0.0, 4096.0);
	if (hTofp == NULL) hTofp = new TH2S("hTofp", "TOF_Ort", 64, 0.0, 4096.0, 1024, 0.0, 4096.0);
	if (hDpp == NULL) hDpp = new TH2S("hDpp", "x_Winkel_Ort", 64, 0.0, 4096.0, 256, 0.0, 4096.0);
	if (hChi2p == NULL) hChi2p = new TH2S("hChi2p", "Chi2_Ort", 256, 0.0, 4096.0, 1000, 0.0, 99.9);
	if (hE1E4 == NULL) hE1E4 = new TH2S("hE1E4", "E1_E4", 64, 0.0, 4096.0, 256, 0.0, 4096.0);
	if (hE1E5 == NULL) hE1E5 = new TH2S("hE1E5", "E1_E5", 64, 0.0, 4096.0, 256, 0.0, 4096.0);
	if (hE4E5 == NULL) hE4E5 = new TH2S("hE4E5", "E4_E5", 64, 0.0, 4096.0, 256, 0.0, 4096.0);
	if (hEtw == NULL) hEtw = new TH1F("hEtw", "Etot_W", 4096, 0.0, 4096.0);
	if (hE1w == NULL) hE1w = new TH1F("hE1w", "E1_W", 4096, 0.0, 4096.0);
	if (hE2w == NULL) hE2w = new TH1F("hE2w", "E2_W", 4096, 0.0, 4096.0);
	if (hE3w == NULL) hE3w = new TH1F("hE3w", "E3_W", 4096, 0.0, 4096.0);
	if (hE4w == NULL) hE4w = new TH1F("hE4w", "E4_W", 4096, 0.0, 4096.0);
	if (hE5w == NULL) hE5w = new TH1F("hE5w", "E5_W", 4096, 0.0, 4096.0);
	if (hPw == NULL) hPw = new TH1F("hPw", "Ort_W", 4096, 0.0, 4096.0);
	if (hP2w == NULL) hP2w = new TH1F("hP2w", "Ort2_W", 4096, 0.0, 4096.0);
	if (hDtw == NULL) hDtw = new TH1F("hDtw", "y_Winkel_W", 4096, 0.0, 4096.0);
	if (hTofw == NULL) hTofw = new TH1F("hTofw", "TOF_W", 4096, 0.0, 4096.0);
	if (hDpw == NULL) hDpw = new TH1F("hDpw", "x_Winkel_W", 4096, 0.0, 4096.0);
	if (hTimew == NULL) hTimew = new TH1F("hTimew", "Time_W", 16384, 0.0, 16384.0);
	if (hChi2w == NULL) hChi2w = new TH1F("hChi2w", "Chi2_W", 5000, 0.0, 499.9);
	if (hEtpw == NULL) hEtpw = new TH2S("hEtpw", "Etot_Ort_W", 128, 0.0, 4096.0, 512, 0.0, 4096.0);
	if (hE1pw == NULL) hE1pw = new TH2S("hE1pw", "E1_Ort_W", 128, 0.0, 4096.0, 512, 0.0, 4096.0);
	if (hE2pw == NULL) hE2pw = new TH2S("hE2pw", "E2_Ort_W", 128, 0.0, 4096.0, 512, 0.0, 4096.0);
	if (hE3pw == NULL) hE3pw = new TH2S("hE3pw", "E3_Ort_W", 128, 0.0, 4096.0, 512, 0.0, 4096.0);
	if (hE4pw == NULL) hE4pw = new TH2S("hE4pw", "E4_Ort_W", 128, 0.0, 4096.0, 512, 0.0, 4096.0);
	if (hE5pw == NULL) hE5pw = new TH2S("hE5pw", "E5_Ort_W", 128, 0.0, 4096.0, 512, 0.0, 4096.0);
	if (hE1E4w == NULL) hE1E4w = new TH2S("hE1E4w", "E1_E4_W", 128, 0.0, 4096.0, 512, 0.0, 4096.0);
	if (hE1E5w == NULL) hE1E5w = new TH2S("hE1E5w", "E1_E5_W", 128, 0.0, 4096.0, 512, 0.0, 4096.0);
	if (hE4E5w == NULL) hE4E5w = new TH2S("hE4E5w", "E4_E5_W", 128, 0.0, 4096.0, 512, 0.0, 4096.0);
	if (hDtpw == NULL) hDtpw = new TH2S("hDtpw", "y_Winkel_Ort_W", 128, 0.0, 4096.0, 512, 0.0, 4096.0);
	if (hTofpw == NULL) hTofpw = new TH2S("hTofpw", "TOF_Ort_W", 128, 0.0, 4096.0, 1024, 0.0, 4096.0);
	if (hDppw == NULL) hDppw = new TH2S("hDppw", "x_Winkel_Ort_W", 128, 0.0, 4096.0, 512, 0.0, 4096.0);
	if (hChi2pw == NULL) hChi2pw = new TH2S("hChi2pw", "Chi2_Ort_W", 256, 0.0, 4096.0, 1000, 0.0, 99.9);


// initialize user code (objects, variables ...) - 2nd call *AFTER* booking histograms
	this->InitializeUserCode(IOSpec, kFALSE);

	return(kTRUE);
}

Bool_t TMrbAnalyze::HandleMessage(const Char_t * Args){
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TMrbAnalyze::HandleMessage
// Purpose:        Execute a message
// Arguments:      Char_t * Args   -- argument list
// Results:        kTRUE/kFALSE
// Description:    Called by message handler.
//                 Format of argument string depends on caller's definitions
//                 and has to be decoded according to his requirements.
// Exceptions:
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	return(kFALSE);
};

Bool_t TMrbAnalyze::ReloadParams(TMrbIOSpec * IOSpec) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TMrbAnalyze::ReloadParams
// Purpose:        Reload parameters from file
// Arguments:      TMrbIOSpec * IOSpec   -- i/o specifications
// Results:        kTRUE/kFALSE
// Description:    Reloads params (vars, wdws, functions etc.) from root file
// Exceptions:
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Bool_t sts = this->ReloadVarsAndWdws(IOSpec);
	this->AdjustWindowPointers();
	this->AdjustFunctionPointers();
	return(sts);
};

Bool_t TMrbAnalyze::FinishRun(TMrbIOSpec * IOSpec, Bool_t BeforeSH) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TMrbAnalyze::FinishRun
// Purpose:        Do some work at end of run
// Arguments:      TMrbIOSpec * IOSpec   -- i/o specifications
//                 Bool_t BeforeSH       -- kTRUE if before saving histos
// Results:        kTRUE/kFALSE
// Description:    User may overwrite this method by his own
// Exceptions:
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	return(kTRUE);
};

Bool_t TMrbAnalyze::AdjustWindowPointers() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TMrbAnalyze::AdjustWindowPointers
// Purpose:        Adjust pointers to windows after reload
// Arguments:      --
// Results:        kTRUE/kFALSE
// Description:    Update pointers to windows (2-dim only).
// Exceptions:
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	return(kTRUE);
};

Bool_t TMrbAnalyze::AdjustFunctionPointers() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TMrbAnalyze::AdjustFunctionPointers
// Purpose:        Adjust pointers to functions after reload
// Arguments:      --
// Results:        kTRUE/kFALSE
// Description:    Update pointers to functions (TF1 and TF2).
// Exceptions:
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	return(kTRUE);
};

Bool_t TMrbAnalyze::WriteRootTree(TMrbIOSpec * IOSpec) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TMrbAnalyze::WriteRootTree
// Purpose:        Open a file to write raw data to tree
// Arguments:      TMrbIOSpec * IOSpec   -- i/o specifications
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Opens a ROOT file and creates trees.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	TString rootFile;

	if ((IOSpec->GetOutputMode() & TMrbIOSpec::kOutputOpen) == 0) return(kTRUE);

	if (fWriteRootTree) {
		gMrbLog->Err()	<< "ROOT file already open - " << fRootFileOut->GetName() << endl;
		gMrbLog->Flush(this->ClassName(), "WriteRootTree");
		return(kFALSE);
	}

	rootFile = IOSpec->GetOutputFile();

	if (verboseMode) {
		gMrbLog->Out()	<< "Opening ROOT file \"" << rootFile << "\"" << endl;
		gMrbLog->Flush(this->ClassName(), "WriteRootTree");
	}

	fRootFileOut = new TFile(rootFile.Data(), "RECREATE", "GAMS experiment");
	fRootFileOut->SetCompressionLevel(1);
	fWriteRootTree = kTRUE;

// create trees for start/stop info
	gStartEvent->CreateTree();
	gStopEvent->CreateTree();

// create tree to hold dead-time data
	gDeadTime->CreateTree();

// create ROOT trees for each user event
 	gDevt->CreateTree();

// control output of tree data individually for each event
 	gDevt->WriteTree(gEnv->GetValue(gMrbAnalyze->GetResource("Event.Devt.WriteTree"), kTRUE));
 	if (!gDevt->TreeToBeWritten()) {
 		gMrbLog->Wrn()	<< "Output of tree data TURNED OFF for event \"devt\"" << endl;
 		gMrbLog->Flush(this->ClassName(), "WriteRootTree");
 	}

	return(kTRUE);
}

Bool_t TMrbAnalyze::OpenRootFile(TMrbIOSpec * IOSpec) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TMrbAnalyze::OpenRootFile
// Purpose:        Open a file to replay tree data
// Arguments:      TMrbIOSpec * IOSpec    -- i/o specifications
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Opens a ROOT file for input.
//                 Replay mode only.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	TString rootFile;

	if ((IOSpec->GetInputMode() & TMrbIOSpec::kInputRoot) == 0) return(kTRUE);

	if (fRootFileIn != NULL) {
		gMrbLog->Err()	<< "ROOT file already open - " << fRootFileIn->GetName() << endl;
		gMrbLog->Flush(this->ClassName(), "OpenRootFile");
		return(kFALSE);
	}

	rootFile = IOSpec->GetInputFile();

	if (rootFile.CompareTo("fake.root") == 0) {
		this->SetFakeMode();
// set fake mode for all trees
 		gDevt->SetFakeMode();
 		gStopEvent->SetFakeMode();
		if (verboseMode) {
			gMrbLog->Out()	<< "Running in FAKE mode" << endl;
			gMrbLog->Flush(this->ClassName(), "OpenRootFile");
		}
	} else {
		gSystem->ExpandPathName(rootFile);
		if(gSystem->AccessPathName(rootFile, kFileExists)){
			gMrbLog->Err()	<< "No such file - " << rootFile << endl;
			gMrbLog->Flush(this->ClassName(), "OpenRootFile");
			return(kFALSE);
		}
		fRootFileIn = new TFile(rootFile.Data(), "READ");
		if (fRootFileIn->IsZombie() || !fRootFileIn->IsOpen()) {
			gMrbLog->Err()	<< "Can't open ROOT file - " << rootFile << endl;
			gMrbLog->Flush(this->ClassName(), "OpenRootFile");
			return(kFALSE);
		} else if (verboseMode) {
			gMrbLog->Out()	<< "Opening ROOT file \"" << rootFile << "\"" << endl;
			gMrbLog->Flush(this->ClassName(), "OpenRootFile");
		}

// initialize ROOT trees
 		gDevt->InitializeTree(fRootFileIn);

// initialize trees containing start/stop information
		gStartEvent->InitializeTree(fRootFileIn);
		gStopEvent->InitializeTree(fRootFileIn);

// initialize dead-time tree
		gDeadTime->InitializeTree(fRootFileIn);

// turn on replay mode
		fReplayMode = kTRUE;
 		gDevt->SetReplayMode(gEnv->GetValue(gMrbAnalyze->GetResource("Event.Devt.ReplayMode"), kTRUE));
 		if (!gDevt->IsReplayMode()) {
 			gMrbLog->Wrn()	<< "Replay of tree data TURNED OFF for event \"devt\"" << endl;
 			gMrbLog->Flush(this->ClassName(), "OpenRootFile");
 		}
 		gStopEvent->SetReplayMode(gEnv->GetValue(gMrbAnalyze->GetResource("Event.StopEvent.ReplayMode"), kTRUE));
 		if (!gStopEvent->IsReplayMode()) {
 			gMrbLog->Wrn()	<< "Replay of tree data TURNED OFF for event \"stopEvent\"" << endl;
 			gMrbLog->Flush(this->ClassName(), "OpenRootFile");
 		}
	}
	return(kTRUE);
}

Bool_t TMrbAnalyze::ReplayEvents(TMrbIOSpec * IOSpec) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TMrbAnalyze::ReplayEvents
// Purpose:        Read data from ROOT file
// Arguments:      TMrbIOSpec * IOSpec  -- i/o specifications
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Replays event data.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Bool_t sts;

	if (!this->IsFakeMode()) {
		if (fRootFileIn == NULL) {
			gMrbLog->Err()	<< "No ROOT file open" << endl;
			gMrbLog->Flush(this->ClassName(), "ReplayEvents");
			return(kFALSE);
		}

// output start/stop time stamps
		this->PrintStartStop();
	}

// replay tree data for each event with replay mode turned-on
	sts = kTRUE;
 if (gDevt->IsReplayMode()) {
 	if (!gDevt->ReplayTreeData(IOSpec)) sts = kFALSE;
 }
 if (gStopEvent->IsReplayMode()) {
 	if (!gStopEvent->ReplayTreeData(IOSpec)) sts = kFALSE;
 }

	return(sts);
}

void TMrbAnalyze::SetScaleDown(Int_t ScaleDown) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TMrbAnalyze::SetScaleDown
// Purpose:        Set a scale-down value common to all events
// Arguments:      Int_t ScaleDown    -- scale-down factor
// Results:
// Exceptions:
// Description:    Defines a global scale-down factor valid for all events.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	fScaleDown = ScaleDown;

// set scale down individually for each event
 	gDevt->SetScaleDown(ScaleDown);
 	gStopEvent->SetScaleDown(ScaleDown);
}

Bool_t TMrbSubevent_Caen::FillSubevent(const UShort_t * SevtData, Int_t SevtWC, Int_t TimeStamp) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:		   TMrbSubevent_Caen::FillSubevent
// Purpose: 	   Decode CAEN buffer
// Arguments:	   const UShort_t * SevtData   -- pointer to unpacked subevent data
//  			   Int_t SevtWC 			   -- word count
//  			   Int_t TimeStamp  		   -- time since last start
//  											  in units of 100 microsecs
// Results: 	   kTRUE/kFALSE
// Exceptions:     
// Description:    Decodes data from a CAEN Vxxx.
// Keywords:	   
//////////////////////////////////////////////////////////////////////////////

	fTimeStamp = TimeStamp; 				// store time since last "start acquisition"
	this->SetTimeStamp(TimeStamp);			// passed as argument by parent event

	this->GetHitBuffer()->Reset();			// clear hit buffer

	recNo++;						// count this record	
	Int_t origSevtWC = SevtWC;				// save original subevent wc
	if (forcedDump && (SevtWC > 0)) gMrbAnalyze->DumpData("sevt", recNo, this->ClassName(), "FillSubevent", "Forced dump", SevtData, origSevtWC);
	Int_t wordsProcessed = 0; 			// words processed so far
	UShort_t * dataPtr = (UShort_t *) SevtData;
	Int_t eventsProcessed = 0;
	while (SevtWC > 0) {									// subevent may contain several CAEN modules
		Int_t header = (*dataPtr++) << 16; 					// header word (MSB)
		header |= *dataPtr++; 								// header word (LSB)
		if ((header & CAEN_Vxxx_D_HDR) == 0) {
			gMrbAnalyze->DumpData("sevt", recNo, this->ClassName(), "FillSubevent", "Wrong header", SevtData, origSevtWC);
			cerr	<< setred
					<< this->ClassName() << "::FillSubevent() [sevtWC="
					<< wordsProcessed << "] Wrong header - 0x" << setbase(16) << header << setbase(10)
					<< setblack << endl;
			return(kTRUE);
		}
		Int_t wc = (header >> CAEN_Vxxx_SH_WC) & CAEN_Vxxx_M_WC;				// extract wc
		Int_t wcs = wc;
		if (wc < 0 || wc > 32) {				// max 32 data words
			gMrbAnalyze->DumpData("sevt", recNo, this->ClassName(), "FillSubevent", "Wrong word count", SevtData, origSevtWC);
			cerr	<< setred
					<< this->ClassName() << "::FillSubevent() [sevtWC="
					<< wordsProcessed << "] Wrong word count - " << wc << setblack << endl;
			return(kTRUE);
		}
		Int_t moduleNumber = header & CAEN_Vxxx_M_MSERIAL;	// extract module number
		TMrbModuleListEntry * mle = gMrbAnalyze->GetModuleListEntry(moduleNumber);
		if (mle == NULL) {
			gMrbAnalyze->DumpData("sevt", recNo, this->ClassName(), "FillSubevent", "Wrong module number", SevtData, origSevtWC);
			cerr	<< setred
					<< this->ClassName() << "::FillSubevent() [sevtWC="
					<< wordsProcessed << "] Wrong module number - " << moduleNumber
					<< setblack << endl;
			return(kTRUE);
		}

		if (eventsProcessed >= CAEN_Vxxx_N_MAXEVENTS) {
			gMrbAnalyze->DumpData("sevt", recNo, this->ClassName(), "FillSubevent", "Too many events", SevtData, origSevtWC);
			cerr	<< setred
					<< this->ClassName() << "::FillSubevent(): [Module "
					<< gMrbAnalyze->GetModuleName(moduleNumber) << " (" << moduleNumber << "), sevtWC="
					<< wordsProcessed << "] Too many events - max " << CAEN_Vxxx_N_MAXEVENTS << setblack << endl;
			return(kTRUE);
		}

		mle->IncrEventHits();				// count this event

		UShort_t evtData[2];
		for (; wc; wc--) {
			Int_t data = (*dataPtr++) << 16;		// data (MSB)
			data |= *dataPtr++; 			// data (LSB)
			if ((data & CAEN_Vxxx_M_ID) != CAEN_Vxxx_D_DATA) {
				if ((data & CAEN_Vxxx_M_ID) == CAEN_Vxxx_D_EOB) {
					gMrbAnalyze->DumpData("sevt", recNo, this->ClassName(), "FillSubevent", "Unexpected EOB", SevtData, origSevtWC);
					cerr	<< setred
							<< this->ClassName() << "::FillSubevent(): [Module "
							<< gMrbAnalyze->GetModuleName(moduleNumber) << " (" << moduleNumber << "), sevtWC=" << wordsProcessed
							<< "] Unexpected EOB - remaining wc=" << wc << " ignored. Continuing with next event."
							<< setblack << endl;
					dataPtr -= 2;
					wcs -= wc;
					break;
				} else {
					gMrbAnalyze->DumpData("sevt", recNo, this->ClassName(), "FillSubevent", "Wrong data", SevtData, origSevtWC);
					cerr	<< setred
							<< this->ClassName() << "::FillSubevent(): [Module "
							<< gMrbAnalyze->GetModuleName(moduleNumber) << " (" << moduleNumber << "), sevtWC="
							<< wordsProcessed << "] Wrong data - 0x" << setbase(16) << data << setbase(10)
							<< setblack << endl;
					return(kTRUE);
				}
			}
			Int_t chn = (data >> CAEN_Vxxx_SH_CHN) & CAEN_Vxxx_M_CHN;
			mle->IncrChannelHits(chn);			// count this channel
			evtData[0] = 0;
			if (data & CAEN_Vxxx_B_OVERFLOW) evtData[1] = CAEN_Vxxx_B_OVERFLOW;
			else if (data & CAEN_Vxxx_B_UNDERTHRESH) evtData[1] = (UShort_t) -1;
			else evtData[1] = (UShort_t) (data & CAEN_Vxxx_M_ADCDATA);
			TUsrHit * hit = this->GetHitBuffer()->AddHit(	eventsProcessed,
											moduleNumber,
											chn,
											NULL,
											evtData, 2);
			if (hit == NULL) {
				cerr	<< setred
					<< this->ClassName() << "::FillSubevent() [sevtWC="
					<< wordsProcessed << "] Error while filling hitbuffer - giving up"
					<< setblack << endl;
				return(kTRUE);
			}
		}
		Int_t trailer = (*dataPtr++) << 16; 					// trailer word (MSB)
		trailer |= *dataPtr++; 									// trailer word (LSB)
		if ((trailer & CAEN_Vxxx_D_EOB) == 0) {
			gMrbAnalyze->DumpData("sevt", recNo, this->ClassName(), "FillSubevent", "Wrong EOB", SevtData, origSevtWC);
			cerr	<< setred
					<< this->ClassName() << "::FillSubevent(): [Module "
					<< gMrbAnalyze->GetModuleName(moduleNumber) << " (" << moduleNumber << "), sevtWC="
					<< wordsProcessed << "] Wrong EOB - 0x" << setbase(16) << trailer << setbase(10)
					<< setblack << endl;
			return(kTRUE);
		}
		wcs = (wcs + 2) * 2;
		SevtWC -= wcs;						// remaining subevent data
		wordsProcessed += wcs;
		eventsProcessed++;
	}
	return(kTRUE);
}

Bool_t TUsrSevtDet::BookHistograms() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtDet::BookHistograms
// Purpose:        Define histograms for subevent det
// Arguments:
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Instantiates histograms for subevent det.
//                 Each histogram will be booked only once.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	if (hEt == NULL) {
 			hEt = new TH1F("hEt", "det.et", 4096, 0.0, 4096.0);
 			gMrbAnalyze->AddHistoToList(hEt, kMrbModuleAdc, 0);
 	}
	if (hE1l == NULL) {
 			hE1l = new TH1F("hE1l", "det.e1l", 4096, 0.0, 4096.0);
 			gMrbAnalyze->AddHistoToList(hE1l, kMrbModuleAdc, 1);
 	}
	if (hE1r == NULL) {
 			hE1r = new TH1F("hE1r", "det.e1r", 4096, 0.0, 4096.0);
 			gMrbAnalyze->AddHistoToList(hE1r, kMrbModuleAdc, 2);
 	}
	if (hE2l == NULL) {
 			hE2l = new TH1F("hE2l", "det.e2l", 4096, 0.0, 4096.0);
 			gMrbAnalyze->AddHistoToList(hE2l, kMrbModuleAdc, 3);
 	}
	if (hE2r == NULL) {
 			hE2r = new TH1F("hE2r", "det.e2r", 4096, 0.0, 4096.0);
 			gMrbAnalyze->AddHistoToList(hE2r, kMrbModuleAdc, 4);
 	}
	if (hE3 == NULL) {
 			hE3 = new TH1F("hE3", "det.e3", 4096, 0.0, 4096.0);
 			gMrbAnalyze->AddHistoToList(hE3, kMrbModuleAdc, 5);
 	}
	if (hE4 == NULL) {
 			hE4 = new TH1F("hE4", "det.e4", 4096, 0.0, 4096.0);
 			gMrbAnalyze->AddHistoToList(hE4, kMrbModuleAdc, 6);
 	}
	if (hE5 == NULL) {
 			hE5 = new TH1F("hE5", "det.e5", 4096, 0.0, 4096.0);
 			gMrbAnalyze->AddHistoToList(hE5, kMrbModuleAdc, 7);
 	}
	if (hDt == NULL) {
 			hDt = new TH1F("hDt", "det.dt", 4096, 0.0, 4096.0);
 			gMrbAnalyze->AddHistoToList(hDt, kMrbModuleAdc, 8);
 	}
	if (hTof == NULL) {
 			hTof = new TH1F("hTof", "det.tof", 4096, 0.0, 4096.0);
 			gMrbAnalyze->AddHistoToList(hTof, kMrbModuleAdc, 9);
 	}
	if (hPup == NULL) {
 			hPup = new TH1F("hPup", "det.pup", 4096, 0.0, 4096.0);
 			gMrbAnalyze->AddHistoToList(hPup, kMrbModuleAdc, 10);
 	}

	return(kTRUE);
}

Bool_t TUsrSevtDet::BookParams() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name: 		  TUsrSevtDet::BookParams
// Purpose:  	  Fill param list for subevent det
// Arguments:
// Results:  	  kTRUE/kFALSE
// Exceptions:
// Description:    Generates entries in the list of params.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

 	if (gMrbAnalyze->FindParam("et") == NULL) gMrbAnalyze->AddParamToList("et", (TObject *) &et, kMrbModuleAdc, 0);
 	if (gMrbAnalyze->FindParam("e1l") == NULL) gMrbAnalyze->AddParamToList("e1l", (TObject *) &e1l, kMrbModuleAdc, 1);
 	if (gMrbAnalyze->FindParam("e1r") == NULL) gMrbAnalyze->AddParamToList("e1r", (TObject *) &e1r, kMrbModuleAdc, 2);
 	if (gMrbAnalyze->FindParam("e2l") == NULL) gMrbAnalyze->AddParamToList("e2l", (TObject *) &e2l, kMrbModuleAdc, 3);
 	if (gMrbAnalyze->FindParam("e2r") == NULL) gMrbAnalyze->AddParamToList("e2r", (TObject *) &e2r, kMrbModuleAdc, 4);
 	if (gMrbAnalyze->FindParam("e3") == NULL) gMrbAnalyze->AddParamToList("e3", (TObject *) &e3, kMrbModuleAdc, 5);
 	if (gMrbAnalyze->FindParam("e4") == NULL) gMrbAnalyze->AddParamToList("e4", (TObject *) &e4, kMrbModuleAdc, 6);
 	if (gMrbAnalyze->FindParam("e5") == NULL) gMrbAnalyze->AddParamToList("e5", (TObject *) &e5, kMrbModuleAdc, 7);
 	if (gMrbAnalyze->FindParam("dt") == NULL) gMrbAnalyze->AddParamToList("dt", (TObject *) &dt, kMrbModuleAdc, 8);
 	if (gMrbAnalyze->FindParam("tof") == NULL) gMrbAnalyze->AddParamToList("tof", (TObject *) &tof, kMrbModuleAdc, 9);
 	if (gMrbAnalyze->FindParam("pup") == NULL) gMrbAnalyze->AddParamToList("pup", (TObject *) &pup, kMrbModuleAdc, 10);

	return(kTRUE);
}

Bool_t TUsrSevtDet::FillHistograms() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtDet::FillHistograms
// Purpose:        Accumulate histograms for subevent det
// Arguments:
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Loops thru hitbuffer and performs add-one to histograms.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	for (Int_t i = 0; i < fHitBuffer.GetNofHits(); i++) {
		TUsrHit * hit = fHitBuffer.GetHitAt(i);
		hit->FillHistogram();
	}
	gMrbAnalyze->SetUpdateFlag(); 		// activate update handler
	return(kTRUE);
}

Bool_t TUsrSevtDet::AddBranch(TTree * Tree) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtDet::AddBranch
// Purpose:        Create a branch for subevent det
// Arguments:      TTree * Tree      -- pointer to parent's tree struct
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Calls method TTree::Branch for this subevent.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Int_t bufSize;
	Int_t splitMode;

	if (gMrbAnalyze->TreeToBeWritten()) {
		bufSize = gMrbAnalyze->GetBranchSize();
		splitMode = gMrbAnalyze->GetSplitLevel();
		if (verboseMode) {
			cout	<< this->ClassName() << "::AddBranch(): adding branch \"det\" to tree \""
					<< Tree->GetName() << "\" (bufsiz="
					<< bufSize
					<< ", split=" << splitMode << ")"
					<< endl;
		}
		fMyAddr = (TObject *) this; 		// we need a '**' ref to this object
		Tree->Branch( 	"det", this->ClassName(),
						&fMyAddr,
						bufSize,
						splitMode);
	}
	return(kTRUE);
}

Bool_t TUsrSevtDet::InitializeBranch(TTree * Tree) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtDet::InitializeBranch
// Purpose:        Initialize branch addresses for subevent det
// Arguments:      TTree * Tree      -- pointer to parent's tree struct
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Set branch address for subevent det.
//                 Subevent will be fetched from tree AS A WHOLE.
//                 >> Replay mode only <<
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	fBranch = Tree->GetBranch("det");
	fMyAddr = this;
	fBranch->SetAddress(&fMyAddr);

	return(kTRUE);
}

void TUsrSevtDet::Reset(Int_t InitValue, Bool_t DataOnly) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtDet::Reset
// Purpose:        Reset subevent data
// Arguments:      Int_t InitValue   -- initialization value
//                 Bool_t DataOnly   -- do not reset hit buffer if kFALSE
// Results:        --
// Exceptions:
// Description:    Initializes subevent data (InitValue ignored!)
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	if (!DataOnly) this->GetHitBuffer()->Reset();			// reset hit buffer
	et.ClearData();	// det.et
	e1l.ClearData();	// det.e1l
	e1r.ClearData();	// det.e1r
	e2l.ClearData();	// det.e2l
	e2r.ClearData();	// det.e2r
	e3.ClearData();	// det.e3
	e4.ClearData();	// det.e4
	e5.ClearData();	// det.e5
	dt.ClearData();	// det.dt
	tof.ClearData();	// det.tof
	pup.ClearData();	// det.pup
}

Bool_t TUsrSevtSsca::FillSubevent(const UShort_t * SevtData, Int_t SevtWC, Int_t TimeStamp) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtSsca::FillSubevent
// Purpose:        Fill local instance of subevent ssca with MBS data
// Arguments:      const UShort_t * SevtData   -- pointer to unpacked subevent data
//                 Int_t SevtWC                -- word count
//                 Int_t TimeStamp             -- time since last start
//                                                in units of 100 microsecs
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Assigns MBS data to corresponding data members
//                 of subevent ssca.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Int_t i = 0;
 Int_t pIdx = 0;

	if (SevtWC == 0) return(kTRUE);

	if (SevtWC != 64) {	// test word count
		cerr	<< setred << this->ClassName() << "::FillSubevent(): ssca (Serial=2): Wrong number of data words - "
				<< SevtWC << " (should be 64)"
				<< setblack << endl;
	}
	fTimeStamp = TimeStamp; 				// store time since last "start acquisition"
	this->SetTimeStamp(TimeStamp);			// passed as argument by parent event

// pass MBS data to subevent
// start of module sca, SIS 3820 scaler 32 x 32 bit, Serial=2
	for (i = 0; i < 32; i++, pIdx += 2) scdata[i] = (Int_t) ((SevtData[pIdx] << 16) | SevtData[pIdx+1]);	// ssca.scdata[32]

	return(kTRUE);
}

Bool_t TUsrSevtSsca::BookParams() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name: 		  TUsrSevtSsca::BookParams
// Purpose:  	  Fill param list for subevent ssca
// Arguments:
// Results:  	  kTRUE/kFALSE
// Exceptions:
// Description:    Generates entries in the list of params.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	for (Int_t i = 0; i < 32; i++) {
 		TString pName = Form("scdata%d", i);
 		if (gMrbAnalyze->FindParam(pName.Data()) == NULL) gMrbAnalyze->AddParamToList(pName.Data(), (TObject *) &scdata[i], kMrbModuleSca, 0 + i);
 	}

	return(kTRUE);
}

Bool_t TUsrSevtSsca::BookHistograms() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtSsca::BookHistograms
// Purpose:        Define histograms for subevent ssca
// Arguments:
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Instantiates histograms for subevent ssca.
//                 Each histogram will be booked only once.
// Keywords:
//////////////////////////////////////////////////////////////////////////////


	return(kTRUE);
}

Bool_t TUsrSevtSsca::FillHistograms() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtSsca::FillHistograms
// Purpose:        Accumulate histograms for subevent ssca
// Arguments:
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Performs add-one to histograms.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

// add-one to histograms

	gMrbAnalyze->SetUpdateFlag(); 		// activate update handler
	return(kTRUE);
}

Bool_t TUsrSevtSsca::AddBranch(TTree * Tree) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtSsca::AddBranch
// Purpose:        Create a branch for subevent ssca
// Arguments:      TTree * Tree      -- pointer to parent's tree struct
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Calls method TTree::Branch for this subevent.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Int_t bufSize;
	Int_t splitMode;

	if (gMrbAnalyze->TreeToBeWritten()) {
		bufSize = gMrbAnalyze->GetBranchSize();
		splitMode = gMrbAnalyze->GetSplitLevel();
		if (verboseMode) {
			cout	<< this->ClassName() << "::AddBranch(): adding branch \"ssca\" to tree \""
					<< Tree->GetName() << "\" (bufsiz="
					<< bufSize
					<< ", split=" << splitMode << ")"
					<< endl;
		}
		fMyAddr = (TObject *) this; 		// we need a '**' ref to this object
		Tree->Branch( 	"ssca", this->ClassName(),
						&fMyAddr,
						bufSize,
						splitMode);
	}
	return(kTRUE);
}

Bool_t TUsrSevtSsca::InitializeBranch(TTree * Tree) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtSsca::InitializeBranch
// Purpose:        Initialize branch addresses for subevent ssca
// Arguments:      TTree * Tree      -- pointer to parent's tree struct
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Set branch address for subevent ssca.
//                 Subevent will be fetched from tree AS A WHOLE.
//                 >> Replay mode only <<
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	fBranch = Tree->GetBranch("ssca");
	if (fBranch) {
		fMyAddr = this;
		fBranch->SetAddress(&fMyAddr);
	} else {
		cout	<< setred << this->ClassName() << "::InitializeBranch(): branch \"ssca\" not found in tree \""
				<< Tree->GetName() << "\"" << setblack << endl;
	}

	return(kTRUE);
}

void TUsrSevtSsca::Reset(Int_t InitValue, Bool_t DataOnly) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtSsca::Reset
// Purpose:        Reset subevent data
// Arguments:      Int_t InitValue   -- initialization value
//                 Bool_t DataOnly  -- reset data only if kTRUE (ignored)
// Results:        --
// Exceptions:
// Description:    Initializes subevent data with given value.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	for (Int_t i = 0; i < 32; i++) scdata[i] = InitValue;	// ssca.scdata
}


Bool_t TUsrEvtDevt::BookParams() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtDevt::BookParams
// Purpose:        Setup param lists for event devt
// Arguments:      --
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Fills param tables for all subevents in event devt.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Bool_t err = kFALSE;

	err |= !det.BookParams();
	err |= !ssca.BookParams();


	return(!err);
}

Bool_t TUsrEvtDevt::BookHistograms() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtDevt::BookHistograms
// Purpose:        Book histograms for event devt
// Arguments:      --
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Books histograms for all subevents in event devt.
//                 If this is a multi-trigger event derived from single-bit
//                 triggers booking is already done there.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Bool_t err = kFALSE;

	err |= !det.BookHistograms();
	err |= !ssca.BookHistograms();


	return(!err);
}

Int_t TUsrEvtDevt::SetupLofSubevents() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtDevt::SetupLofSubevents
// Purpose:        Setup list of subevents for event devt
// Arguments:      --
// Results:        Int_t NofSubevents    -- number of subevents
// Exceptions:
// Description:    Inserts subevents in a list which later on may be
//                 accessed by subevent index
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	fLofSubevents.Clear();
	this->AddSubevent(&det, (Int_t) kMrbSevtDet);	// subevent det: det subevent, id=1
	this->AddSubevent(&ssca, (Int_t) kMrbSevtSsca);	// subevent ssca: scaler subevent, id=2

	return(fLofSubevents.GetSize());
}

Bool_t TUsrEvtDevt::AllocHitBuffers() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtDevt::AllocHitBuffers
// Purpose:        Allocate hit buffer for event devt
// Arguments:      --
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Allocates hit buffers for subevents to store subevent data
//                 (class TUsrHitBuffer / TClonesArray)
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	TUsrHitBuffer * hb;
	TUsrHBX * hbx;
	Int_t nofEntriesS, highWaterS, coincWindowS;

// get dimension of hit buffer
 	Int_t nofEntriesE = gEnv->GetValue(gMrbAnalyze->GetResource("Event.Devt.HitBuffer.NofEntries"), 1000);
 	Int_t highWaterE = gEnv->GetValue(gMrbAnalyze->GetResource("Event.Devt.HitBuffer.HighWater"), 0);
 	Int_t coincWindowE = gEnv->GetValue(gMrbAnalyze->GetResource("Event.Devt.HitBuffer.CoincWindow"), 0);

// alloc hit buffer for subevent det
 	nofEntriesS = gEnv->GetValue(gMrbAnalyze->GetResource("Subevent.Det.HitBuffer.NofEntries"), nofEntriesE);
 	highWaterS = gEnv->GetValue(gMrbAnalyze->GetResource("Subevent.Det.HitBuffer.HighWater"), highWaterE);
 	coincWindowS = gEnv->GetValue(gMrbAnalyze->GetResource("Subevent.Det.HitBuffer.CoincWindow"), coincWindowE);
 	hb = det.AllocHitBuffer("HBDet", nofEntriesS, highWaterS);
 	hbx = new TUsrHBX(this, hb, coincWindowS);
		hbx->SetParentSevt(&det);
 	this->AddHBX(hbx, (Int_t) kMrbSevtDet);


	return(kTRUE);
}

Bool_t TUsrEvtDevt::CreateTree() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtDevt::CreateTree
// Purpose:        Create a tree for event devt
// Arguments:      --
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Creates a tree for event devt.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Int_t autoSave;

	if (gMrbAnalyze->TreeToBeWritten()) {
		if (verboseMode) {
			cout	<< this->ClassName() << "::CreateTree(): Creating tree \"devt\""
					<< endl;
		}
		fTreeOut = new TTree("devt",
								"det event");
		autoSave = gEnv->GetValue(gMrbAnalyze->GetResource("Event.Devt.AutoSave"),
								10000000);

		fTreeOut->SetAutoSave(autoSave);

		det.AddBranch(fTreeOut);					// branch for subevent det
		ssca.AddBranch(fTreeOut);					// branch for subevent ssca
	}
	return(kTRUE);
}

Bool_t TUsrEvtDevt::InitializeTree(TFile * RootFile) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtDevt::InitializeTree
// Purpose:        Initialize tree for event devt
// Arguments:      TFile * RootFile  -- file containing tree data
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Initializes tree and branches for event devt.
//                 Replay mode only.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	TObject * obj;

	obj = RootFile->Get("devt");
	if (obj == NULL) {
		gMrbLog->Err()	<< "Tree \"devt\" not found in ROOT file " << RootFile->GetName() << endl;
		gMrbLog->Flush(this->ClassName(), "InitializeTree");
		return(kFALSE);
	}
	if (obj->IsA() != TTree::Class()) {
		gMrbLog->Err()	<< "Not a TTree object - " << obj->GetName() << endl;
		gMrbLog->Flush(this->ClassName(), "InitializeTree");
		return(kFALSE);
	}
	fTreeIn = (TTree *) obj;
	fReplayMode = kTRUE;

// initialize branches and assign addresses
	det.InitializeBranch(fTreeIn);
	ssca.InitializeBranch(fTreeIn);

	return(kTRUE);
}

Bool_t TUsrEvtDevt::ReplayTreeData(TMrbIOSpec * IOSpec) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtDevt::ReplayTreeData
// Purpose:        Replay tree data for event devt
// Arguments:      TMrbIOSpec * IOSpec  -- i/o specifications
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Reads in tree data for event devt.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Int_t nofEntries, nofEvents, nofBytes, nbytes;
	Int_t startEvent, stopEvent;
	TString startTime, stopTime;
	TMrbAnalyze::EMrbRunStatus rsts;
	Int_t timeStamp;

	if (!this->IsFakeMode() && fTreeIn == NULL) return(kFALSE);

	nofBytes = 0;
	nofEvents = 0;

	if (fReplayMode) {
		if (!this->IsFakeMode()) {
			nofEntries = (Int_t) fTreeIn->GetEntries();
			if (nofEntries == 0) {
				gMrbLog->Err()	<< "No events found in tree - " << fTreeIn->GetName() << endl;
				gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				return(kFALSE);
			}
		} else {
			nofEntries = 1000000000;
		}

		startEvent = IOSpec->GetStartEvent();
		stopEvent = IOSpec->GetStopEvent();

		if (IOSpec->IsTimeStampMode()) {
			Int_t tsOffset = gEnv->GetValue("TUsrEvtDevt.TimeOffset", 0);

			IOSpec->ConvertToTimeStamp(startTime, startEvent);
			IOSpec->ConvertToTimeStamp(stopTime, stopEvent);

			if (startEvent > stopEvent) {
				gMrbLog->Err()	<< "Timestamps out of order - start at "
								<< startTime << " ... stop at " << stopTime << endl;
				gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				return(kFALSE);
			}

			if (verboseMode) {
				if (this->IsFakeMode()) {
					gMrbLog->Out()	<< "Replaying tree in FAKE mode" << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				} else {
					gMrbLog->Out()	<< "Replaying tree " << fTreeIn->GetName() << " -" << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
					gMrbLog->Out()	<< "Events with time stamps " << startTime << " ... " << stopTime << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				}
			}

			Bool_t go = kTRUE;
			for (Int_t evt = 0; evt < nofEntries; evt++) {
				if (!gMrbAnalyze->TestRunStatus()) return(kFALSE);
				pthread_mutex_lock(&global_data_mutex);
				if (!this->IsFakeMode()) nbytes = fTreeIn->GetEvent(evt);
				timeStamp = det.fTimeStamp - tsOffset;
				if (timeStamp >= startEvent && timeStamp <= stopEvent) {
					nofEvents++;
					fNofEvents++;
					nofBytes += nbytes;
					gMrbAnalyze->IncrEventCount();
					go = this->Analyze(evt);
				}
				pthread_mutex_unlock(&global_data_mutex);
				if (!go || (timeStamp > stopEvent)) break;
			}

			if (verboseMode) {
				if (this->IsFakeMode()) {
					gMrbLog->Out()	<< nofEvents
									<< " entries (timestamps " << startTime << " ... " << stopTime
									<< ") faked" << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData", setblue);
				} else {
					gMrbLog->Out()	<< nofEvents
									<< " entries (timestamps " << startTime << " ... " << stopTime
									<< ") read from tree \"" << fTreeIn->GetName() << "\" (" << nofBytes << " bytes)"
									<< endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData", setblue);
				}
			}

		} else {

			if (startEvent > stopEvent) {
				gMrbLog->Err()	<< "Event numbers out of order - start at "
								<< startEvent << " ... stop at " << stopEvent << endl;
				gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				return(kFALSE);
			}

			if (startEvent > nofEntries) {
				gMrbLog->Err()	<< "StartEvent (" << startEvent
								<< ") beyond end of data (" << stopEvent << ")" << endl;
				gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				return(kFALSE);
			}

			if (stopEvent == 0 || stopEvent >= nofEntries) stopEvent = nofEntries - 1;

			if (verboseMode) {
				if (this->IsFakeMode()) {
					gMrbLog->Out()	<< "Replaying tree in FAKE mode" << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				} else {
					gMrbLog->Out()	<< "Replaying tree " << fTreeIn->GetName() << " -" << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData", setblue);
					gMrbLog->Out()	<< "Events " << startEvent << " ... " << stopEvent << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData", setblue);
				}
			}

			Bool_t go = kTRUE;
			for (Int_t evt = startEvent; evt <= stopEvent; evt++) {
				if (!gMrbAnalyze->TestRunStatus()) return(kFALSE);
				pthread_mutex_lock(&global_data_mutex);
				if (!this->IsFakeMode()) nofBytes += fTreeIn->GetEvent(evt);
				nofEvents++;
				fNofEvents++;
				gMrbAnalyze->IncrEventCount();
				go = this->Analyze(evt);
				pthread_mutex_unlock(&global_data_mutex);
				if (!go) break;
			}

			if (verboseMode) {
				if (this->IsFakeMode()) {
					gMrbLog->Out()	<< nofEvents << " entries (" << startEvent << " ... " << stopEvent
									<< ") faked" << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData", setblue);
				} else {
					gMrbLog->Out()	<< nofEvents << " entries (" << startEvent << " ... " << stopEvent
									<< ") read from tree \"" << fTreeIn->GetName() << "\" (" << nofBytes << " bytes)"
									<< endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData", setblue);
				}
			}
		}
	}
	return(kTRUE);
}

Bool_t TUsrEvtDevt::FillEvent(const s_vehe * EventData, MBSDataIO * BaseAddr) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtDevt::FillEvent
// Purpose:        Fill local event instance with MBS data
// Arguments:      const s_vehe * EventData   -- pointer to MBS event structure
//                 MBSDataIO * BaseAddr -- pointer to MBS data base
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Decodes the event header,
//                 then loops over subevents
//                 and calls sevt-specific fill methods
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	UInt_t SevtType;
	const UShort_t * SevtData;
	const UInt_t * ClockData;
	const UInt_t * DeadTimeData;
	Int_t timeStamp;
	Int_t dtTotalEvents, dtAccuEvents, dtScalerContents;

	fEventNumber = (UInt_t) EventData->l_count;
	fNofEvents++;
	gMrbAnalyze->IncrEventCount();

	fClockRes = 0;									// reset time stamp
	fClockSecs = 0;
	fClockNsecs = 0;

	fSubeventPattern = 0;							// reset sevt pattern

	this->Reset();									// clear data section

	for (;;) {
		SevtType = NextSubeventHeader(BaseAddr);			// get next subevent
		if (SevtType == MBS_STYPE_EOE) return(kTRUE);		// end of event
		if (SevtType == MBS_STYPE_ABORT) return(kFALSE);	// error


		switch (BaseAddr->sevt_id) {				// dispatch according to subevent id
			case kMrbSevtDet:				// subevent det: det subevent, id=1
				if ((SevtData = NextSubeventData(BaseAddr, 11, kFALSE)) == NULL) return(kFALSE);
				if (!det.FillSubevent(SevtData, BaseAddr->sevt_wc, this->GetTimeStamp())) return(kFALSE);
				fSubeventPattern |= kMrbSevtBitDet;
				continue;
			case kMrbSevtSsca:				// subevent ssca: scaler subevent, id=2
				if ((SevtData = NextSubeventData(BaseAddr, 32, kFALSE)) == NULL) return(kFALSE);
				if (!ssca.FillSubevent(SevtData, BaseAddr->sevt_wc, this->GetTimeStamp())) return(kFALSE);
				fSubeventPattern |= kMrbSevtBitSsca;
				continue;

			case kMrbSevtTimeStamp:					// subevent type [9000,1] (ROOT Id=999): time stamp
				if ((SevtData = NextSubeventData(BaseAddr, 100, kFALSE)) == NULL) return(kFALSE);
				ClockData = (UInt_t *) SevtData;
				fClockRes = *ClockData++;
				fClockSecs = *ClockData++;
				fClockNsecs = *ClockData++;
				timeStamp = this->CalcTimeDiff((TUsrEvent *) gStartEvent);	// calc time since last start
				this->SetTimeStamp(timeStamp);		// mark event with time stamp
				continue;

			case kMrbSevtDeadTime:					// subevent type [9000,2] (ROOT Id=998): dead time data
				if ((SevtData = NextSubeventData(BaseAddr, 100, kFALSE)) == NULL) return(kFALSE);
				DeadTimeData = (UInt_t *) SevtData;
				fClockRes = *DeadTimeData++;
				fClockSecs = *DeadTimeData++;
				fClockNsecs = *DeadTimeData++;
				timeStamp = this->CalcTimeDiff((TUsrEvent *) gStartEvent);	// calc time since last start
				this->SetTimeStamp(timeStamp);		// mark event with time stamp
				dtTotalEvents = *DeadTimeData++;
				dtAccuEvents = *DeadTimeData++;
				dtScalerContents = *DeadTimeData++;
				gDeadTime->Set(timeStamp, dtTotalEvents, dtAccuEvents, dtScalerContents);
				if (gMrbAnalyze->TreeToBeWritten()) gDeadTime->GetTreeOut()->Fill();
				continue;

			case kMrbSevtDummy: 					// dummy subevent created by MBS internally [111,111] (ROOT Id=888), wc = 0
				if ((SevtData = NextSubeventData(BaseAddr, 100, kFALSE)) == NULL) return(kFALSE);
				if (BaseAddr->sevt_wc != 0) {
					gMrbLog->Err()	<< "Dummy MBS subevent [111,111], serial=888 - wc = "
									<< BaseAddr->sevt_wc << " (should be 0)" << endl;
					gMrbLog->Flush(this->ClassName(), "FillEvent");
				}
				continue;

			default: 					// unknown subevent id
				gMrbLog->Err()	<< "Unknown subevent id - " << BaseAddr->sevt_id << endl;
				gMrbLog->Flush(this->ClassName(), "FillEvent");
				continue;
		}
		gMrbLog->Err()	<< "Illegal subevent - type=0x"
						<< setbase(16) << BaseAddr->sevt_otype
						<< setbase(10) << " ["
						<< (BaseAddr->sevt_otype >> 16) << "," << (BaseAddr->sevt_otype & 0xffff)
						<< "], serial=" << BaseAddr->sevt_id << endl;
		gMrbLog->Flush(this->ClassName(), "FillEvent");
	}
	return(kTRUE);
}


Bool_t TUsrEvtDevt::FillHistograms() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:		   TUsrEvtDevt::FillHistograms
// Purpose: 	   Fill histograms for event devt
//  			   (Trigger 1)
// Arguments:	   --
// Results: 	   kTRUE/kFALSE
// Exceptions:
// Description:    Fills histograms for event devt,
//                 one for each parameter.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	det.FillHistograms();	// fill histograms for subevent det
	ssca.FillHistograms();	// fill histograms for subevent ssca
	this->FillRateHistograms(); // fill rate histos for event devt

	return(kTRUE);
}

Bool_t TUsrEvtDevt::FillRateHistograms() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:		   TUsrEvtDevt::FillRateHistograms
// Purpose: 	   Fill rate histograms for event devt
//  			   (Trigger 1)
// Arguments:	   --
// Results: 	   kTRUE/kFALSE
// Exceptions:
// Description:    Fills rate histograms for event devt
// Keywords:
//////////////////////////////////////////////////////////////////////////////


	gMrbAnalyze->SetUpdateFlag();
	return(kTRUE);
}

void TUsrEvtDevt::Reset(Int_t InitValue, Bool_t DataOnly) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtDevt::Reset
// Purpose:        Reset event data
// Arguments:      Int_t InitValue  -- initialization value
//                 Bool_t DataOnly  -- reset data only if kTRUE
// Results:        --
// Exceptions:
// Description:    Resets all subevents assigned to this event.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	det.Reset(InitValue, DataOnly);	// reset param data in subevent det
	ssca.Reset(InitValue, DataOnly);	// reset param data in subevent ssca
 	gMrbAnalyze->ResetModuleHits();
}

Int_t TUsrEvtDevt::UnpackNextEvent(Int_t EntryNo) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtDevt::UnpackNextEvent
// Purpose:        Get next mbs event from tree
// Arguments:      TTree * Tree       -- input tree
//                 Int_t EntryNo      -- current entry in tree
// Results:        Int_t EntryNo      -- next entry
// Exceptions:
// Description:    Reads next mbs event from tree
//                 (to be used in offline analysis only)
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	if (EntryNo < fTreeIn->GetEntriesFast()) {		// are there more events?
		fTreeIn->GetEvent(EntryNo);
		EntryNo++;
		return EntryNo;
	}
	return -1;
}

Bool_t TUsrEvtXstop::BookParams() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtXstop::BookParams
// Purpose:        Setup param lists for event xstop
// Arguments:      --
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Fills param tables for all subevents in event xstop.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Bool_t err = kFALSE;

	err |= !ssca.BookParams();


	return(!err);
}

Bool_t TUsrEvtXstop::BookHistograms() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtXstop::BookHistograms
// Purpose:        Book histograms for event xstop
// Arguments:      --
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Books histograms for all subevents in event xstop.
//                 If this is a multi-trigger event derived from single-bit
//                 triggers booking is already done there.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Bool_t err = kFALSE;

	err |= !ssca.BookHistograms();


	return(!err);
}

Int_t TUsrEvtXstop::SetupLofSubevents() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtXstop::SetupLofSubevents
// Purpose:        Setup list of subevents for event xstop
// Arguments:      --
// Results:        Int_t NofSubevents    -- number of subevents
// Exceptions:
// Description:    Inserts subevents in a list which later on may be
//                 accessed by subevent index
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	fLofSubevents.Clear();
	this->AddSubevent(&ssca, (Int_t) kMrbSevtSsca);	// subevent ssca: scaler subevent, id=2

	return(fLofSubevents.GetSize());
}

Bool_t TUsrEvtXstop::AllocHitBuffers() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtXstop::AllocHitBuffers
// Purpose:        Allocate hit buffer for event xstop
// Arguments:      --
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Allocates hit buffers for subevents to store subevent data
//                 (class TUsrHitBuffer / TClonesArray)
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	TUsrHitBuffer * hb;
	TUsrHBX * hbx;
	Int_t nofEntriesS, highWaterS, coincWindowS;

// get dimension of hit buffer
 	Int_t nofEntriesE = gEnv->GetValue(gMrbAnalyze->GetResource("Event.Xstop.HitBuffer.NofEntries"), 1000);
 	Int_t highWaterE = gEnv->GetValue(gMrbAnalyze->GetResource("Event.Xstop.HitBuffer.HighWater"), 0);
 	Int_t coincWindowE = gEnv->GetValue(gMrbAnalyze->GetResource("Event.Xstop.HitBuffer.CoincWindow"), 0);


	return(kTRUE);
}

Bool_t TUsrEvtXstop::CreateTree() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtXstop::CreateTree
// Purpose:        Create a tree for event xstop
// Arguments:      --
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Creates a tree for event xstop.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Int_t autoSave;

	if (gMrbAnalyze->TreeToBeWritten()) {
		if (verboseMode) {
			cout	<< this->ClassName() << "::CreateTree(): Creating tree \"xstop\""
					<< endl;
		}
		fTreeOut = new TTree("xstop",
								"stop acquisition, trigger 15, user-defined");
		autoSave = gEnv->GetValue(gMrbAnalyze->GetResource("Event.Xstop.AutoSave"),
								10000000);

		fTreeOut->SetAutoSave(autoSave);

		fTreeOut->Branch("stop", &fClockSecs, "stop/i");	// timestamp of stop event
		ssca.AddBranch(fTreeOut);					// branch for subevent ssca
	}
	return(kTRUE);
}

Bool_t TUsrEvtXstop::InitializeTree(TFile * RootFile) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtXstop::InitializeTree
// Purpose:        Initialize tree for event xstop
// Arguments:      TFile * RootFile  -- file containing tree data
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Initializes tree and branches for event xstop.
//                 Replay mode only.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	TObject * obj;

	obj = RootFile->Get("xstop");
	if (obj == NULL) {
		gMrbLog->Err()	<< "Tree \"xstop\" not found in ROOT file " << RootFile->GetName() << endl;
		gMrbLog->Flush(this->ClassName(), "InitializeTree");
		return(kFALSE);
	}
	if (obj->IsA() != TTree::Class()) {
		gMrbLog->Err()	<< "Not a TTree object - " << obj->GetName() << endl;
		gMrbLog->Flush(this->ClassName(), "InitializeTree");
		return(kFALSE);
	}
	fTreeIn = (TTree *) obj;
	fReplayMode = kTRUE;

// initialize branches and assign addresses
	fTreeIn->SetBranchAddress("stop", &fClockSecs);		// timestamp for stop event
	ssca.InitializeBranch(fTreeIn);

	return(kTRUE);
}

Bool_t TUsrEvtXstop::ReplayTreeData(TMrbIOSpec * IOSpec) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtXstop::ReplayTreeData
// Purpose:        Replay tree data for event xstop
// Arguments:      TMrbIOSpec * IOSpec  -- i/o specifications
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Reads in tree data for event xstop.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	Int_t nofEntries, nofEvents, nofBytes, nbytes;
	Int_t startEvent, stopEvent;
	TString startTime, stopTime;
	TMrbAnalyze::EMrbRunStatus rsts;
	Int_t timeStamp;

	if (!this->IsFakeMode() && fTreeIn == NULL) return(kFALSE);

	nofBytes = 0;
	nofEvents = 0;

	if (fReplayMode) {
		if (!this->IsFakeMode()) {
			nofEntries = (Int_t) fTreeIn->GetEntries();
			if (nofEntries == 0) {
				gMrbLog->Err()	<< "No events found in tree - " << fTreeIn->GetName() << endl;
				gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				return(kFALSE);
			}
		} else {
			nofEntries = 1000000000;
		}

		startEvent = IOSpec->GetStartEvent();
		stopEvent = IOSpec->GetStopEvent();

		if (IOSpec->IsTimeStampMode()) {
			Int_t tsOffset = gEnv->GetValue("TUsrEvtXstop.TimeOffset", 0);

			IOSpec->ConvertToTimeStamp(startTime, startEvent);
			IOSpec->ConvertToTimeStamp(stopTime, stopEvent);

			if (startEvent > stopEvent) {
				gMrbLog->Err()	<< "Timestamps out of order - start at "
								<< startTime << " ... stop at " << stopTime << endl;
				gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				return(kFALSE);
			}

			if (verboseMode) {
				if (this->IsFakeMode()) {
					gMrbLog->Out()	<< "Replaying tree in FAKE mode" << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				} else {
					gMrbLog->Out()	<< "Replaying tree " << fTreeIn->GetName() << " -" << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
					gMrbLog->Out()	<< "Events with time stamps " << startTime << " ... " << stopTime << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				}
			}

			Bool_t go = kTRUE;
			for (Int_t evt = 0; evt < nofEntries; evt++) {
				if (!gMrbAnalyze->TestRunStatus()) return(kFALSE);
				pthread_mutex_lock(&global_data_mutex);
				if (!this->IsFakeMode()) nbytes = fTreeIn->GetEvent(evt);
				timeStamp = ssca.fTimeStamp - tsOffset;
				if (timeStamp >= startEvent && timeStamp <= stopEvent) {
					nofEvents++;
					fNofEvents++;
					nofBytes += nbytes;
					gMrbAnalyze->IncrEventCount();
					go = this->Analyze(evt);
				}
				pthread_mutex_unlock(&global_data_mutex);
				if (!go || (timeStamp > stopEvent)) break;
			}

			if (verboseMode) {
				if (this->IsFakeMode()) {
					gMrbLog->Out()	<< nofEvents
									<< " entries (timestamps " << startTime << " ... " << stopTime
									<< ") faked" << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData", setblue);
				} else {
					gMrbLog->Out()	<< nofEvents
									<< " entries (timestamps " << startTime << " ... " << stopTime
									<< ") read from tree \"" << fTreeIn->GetName() << "\" (" << nofBytes << " bytes)"
									<< endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData", setblue);
				}
			}

		} else {

			if (startEvent > stopEvent) {
				gMrbLog->Err()	<< "Event numbers out of order - start at "
								<< startEvent << " ... stop at " << stopEvent << endl;
				gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				return(kFALSE);
			}

			if (startEvent > nofEntries) {
				gMrbLog->Err()	<< "StartEvent (" << startEvent
								<< ") beyond end of data (" << stopEvent << ")" << endl;
				gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				return(kFALSE);
			}

			if (stopEvent == 0 || stopEvent >= nofEntries) stopEvent = nofEntries - 1;

			if (verboseMode) {
				if (this->IsFakeMode()) {
					gMrbLog->Out()	<< "Replaying tree in FAKE mode" << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData");
				} else {
					gMrbLog->Out()	<< "Replaying tree " << fTreeIn->GetName() << " -" << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData", setblue);
					gMrbLog->Out()	<< "Events " << startEvent << " ... " << stopEvent << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData", setblue);
				}
			}

			Bool_t go = kTRUE;
			for (Int_t evt = startEvent; evt <= stopEvent; evt++) {
				if (!gMrbAnalyze->TestRunStatus()) return(kFALSE);
				pthread_mutex_lock(&global_data_mutex);
				if (!this->IsFakeMode()) nofBytes += fTreeIn->GetEvent(evt);
				nofEvents++;
				fNofEvents++;
				gMrbAnalyze->IncrEventCount();
				go = this->Analyze(evt);
				pthread_mutex_unlock(&global_data_mutex);
				if (!go) break;
			}

			if (verboseMode) {
				if (this->IsFakeMode()) {
					gMrbLog->Out()	<< nofEvents << " entries (" << startEvent << " ... " << stopEvent
									<< ") faked" << endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData", setblue);
				} else {
					gMrbLog->Out()	<< nofEvents << " entries (" << startEvent << " ... " << stopEvent
									<< ") read from tree \"" << fTreeIn->GetName() << "\" (" << nofBytes << " bytes)"
									<< endl;
					gMrbLog->Flush(this->ClassName(), "ReplayTreeData", setblue);
				}
			}
		}
	}
	return(kTRUE);
}

Bool_t TUsrEvtXstop::FillEvent(const s_vehe * EventData, MBSDataIO * BaseAddr) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtXstop::FillEvent
// Purpose:        Fill local event instance with MBS data
// Arguments:      const s_vehe * EventData   -- pointer to MBS event structure
//                 MBSDataIO * BaseAddr -- pointer to MBS data base
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Decodes the event header,
//                 then loops over subevents
//                 and calls sevt-specific fill methods
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	UInt_t SevtType;
	const UShort_t * SevtData;
	const UInt_t * ClockData;
	const UInt_t * DeadTimeData;
	Int_t timeStamp;
	Int_t dtTotalEvents, dtAccuEvents, dtScalerContents;

	fEventNumber = (UInt_t) EventData->l_count;
	fNofEvents++;
	gMrbAnalyze->IncrEventCount();

	fClockRes = 0;									// reset time stamp
	fClockSecs = 0;
	fClockNsecs = 0;

	fSubeventPattern = 0;							// reset sevt pattern

	this->Reset();									// clear data section

	for (;;) {
		SevtType = NextSubeventHeader(BaseAddr);			// get next subevent
		if (SevtType == MBS_STYPE_EOE) return(kTRUE);		// end of event
		if (SevtType == MBS_STYPE_ABORT) return(kFALSE);	// error


		switch (BaseAddr->sevt_id) {				// dispatch according to subevent id
			case kMrbSevtSsca:				// subevent ssca: scaler subevent, id=2
				if ((SevtData = NextSubeventData(BaseAddr, 32, kFALSE)) == NULL) return(kFALSE);
				if (!ssca.FillSubevent(SevtData, BaseAddr->sevt_wc, this->GetTimeStamp())) return(kFALSE);
				fSubeventPattern |= kMrbSevtBitSsca;
				continue;

			case kMrbSevtTimeStamp:					// subevent type [9000,1] (ROOT Id=999): time stamp
				if ((SevtData = NextSubeventData(BaseAddr, 100, kFALSE)) == NULL) return(kFALSE);
				ClockData = (UInt_t *) SevtData;
				fClockRes = *ClockData++;
				fClockSecs = *ClockData++;
				fClockNsecs = *ClockData++;
				timeStamp = this->CalcTimeDiff((TUsrEvent *) gStartEvent);	// calc time since last start
				this->SetTimeStamp(timeStamp);		// mark event with time stamp
				continue;

			case kMrbSevtDeadTime:					// subevent type [9000,2] (ROOT Id=998): dead time data
				if ((SevtData = NextSubeventData(BaseAddr, 100, kFALSE)) == NULL) return(kFALSE);
				DeadTimeData = (UInt_t *) SevtData;
				fClockRes = *DeadTimeData++;
				fClockSecs = *DeadTimeData++;
				fClockNsecs = *DeadTimeData++;
				timeStamp = this->CalcTimeDiff((TUsrEvent *) gStartEvent);	// calc time since last start
				this->SetTimeStamp(timeStamp);		// mark event with time stamp
				dtTotalEvents = *DeadTimeData++;
				dtAccuEvents = *DeadTimeData++;
				dtScalerContents = *DeadTimeData++;
				gDeadTime->Set(timeStamp, dtTotalEvents, dtAccuEvents, dtScalerContents);
				if (gMrbAnalyze->TreeToBeWritten()) gDeadTime->GetTreeOut()->Fill();
				continue;

			case kMrbSevtDummy: 					// dummy subevent created by MBS internally [111,111] (ROOT Id=888), wc = 0
				if ((SevtData = NextSubeventData(BaseAddr, 100, kFALSE)) == NULL) return(kFALSE);
				if (BaseAddr->sevt_wc != 0) {
					gMrbLog->Err()	<< "Dummy MBS subevent [111,111], serial=888 - wc = "
									<< BaseAddr->sevt_wc << " (should be 0)" << endl;
					gMrbLog->Flush(this->ClassName(), "FillEvent");
				}
				continue;

			default: 					// unknown subevent id
				gMrbLog->Err()	<< "Unknown subevent id - " << BaseAddr->sevt_id << endl;
				gMrbLog->Flush(this->ClassName(), "FillEvent");
				continue;
		}
		gMrbLog->Err()	<< "Illegal subevent - type=0x"
						<< setbase(16) << BaseAddr->sevt_otype
						<< setbase(10) << " ["
						<< (BaseAddr->sevt_otype >> 16) << "," << (BaseAddr->sevt_otype & 0xffff)
						<< "], serial=" << BaseAddr->sevt_id << endl;
		gMrbLog->Flush(this->ClassName(), "FillEvent");
	}
	return(kTRUE);
}


Bool_t TUsrEvtXstop::FillHistograms() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:		   TUsrEvtXstop::FillHistograms
// Purpose: 	   Fill histograms for event xstop
//  			   (Trigger 15)
// Arguments:	   --
// Results: 	   kTRUE/kFALSE
// Exceptions:
// Description:    Fills histograms for event xstop,
//                 one for each parameter.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	ssca.FillHistograms();	// fill histograms for subevent ssca
	this->FillRateHistograms(); // fill rate histos for event xstop

	return(kTRUE);
}

Bool_t TUsrEvtXstop::FillRateHistograms() {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:		   TUsrEvtXstop::FillRateHistograms
// Purpose: 	   Fill rate histograms for event xstop
//  			   (Trigger 15)
// Arguments:	   --
// Results: 	   kTRUE/kFALSE
// Exceptions:
// Description:    Fills rate histograms for event xstop
// Keywords:
//////////////////////////////////////////////////////////////////////////////


	gMrbAnalyze->SetUpdateFlag();
	return(kTRUE);
}

void TUsrEvtXstop::Reset(Int_t InitValue, Bool_t DataOnly) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrSevtXstop::Reset
// Purpose:        Reset event data
// Arguments:      Int_t InitValue  -- initialization value
//                 Bool_t DataOnly  -- reset data only if kTRUE
// Results:        --
// Exceptions:
// Description:    Resets all subevents assigned to this event.
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	ssca.Reset(InitValue, DataOnly);	// reset param data in subevent ssca
 	gMrbAnalyze->ResetModuleHits();
}

Int_t TUsrEvtXstop::UnpackNextEvent(Int_t EntryNo) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TUsrEvtXstop::UnpackNextEvent
// Purpose:        Get next mbs event from tree
// Arguments:      TTree * Tree       -- input tree
//                 Int_t EntryNo      -- current entry in tree
// Results:        Int_t EntryNo      -- next entry
// Exceptions:
// Description:    Reads next mbs event from tree
//                 (to be used in offline analysis only)
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	if (EntryNo < fTreeIn->GetEntriesFast()) {		// are there more events?
		fTreeIn->GetEvent(EntryNo);
		EntryNo++;
		return EntryNo;
	}
	return -1;
}



Bool_t TMrbTransport::ProcessEvent(s_vehe * EventData) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           TMrbTransport::ProcessEvent
// Purpose:        Process event data
// Arguments:      s_vehe * EventData  -- pointer to event data (including header)
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    This is the event filling function.
//                 User has to define how to process this event.
//                 Default is via method Analyze().
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	MBSDataIO * BaseAddr; 		// base addr of MBS data
	Bool_t sts;

	if (!gMrbAnalyze->TestRunStatus()) return(kFALSE);		// run is stopped (maybe after pausing)

	gMrbAnalyze->ResetEventsPerTrigger();		// clear event count for all modules

	BaseAddr = GetMbsBase();			// fetch base addr

	switch (EventData->i_trigger) {		// dispatch according to trigger number
		case kMrbTriggerDevt:		// event devt, trigger 1
			pthread_mutex_lock(&global_data_mutex);
			gDevt->FillEvent(EventData, BaseAddr);
			sts = gDevt->Analyze(0);
			pthread_mutex_unlock(&global_data_mutex);
			return(kTRUE);


		case kMrbTriggerStartAcquisition:					// start acquisition (trigger 14)
			gStartEvent->ExtractTimeStamp(EventData, BaseAddr);
			gStartEvent->Print("START Acquisition");
			gMrbAnalyze->SetRunStatus(TMrbAnalyze::M_RUNNING);
			return(kTRUE);

		case kMrbTriggerStopAcquisition:					// stop acquisition (trigger 15)
			gStopEvent->FillEvent(EventData, BaseAddr);
			gStopEvent->Print("STOP Acquisition");
			sts = gStopEvent->Analyze(0);
			gMrbAnalyze->SetRunStatus(TMrbAnalyze::M_STOPPING);
			return(kFALSE);

		default:
			gMrbLog->Err()	<< "Illegal trigger number - #" << EventData->i_trigger << endl;
			gMrbLog->Flush(this->ClassName(), "ProcessEvent");
			return(kTRUE);				// kTRUE -> ignore it, continue data taking!
	}
	return(kTRUE);
}


Bool_t UnpackInitialize(const Char_t * RootFile) {
//________________________________________________________________[C++ METHOD]
//////////////////////////////////////////////////////////////////////////////
// Name:           UnpackInitialize
// Purpose:        Initialize unpacker
// Arguments:      Char_t * RootFile     -- name of root file containing tree
// Results:        kTRUE/kFALSE
// Exceptions:
// Description:    Initialize unpacking and open root file
//                 (to be used in offline analysis only)
// Keywords:
//////////////////////////////////////////////////////////////////////////////

	if (gMrbAnalyze == NULL) {
		TMrbIOSpec * io = new TMrbIOSpec();
		gMrbAnalyze = new TMrbAnalyze(io);			// init analysis specific for current setup
	}
	if (!gMrbAnalyze->OpenRootFile(RootFile)) return kFALSE;		// open root file, setup tree
	return kTRUE;
}

