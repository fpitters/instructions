#ifdef __CINT__
//___________________________________________________________[CINT DIRECTIVES]
//////////////////////////////////////////////////////////////////////////////
// @Name:          GamsAnalyzeLinkDef.h
// @Purpose:       Directives telling CINT how to compile GamsAnalyze
//                 To be used with user files GamsAnalyze.cxx/.h
// Description:    Contains a "#pragma link C++ class" statement
//                 for each user-defined class
// Author:         gams
// Revision:         
// Date:           Sun Feb 17 11:13:39 2019
// URL:            
// Keywords:       
//////////////////////////////////////////////////////////////////////////////

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

// classes supplied by system
#pragma link C++ class TMrbIOSpec+;
#pragma link C++ class TMrbAnalyze+;
#pragma link C++ class TUsrEvent+;
#pragma link C++ class TUsrEvtStart+;
#pragma link C++ class TUsrEvtStop+;
// classes defined by user
#pragma link C++ class TUsrEvtDevt+;
#pragma link C++ class TUsrEvtXstop+;
#pragma link C++ class TUsrSevtDet+;
#pragma link C++ class TUsrSevtSsca+;
#pragma link C++ class TMrbSubevent_Caen+;
#endif
