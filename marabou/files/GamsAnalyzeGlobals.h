#include "GlobDef.h"

//___________________________________________________________[C++ HEADER FILE]
//////////////////////////////////////////////////////////////////////////////
// Name:           GamsAnalyzeGlobals.h
// Purpose:        Header file to define global pointers
// Description:    GAMS experiment
//
//                 This file contains most of user-defined pointers
//                 such as pointers to events, histograms, windows etc.
//
// Author:         gams
// Revision:
// Date:           Sun Feb 17 11:13:39 2019
// URL:
// Keywords:
//////////////////////////////////////////////////////////////////////////////

#include "TH1.h"
#include "TH2.h"
#include "TH3.h"

#include "TMrbVar.h"
#include "TMrbWdw.h"
#include "TMrbVarWdwCommon.h"

//_______________________________________________________________[GLOBAL DEFS]
//////////////////////////////////////////////////////////////////////////////
// Purpose:        Define pointers to event classes
// Comment:        Each event class (trigger) may be addressed by g<EventName>
//////////////////////////////////////////////////////////////////////////////

GLOBAL(TUsrEvtDevt *,gDevt,gDevt = NULL);	// event devt, trigger 1

//_______________________________________________________________[GLOBAL DEFS]
//////////////////////////////////////////////////////////////////////////////
// Purpose:        Define global instances of user's variables/windows
// Comment:        TMrbVariableX and TMrbVarArrayX are defined as
//                 GLOBAL objects so they can be addressed DIRECTLY
//                 TMrbWindowX objects are defined by pointers to the heap.
//////////////////////////////////////////////////////////////////////////////

GLOBAL(TMrbVarF,pt,pt("pt", 0.0));
GLOBAL(TMrbVarF,p1,p1("p1", 0.0));
GLOBAL(TMrbVarF,p2,p2("p2", 0.0));
GLOBAL(TMrbVarF,p3,p3("p3", 0.0));
GLOBAL(TMrbVarF,p4,p4("p4", 0.0));
GLOBAL(TMrbVarF,p5,p5("p5", 0.0));
GLOBAL(TMrbVarF,pdt,pdt("pdt", 0.0));
GLOBAL(TMrbVarF,pp2,pp2("pp2", 0.0));
GLOBAL(TMrbVarF,pp,pp("pp", 0.0));
GLOBAL(TMrbVarF,pdp,pdp("pdp", 0.0));
GLOBAL(TMrbVarF,rmst,rmst("rmst", 0.0));
GLOBAL(TMrbVarF,rms1,rms1("rms1", 0.0));
GLOBAL(TMrbVarF,rms2,rms2("rms2", 0.0));
GLOBAL(TMrbVarF,rms3,rms3("rms3", 0.0));
GLOBAL(TMrbVarF,rms4,rms4("rms4", 0.0));
GLOBAL(TMrbVarF,rms5,rms5("rms5", 0.0));
GLOBAL(TMrbVarF,rmsdt,rmsdt("rmsdt", 0.0));
GLOBAL(TMrbVarF,rmsp2,rmsp2("rmsp2", 0.0));
GLOBAL(TMrbVarF,rmsp,rmsp("rmsp", 0.0));
GLOBAL(TMrbVarF,rmsdp,rmsdp("rmsdp", 0.0));


GLOBAL(TMrbWindowI *,wraw,wraw = NULL);
GLOBAL(TMrbWindowI *,wet,wet = NULL);
GLOBAL(TMrbWindowI *,we1,we1 = NULL);
GLOBAL(TMrbWindowI *,we2,we2 = NULL);
GLOBAL(TMrbWindowI *,we3,we3 = NULL);
GLOBAL(TMrbWindowI *,we4,we4 = NULL);
GLOBAL(TMrbWindowI *,we5,we5 = NULL);
GLOBAL(TMrbWindowI *,wdt,wdt = NULL);
GLOBAL(TMrbWindowI *,wdp,wdp = NULL);
GLOBAL(TMrbWindowI *,wp,wp = NULL);
GLOBAL(TMrbWindowI *,wp2,wp2 = NULL);
GLOBAL(TMrbWindowI *,wpup,wpup = NULL);
GLOBAL(TMrbWindowI *,wtof,wtof = NULL);
GLOBAL(TMrbWindowI *,wtime,wtime = NULL);
GLOBAL(TMrbWindowF *,wchi2,wchi2 = NULL);

//_______________________________________________________________[GLOBAL DEFS]
//////////////////////////////////////////////////////////////////////////////
// Purpose:        Define global instances of user's functions
// Comment:        TF1 and TF2 objects are defined by pointers to the heap.
//////////////////////////////////////////////////////////////////////////////


//_______________________________________________________________[GLOBAL DEFS]
//////////////////////////////////////////////////////////////////////////////
// Purpose:        Define pointers to histograms for (sub)events
// Comment:        For each parameter of a subevent there is a histogram
//                 named h<ParamName>
//////////////////////////////////////////////////////////////////////////////

GLOBAL(TH1F *,hEt,hEt = NULL); 	// det.et
GLOBAL(TH1F *,hE1l,hE1l = NULL); 	// det.e1l
GLOBAL(TH1F *,hE1r,hE1r = NULL); 	// det.e1r
GLOBAL(TH1F *,hE2l,hE2l = NULL); 	// det.e2l
GLOBAL(TH1F *,hE2r,hE2r = NULL); 	// det.e2r
GLOBAL(TH1F *,hE3,hE3 = NULL); 	// det.e3
GLOBAL(TH1F *,hE4,hE4 = NULL); 	// det.e4
GLOBAL(TH1F *,hE5,hE5 = NULL); 	// det.e5
GLOBAL(TH1F *,hDt,hDt = NULL); 	// det.dt
GLOBAL(TH1F *,hTof,hTof = NULL); 	// det.tof
GLOBAL(TH1F *,hPup,hPup = NULL); 	// det.pup

//_______________________________________________________________[GLOBAL DEFS]
//////////////////////////////////////////////////////////////////////////////
// Purpose:        Define pointers to histograms booked by user
// Comment:        User may define additional histos here
//////////////////////////////////////////////////////////////////////////////

GLOBAL(TH1F *,hE1,hE1 = NULL);	// E1
GLOBAL(TH1F *,hE2,hE2 = NULL);	// E2
GLOBAL(TH1F *,hP,hP = NULL);	// Ort
GLOBAL(TH1F *,hP2,hP2 = NULL);	// Ort_2
GLOBAL(TH1F *,hDp,hDp = NULL);	// x_Winkel
GLOBAL(TH1F *,hTime,hTime = NULL);	// Time
GLOBAL(TH1F *,hChi2,hChi2 = NULL);	// Chi2
GLOBAL(TH2S *,hEtp,hEtp = NULL);	// Etot_Ort
GLOBAL(TH2S *,hE1p,hE1p = NULL);	// E1_Ort
GLOBAL(TH2S *,hE2p,hE2p = NULL);	// E2_Ort
GLOBAL(TH2S *,hE3p,hE3p = NULL);	// E3_Ort
GLOBAL(TH2S *,hE4p,hE4p = NULL);	// E4_Ort
GLOBAL(TH2S *,hE5p,hE5p = NULL);	// E5_Ort
GLOBAL(TH2S *,hDtp,hDtp = NULL);	// y_Winkel_Ort
GLOBAL(TH2S *,hTofp,hTofp = NULL);	// TOF_Ort
GLOBAL(TH2S *,hDpp,hDpp = NULL);	// x_Winkel_Ort
GLOBAL(TH2S *,hChi2p,hChi2p = NULL);	// Chi2_Ort
GLOBAL(TH2S *,hE1E4,hE1E4 = NULL);	// E1_E4
GLOBAL(TH2S *,hE1E5,hE1E5 = NULL);	// E1_E5
GLOBAL(TH2S *,hE4E5,hE4E5 = NULL);	// E4_E5
GLOBAL(TH1F *,hEtw,hEtw = NULL);	// Etot_W
GLOBAL(TH1F *,hE1w,hE1w = NULL);	// E1_W
GLOBAL(TH1F *,hE2w,hE2w = NULL);	// E2_W
GLOBAL(TH1F *,hE3w,hE3w = NULL);	// E3_W
GLOBAL(TH1F *,hE4w,hE4w = NULL);	// E4_W
GLOBAL(TH1F *,hE5w,hE5w = NULL);	// E5_W
GLOBAL(TH1F *,hPw,hPw = NULL);	// Ort_W
GLOBAL(TH1F *,hP2w,hP2w = NULL);	// Ort2_W
GLOBAL(TH1F *,hDtw,hDtw = NULL);	// y_Winkel_W
GLOBAL(TH1F *,hTofw,hTofw = NULL);	// TOF_W
GLOBAL(TH1F *,hDpw,hDpw = NULL);	// x_Winkel_W
GLOBAL(TH1F *,hTimew,hTimew = NULL);	// Time_W
GLOBAL(TH1F *,hChi2w,hChi2w = NULL);	// Chi2_W
GLOBAL(TH2S *,hEtpw,hEtpw = NULL);	// Etot_Ort_W
GLOBAL(TH2S *,hE1pw,hE1pw = NULL);	// E1_Ort_W
GLOBAL(TH2S *,hE2pw,hE2pw = NULL);	// E2_Ort_W
GLOBAL(TH2S *,hE3pw,hE3pw = NULL);	// E3_Ort_W
GLOBAL(TH2S *,hE4pw,hE4pw = NULL);	// E4_Ort_W
GLOBAL(TH2S *,hE5pw,hE5pw = NULL);	// E5_Ort_W
GLOBAL(TH2S *,hE1E4w,hE1E4w = NULL);	// E1_E4_W
GLOBAL(TH2S *,hE1E5w,hE1E5w = NULL);	// E1_E5_W
GLOBAL(TH2S *,hE4E5w,hE4E5w = NULL);	// E4_E5_W
GLOBAL(TH2S *,hDtpw,hDtpw = NULL);	// y_Winkel_Ort_W
GLOBAL(TH2S *,hTofpw,hTofpw = NULL);	// TOF_Ort_W
GLOBAL(TH2S *,hDppw,hDppw = NULL);	// x_Winkel_Ort_W
GLOBAL(TH2S *,hChi2pw,hChi2pw = NULL);	// Chi2_Ort_W

//_______________________________________________________________[GLOBAL DEFS]
//////////////////////////////////////////////////////////////////////////////
// Purpose:        Define other user globals
// Comment:        Any other user variable which should be globally available
//////////////////////////////////////////////////////////////////////////////

