#___________________________________________________________________[MAKEFILE]
#/////////////////////////////////////////////////////////////////////////////
# Name             GamsAnalyze.mk
# Purpose:         Compile and link user's analyzing code
# Targets:         M_analyze     -- user's analyze program
#                  clean         -- clean-up
# Author:          gams
# Revision:
# Date:
# URL:
#/////////////////////////////////////////////////////////////////////////////

PREFIX		=	Gams
AFILE 		=	GamsAnalyze

INCLUDE 	=	-I$(MARABOU)/include
OBJPATH		=	$(MARABOU)/obj

ROOTCFLAGS	=	$(shell root-config --cflags)
ROOTLIBS	=	$(shell root-config --libs)
ROOTGLIBS	=	$(shell root-config --glibs)

# Linux with egcs
CXX 		=	g++
CXXFLAGS	=	-g -c -O -fPIC -Wno-write-strings $(INCLUDE) -I./

LD			=	g++
LFLAGS		=	-g
CXXFLAGS	+=	$(ROOTCFLAGS)
LIBS		=	$(ROOTLIBS)
GLIBS		=	-L$(ROOTSYS)/lib -lGraf $(ROOTGLIBS)

TMRBLIBS	=	-L$(MARABOU)/lib -lTMrbAnalyze -lTMrbTransport -lTMrbUtils -lMutex

# Header files which GamsAnalyze.cxx depends on
USER_HDRS	=	\
				$(AFILE).h \
				$(AFILE)Globals.h \

ULIB_HDRS	=

# User code to be compiled separately
USER_CODE	=	udef/Initialize.cxx \
				udef/Analyze.cxx \
				udef/Analyze_Xstop.cxx

USER_OBJS	=	Initialize.o \
				Analyze.o \
				Analyze_Xstop.o

# User libs to be loaded
USER_LIBS	=


# User classes to be documented via THtml
DOCS		=	\
				TUsrEvtDevt \
				TUsrEvtXstop \
				TUsrSevtDet \
				TUsrSevtSsca

ALL			=	\
				M_analyze

.SUFFIXES:	.cxx .h

#------------------------------------------------------------------------------

all:		$(ALL)

M_analyze:	$(OBJPATH)/M_analyze.o lib$(AFILE).so
			$(LD) $(LFLAGS) -o M_analyze $(OBJPATH)/M_analyze.o \
					-L. -l$(AFILE) \
					$(USER_LIBS) \
					$(TMRBLIBS) \
					$(GLIBS) -lpthread

$(PREFIX)Fake:	$(PREFIX)Fake.o lib$(AFILE).so
			$(LD) $(LFLAGS) -o $(PREFIX)Fake $(PREFIX)Fake.o \
					-L. -l$(AFILE) \
					$(USER_LIBS) \
					$(TMRBLIBS) \
					$(GLIBS)

lib$(AFILE).so: $(USER_OBJS) $(AFILE).o $(AFILE)Dict.o
			$(CXX) $(LDFLAGS) -shared $(AFILE).o $(AFILE)Dict.o $(USER_OBJS) -o lib$(AFILE).so

$(AFILE).o: $(USER_HDRS) $(ULIB_HDRS) $(AFILE).cxx
			$(CXX) $(CXXFLAGS) $(AFILE).cxx

$(AFILE)Dict.cxx: $(USER_HDRS) $(ULIB_HDRS) $(AFILE).cxx
			rootcint -f $(AFILE)Dict.cxx -c $(INCLUDE) $(ULIB_HDRS) $(USER_HDRS) $(AFILE)LinkDef.h

$(AFILE)Dict.o:	$(AFILE)Dict.cxx
			$(CXX) $(CXXFLAGS) $(AFILE)Dict.cxx

Initialize.o:	$(USER_HDRS) $(ULIB_HDRS) udef/Initialize.cxx
			$(CXX) $(CXXFLAGS) -o Initialize.o udef/Initialize.cxx

Analyze.o:	$(USER_HDRS) $(ULIB_HDRS) udef/Analyze.cxx
			$(CXX) $(CXXFLAGS) -o Analyze.o udef/Analyze.cxx

Analyze_Xstop.o:	$(USER_HDRS) $(ULIB_HDRS) udef/Analyze_Xstop.cxx
			$(CXX) $(CXXFLAGS) -o Analyze_Xstop.o udef/Analyze_Xstop.cxx





doc:		$(AFILE).cxx
			@if [ ! -d HTML ]; then mkdir HTML; fi
			@r2html -s . -d HTML \
				-l libMutex.so \
				-l libTMrbUtils.so \
				-l libTGMrbUtils.so \
				-l libTMrbAnalyze.so \
				-l libTMrbTransport.so \
				-l `pwd`/lib$(AFILE).so \
				$(DOCS)
			@for i in $(AFILE).html $(USER_HDRS) $(USER_CODE); do \
				ln -f $$i HTML; \
			done
			@cd HTML; updhref *.html
			@cd HTML/src; updhref *.html

clean:
			rm -f *.o $(USER_OBJS) lib$(AFILE).so $(AFILE)Dict.cxx

.cxx.o:
			$(CXX) $(CXXFLAGS) -o $@ $<
