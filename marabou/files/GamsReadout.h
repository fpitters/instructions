/*_________________________________________________________[C DEFINITION FILE]
//////////////////////////////////////////////////////////////////////////////
// Name:           GamsReadout.h
// Purpose:        MBS readout function
//                 gams: GAMS experiment
//
// Description:    Definitions to be used byt GamsReadout.c
//
// Author:         gams
// Revision:         
// Date:           Sun Feb 17 11:13:39 2019
// Keywords:       
////////////////////////////////////////////////////////////////////////////*/


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/ Device tables
/~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

typedef struct {
	char * name;
	int index;
	int crate;
	int station;
	unsigned long lam;
	int nofParams;
	int nofParamsUsed;
	unsigned long paramPattern;
	void * addr;
	void * devStruct;
} ModuleDef;

typedef struct {
	char * name;
	int index;
	int nofModules;
	int firstModule;
	unsigned long lamPattern;
} SevtDef;

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/ Useful macro defs
/~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#define CC_AUTOREAD_ON(cc)	cc += 0x0c; \
							cctmp = *((volatile unsigned short *) cc); \
							*((volatile unsigned short *) cc) = cctmp | (0x1 << 12)

#define CC_AUTOREAD_OFF(cc)	cc += 0x0c; \
							cctmp = *((volatile unsigned short *) cc); \
							*((volatile unsigned short *) cc) = cctmp & ~(0x1 << 12)

#define CC_READ_DWORD_ON(cc)	CIO_WRITE_R2B(cc, A(2), F(16), 0)
#define CC_READ_DWORD_OFF(cc)	CIO_WRITE_R2B(cc, A(3), F(16), 0)

#define WITH_SCALE_DOWN(n)	if ((total_event_count % n) == 0)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/ Device-dependent definitions
/~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

unsigned short cctmp;	/* temp for vc32/cc32 controller functions */

/* bit definitions for VME ADC CAEN V785 */
#ifndef __CAEN_BITS_AND_BYTES__
#define __CAEN_BITS_AND_BYTES__
#define CAEN_V785_R_OUTPUT						0x0
#define CAEN_V785_R_FIRMWARE						0x1000
#define CAEN_V785_785_R_GEOADDR						0x1002
#define CAEN_V785_R_BITSET1						0x1006
#define CAEN_V785_R_BITCLEAR1					0x1008
#define CAEN_V785_R_STATUS1						0x100E
#define CAEN_V785_R_CONTROL1						0x1010
#define CAEN_V785_R_ADDR_HIGH					0x1012
#define CAEN_V785_R_ADDR_LOW						0x1014
#define CAEN_V785_R_EVT_TRIG						0x1020
#define CAEN_V785_R_STATUS2						0x1022
#define CAEN_V785_R_EVT_CNT_LOW					0x1024
#define CAEN_V785_R_EVT_CNT_HIGH 				0x1026
#define CAEN_V785_R_INCR_EVT						0x1028
#define CAEN_V785_R_INCR_OFFS					0x102A
#define CAEN_V785_R_BITSET2						0x1032
#define CAEN_V785_R_BITCLEAR2					0x1034
#define CAEN_V785_R_CRATE_SEL					0x103C
#define CAEN_V785_R_TEST_EVT_WRITE				0x103E
#define CAEN_V785_R_EVT_CNT_RESET				0x1040
#define CAEN_V785_R_THRESH 						0x1080
#define CAEN_V785_R_ROM	 						0x8000

#define CAEN_V785_B_BITSET1_SEL_ADDR_INTERN 		(0x1 << 4)
#define CAEN_V785_B_BITSET1_RESET			 		(0x1 << 7)
#define CAEN_V785_B_CONTROL1_PROG_RESET_MODULE	(0x1 << 4)
#define CAEN_V785_B_BITSET2_OFFLINE				(0x1 << 1)
#define CAEN_V785_B_BITSET2_CLEAR_DATA		(0x1 << 2)
#define CAEN_V785_B_BITSET2_OVER_RANGE_DIS		(0x1 << 3)
#define CAEN_V785_B_BITSET2_LOW_THRESH_DIS		(0x1 << 4)
#define CAEN_V785_B_BITSET2_SLIDE_ENA			(0x1 << 7)
#define CAEN_V785_B_BITSET2_STEP_TH 			(0x1 << 8)
#define CAEN_V785_B_BITSET2_AUTO_INCR_ENA 		(0x1 << 11)
#define CAEN_V785_B_BITSET2_EMPTY_PROG_ENA		(0x1 << 12)
#define CAEN_V785_B_BITSET2_ALL_TRIG_ENA			(0x1 << 14)
#define CAEN_V785_B_STATUS2_BUFFER_EMPTY			(0x1 << 1)
#define CAEN_V785_B_THRESH_KILL_CHANNEL 			(0x1 << 8)

#define CAEN_V785_SH_WC							8

#define CAEN_V785_D_HDR							0x02000000
#define CAEN_V785_D_DATA							0x00000000
#define CAEN_V785_D_EOB							0x04000000
#define CAEN_V785_D_INVALID						0x06000000

#define CAEN_V785_M_MSERIAL						0xFF
#define CAEN_V785_M_WC							0x3F
#define CAEN_V785_M_CHN							0xFF
#define CAEN_V785_M_ID							0x07000000
#define CAEN_V785_M_HDR							0x07FFFFFF
#define CAEN_V785_M_DATA						0x07FFFFFF
#define CAEN_V785_M_ADCDATA						0x00000FFF

#define CAEN_V785_B_OVERFLOW	 				(0x1 << 12)
#define CAEN_V785_B_UNDERTHRESH 				(0x1 << 13)

#define CAEN_V785_N_MAXEVENTS					32

#define CAEN_V785_A_OUTPUT(module)			((unsigned int *) (module + CAEN_V785_R_OUTPUT))
#define CAEN_V785_A_FIRMWARE(module) 		((unsigned short *) (module + CAEN_V785_R_FIRMWARE))
#define CAEN_V785_A_GEOADDR(module)			((unsigned short *) (module + CAEN_V785_R_GEOADDR))
#define CAEN_V785_A_BITSET1(module)			((unsigned short *) (module + CAEN_V785_R_BITSET1))
#define CAEN_V785_A_BITCLEAR1(module)		((unsigned short *) (module + CAEN_V785_R_BITCLEAR1))
#define CAEN_V785_A_STATUS1(module)			((unsigned short *) (module + CAEN_V785_R_STATUS1))
#define CAEN_V785_A_CONTROL1(module) 		((unsigned short *) (module + CAEN_V785_R_CONTROL1))
#define CAEN_V785_A_ADDR_HIGH(module) 		((unsigned short *) (module + CAEN_V785_R_ADDR_HIGH))
#define CAEN_V785_A_ADDR_LOW(module)			((unsigned short *) (module + CAEN_V785_R_ADDR_LOW))
#define CAEN_V785_A_EVT_TRIG(module)			((unsigned short *) (module + CAEN_V785_R_EVT_TRIG))
#define CAEN_V785_A_STATUS2(module)			((unsigned short *) (module + CAEN_V785_R_STATUS2))
#define CAEN_V785_A_EVT_CNT_LOW(module)		((unsigned short *) (module + CAEN_V785_R_EVT_CNT_LOW))
#define CAEN_V785_A_EVT_CNT_HIGH(module) 	((unsigned short *) (module + CAEN_V785_R_EVT_CNT_HIGH))
#define CAEN_V785_A_INCR_EVT(module)			((unsigned short *) (module + CAEN_V785_R_INCR_EVT))
#define CAEN_V785_A_INCR_OFFS(module) 		((unsigned short *) (module + CAEN_V785_R_INCR_OFFS))
#define CAEN_V785_A_BITSET2(module)			((unsigned short *) (module + CAEN_V785_R_BITSET2))
#define CAEN_V785_A_BITCLEAR2(module)		((unsigned short *) (module + CAEN_V785_R_BITCLEAR2))
#define CAEN_V785_A_CRATE_SEL(module) 		((unsigned short *) (module + CAEN_V785_R_CRATE_SEL))
#define CAEN_V785_A_TEST_EVT_WRITE(module)	((unsigned short *) (module + CAEN_V785_R_TEST_EVT_WRITE))
#define CAEN_V785_A_EVT_CNT_RESET(module)	((unsigned short *) (module + CAEN_V785_R_EVT_CNT_RESET))
#define CAEN_V785_A_THRESH(module)			((unsigned short *) (module + CAEN_V785_R_THRESH))
#define CAEN_V785_A_ROM(module)				((unsigned short *) (module + CAEN_V785_R_ROM))

#define CAEN_V785_SET_THRESHOLD(module,chn,data) 	*(CAEN_V785_A_THRESH(module) + chn) = data

#define CAEN_V785_AUTO_INCR_ON(module)	*CAEN_V785_A_BITSET2(module) = CAEN_V785_B_BITSET2_AUTO_INCR_ENA
#define CAEN_V785_AUTO_INCR_OFF(module)	*CAEN_V785_A_BITCLEAR2(module) = CAEN_V785_B_BITSET2_AUTO_INCR_ENA

#define CAEN_V785_ZERO_SUPPR_ON(module)	*CAEN_V785_A_BITCLEAR2(module) = CAEN_V785_B_BITSET2_LOW_THRESH_DIS
#define CAEN_V785_ZERO_SUPPR_OFF(module)	*CAEN_V785_A_BITSET2(module) = CAEN_V785_B_BITSET2_LOW_THRESH_DIS

#define CAEN_V785_ALL_TRIG_ON(module)		*CAEN_V785_A_BITSET2(module) = CAEN_V785_B_BITSET2_ALL_TRIG_ENA
#define CAEN_V785_ALL_TRIG_OFF(module)	*CAEN_V785_A_BITCLEAR2(module) = CAEN_V785_B_BITSET2_ALL_TRIG_ENA

#define CAEN_V785_THRESH_RES_COARSE(module) *CAEN_V785_A_BITCLEAR2(module) = CAEN_V785_B_BITSET2_STEP_TH
#define CAEN_V785_THRESH_RES_FINE(module)	*CAEN_V785_A_BITSET2(module) = CAEN_V785_B_BITSET2_STEP_TH

#define CAEN_V785_SLIDING_SCALE_ON(module)	*CAEN_V785_A_BITSET2(module) = CAEN_V785_B_BITSET2_SLIDE_ENA
#define CAEN_V785_SLIDING_SCALE_OFF(module)	*CAEN_V785_A_BITCLEAR2(module) = CAEN_V785_B_BITSET2_SLIDE_ENA

#define CAEN_V785_OVER_RANGE_CHECK_ON(module)	*CAEN_V785_A_BITCLEAR2(module) = CAEN_V785_B_BITSET2_OVER_RANGE_DIS
#define CAEN_V785_OVER_RANGE_CHECK_OFF(module)	*CAEN_V785_A_BITSET2(module) = CAEN_V785_B_BITSET2_OVER_RANGE_DIS

#define CAEN_V785_EMPTY_PROG_ENABLE(module)	*CAEN_V785_A_BITSET2(module) = CAEN_V785_B_BITSET2_EMPTY_PROG_ENA
#define CAEN_V785_EMPTY_PROG_DISABLE(module)	*CAEN_V785_A_BITCLEAR2(module) = CAEN_V785_B_BITSET2_EMPTY_PROG_ENA

#define CAEN_V785_CLEAR_DATA(module)	*CAEN_V785_A_BITSET2(module) = CAEN_V785_B_BITSET2_CLEAR_DATA; \
											*CAEN_V785_A_BITCLEAR2(module) = CAEN_V785_B_BITSET2_CLEAR_DATA

#define CAEN_V785_RESET_EVENT_COUNTER(module)	*CAEN_V785_A_EVT_CNT_RESET(module) = 1

#define CAEN_V785_SET_KILL_BIT(module,chn)	*(CAEN_V785_A_THRESH(module) + chn) = CAEN_V785_B_THRESH_KILL_CHANNEL

#define CAEN_V785_READ_BITSET2(module)	*CAEN_V785_A_BITSET2(module)
#endif

/* bit definitions for VME scaler SIS 3820 */
#define SIS_3820_CS_B_REF_PULSE					(0x1 << 6)

#define SIS_3820_R_STATUS					0x0L
#define SIS_3820_R_IDENT					0x4L
#define SIS_3820_R_IRQSTATUS 				0xCL
#define SIS_3820_R_ACQ_PRESET				0x10L
#define SIS_3820_R_ACQ_COUNT				0x14L
#define SIS_3820_R_LNE_PRESC_FACTOR			0x18L
#define SIS_3820_R_PRESET_VALUE_1			0x20L
#define SIS_3820_R_PRESET_VALUE_2			0x24L
#define SIS_3820_R_PRESET_ENABLE			0x28L
#define SIS_3820_R_FIFO_WC					0x38L
#define SIS_3820_R_FIFO_WC_THRESH			0x3CL
#define SIS_3820_R_HISCAL_START_PRESET		0x40L
#define SIS_3820_R_HISCAL_START_COUNTER		0x44L
#define SIS_3820_R_HISCAL_LAST_ACQ_COUNTER	0x48L
#define SIS_3820_R_ACQ_OPER_MODE			0x100L
#define SIS_3820_R_COPY_DISABLE				0x104L
#define SIS_3820_R_LNE_CHAN_SELECT			0x108L
#define SIS_3820_R_PRESET_CHAN_SELECT		0x10CL
#define SIS_3820_R_INHIBIT_COUNTING			0x200L
#define SIS_3820_R_CLEAR					0x204L
#define SIS_3820_R_OVERFLOW					0x208L
#define SIS_3820_R_48BIT_EXTENSION			0x210L
#define SIS_3820_R_KEY_RESET				0x400L
#define SIS_3820_R_KEY_FIFO_RESET			0x404L
#define SIS_3820_R_KEY_TEST_PULSE			0x408L
#define SIS_3820_R_KEY_COUNTER_CLEAR		0x40CL
#define SIS_3820_R_KEY_VME_LNE				0x410L
#define SIS_3820_R_KEY_OPER_ARM 			0x414L
#define SIS_3820_R_KEY_OPER_ENABLE			0x418L
#define SIS_3820_R_KEY_OPER_DISABLE			0x41CL
#define SIS_3820_R_READ_SHADOW				0x800L
#define SIS_3820_R_READ_FIFO				0xA00L

#define SIS_3820_CS_B_REF_PULSER				(0x1 << 6)

#define SIS_3820_ACQ_B_INPUT_MODE_0				0x0
#define SIS_3820_ACQ_B_INPUT_MODE_1				(0x1 << 16)
#define SIS_3820_ACQ_B_INPUT_MODE_2				(0x2 << 16)
#define SIS_3820_ACQ_B_INPUT_MODE_3				(0x3 << 16)
#define SIS_3820_ACQ_B_INPUT_MODE_4				(0x4 << 16)
#define SIS_3820_ACQ_B_INPUT_MODE_5				(0x5 << 16)
#define SIS_3820_ACQ_B_INPUT_MODE_6				(0x6 << 16)

#define SIS_3820_ACQ_B_OUTPUT_MODE_0				0x0
#define SIS_3820_ACQ_B_OUTPUT_MODE_1				(0x1 << 20)
#define SIS_3820_ACQ_B_OUTPUT_MODE_2				(0x2 << 20)
#define SIS_3820_ACQ_B_OUTPUT_MODE_3				(0x3 << 20)

#define SIS_3820_ACQ_B_LNE_SOURCE_0				0x0
#define SIS_3820_ACQ_B_LNE_SOURCE_1				(0x1 << 4)
#define SIS_3820_ACQ_B_LNE_SOURCE_2				(0x2 << 4)
#define SIS_3820_ACQ_B_LNE_SOURCE_3				(0x3 << 4)
#define SIS_3820_ACQ_B_LNE_SOURCE_4				(0x4 << 4)

#define SIS_3820_ACQ_B_CLEAR_ON_COPY			0x0
#define SIS_3820_ACQ_B_NON_CLEARING_MODE		0x1

#define SIS_3820_ACQ_B_DATA_FORMAT_32			0x0
#define SIS_3820_ACQ_B_DATA_FORMAT_24			0x4

#define SIS_3820_ACQ_B_SELECT_SDRAM_MODE		(0x1 << 12)

#define SIS_3820_ACQ_B_SINGLE_HIT				0x0
#define SIS_3820_ACQ_B_MULTI_HIT				(0x2 << 28)

#define SIS_3820_FLAG_B_FIFO_THRESH				(0x1 << 17)

#define SIS_3820_A_STATUS(module)				((unsigned long *) (module + SIS_3820_R_STATUS))
#define SIS_3820_A_IDENT(module)				((unsigned long *) (module + SIS_3820_R_IDENT))
#define SIS_3820_A_IRQSTATUS(module)			((unsigned long *) (module + SIS_3820_R_IRQSTATUS))
#define SIS_3820_A_ACQ_PRESET(module)			((unsigned long *) (module + SIS_3820_R_ACQ_PRESET))
#define SIS_3820_A_ACQ_COUNT(module)			((unsigned long *) (module + SIS_3820_R_ACQ_COUNT))
#define SIS_3820_A_LNE_PRESC_FACTOR(module)		((unsigned long *) (module + SIS_3820_R_LNE_PRESC_FACTOR))
#define SIS_3820_A_PRESET_VALUE_1(module)		((unsigned long *) (module + SIS_3820_R_PRESET_VALUE_1))
#define SIS_3820_A_PRESET_VALUE_2(module)		((unsigned long *) (module + SIS_3820_R_PRESET_VALUE_2))
#define SIS_3820_A_FIFO_WC(module)				((unsigned long *) (module + SIS_3820_R_FIFO_WC))
#define SIS_3820_A_FIFO_WC_THRESH(module)		((unsigned long *) (module + SIS_3820_R_FIFO_WC_THRESH))
#define SIS_3820_A_LNE_CHAN_SELECT(module)			((unsigned long *) (module + SIS_3820_R_LNE_CHAN_SELECT))
#define SIS_3820_A_ACQ_OPER_MODE(module)		((unsigned long *) (module + SIS_3820_R_ACQ_OPER_MODE))
#define SIS_3820_A_COPY_DISABLE(module) 		((unsigned long *) (module + SIS_3820_R_COPY_DISABLE))
#define SIS_3820_A_LNE_CHAN_SELECT(module)		((unsigned long *) (module + SIS_3820_R_LNE_CHAN_SELECT))
#define SIS_3820_A_PRESET_CHAN_SELECT(module)	((unsigned long *) (module + SIS_3820_R_PRESET_CHAN_SELECT))
#define SIS_3820_A_INHIBIT_COUNTING(module) 	((unsigned long *) (module + SIS_3820_R_INHIBIT_COUNTING))
#define SIS_3820_A_CLEAR(module) 				((unsigned long *) (module + SIS_3820_R_CLEAR))
#define SIS_3820_A_OVERFLOW(module) 			((unsigned long *) (module + SIS_3820_R_OVERFLOW))
#define SIS_3820_A_KEY_RESET(module)			((unsigned long *) (module + SIS_3820_R_KEY_RESET))
#define SIS_3820_A_KEY_FIFO_RESET(module)		((unsigned long *) (module + SIS_3820_R_KEY_FIFO_RESET))
#define SIS_3820_A_KEY_COUNTER_CLEAR(module)	((unsigned long *) (module + SIS_3820_R_KEY_COUNTER_CLEAR))
#define SIS_3820_A_KEY_VME_LNE(module)			((unsigned long *) (module + SIS_3820_R_KEY_VME_LNE))
#define SIS_3820_A_KEY_OPER_ARM(module) 		((unsigned long *) (module + SIS_3820_R_KEY_OPER_ARM))
#define SIS_3820_A_KEY_OPER_ENABLE(module)		((unsigned long *) (module + SIS_3820_R_KEY_OPER_ENABLE))
#define SIS_3820_A_KEY_OPER_DISABLE(module)		((unsigned long *) (module + SIS_3820_R_KEY_OPER_DISABLE))
#define SIS_3820_A_EXT48(module)				((unsigned long *) (module + SIS_3820_R_48BIT_EXTENSION))

#define SIS_3820_READ_STATUS(module)			*SIS_3820_A_STATUS(module)
#define SIS_3820_READ_IRQS(module)				*SIS_3820_A_IRQSTATUS(module)
#define SIS_3820_READ_FIFO_WC(module)			*SIS_3820_A_FIFO_WC(module)
#define SIS_3820_READ_OVERFLOWS(module)			*SIS_3820_A_OVERFLOW(module)
#define SIS_3820_SET_ACQ_OPER_MODE(module, b)	*SIS_3820_A_ACQ_OPER_MODE(module) = b
#define SIS_3820_DISABLE_COPY(module, b)		*SIS_3820_A_COPY_DISABLE(module) = b
#define SIS_3820_INHIBIT_COUNTING(module, b) 	*SIS_3820_A_INHIBIT_COUNTING(module) = b
#define SIS_3820_WRITE_FIFO_THRESH(module, b)	*SIS_3820_A_FIFO_WC_THRESH(module) = b
#define SIS_3820_SELECT_LNE_CHANNEL(module, c) 	*SIS_3820_A_LNE_CHAN_SELECT(module) = c
#define SIS_3820_CLEAR_OVERFLOWS(module)		*SIS_3820_A_OVERFLOW(module) = 0xFFFFFFFF
#define SIS_3820_RESET(module)					*SIS_3820_A_KEY_RESET(module) = 0
#define SIS_3820_KEY_VME_LNE(module)			*SIS_3820_A_KEY_VME_LNE(module) = 0
#define SIS_3820_CLEAR_FIFO(module)				*SIS_3820_A_KEY_FIFO_RESET(module) = 0
#define SIS_3820_OPER_ARM(module)				*SIS_3820_A_KEY_OPER_ARM(module) = 0
#define SIS_3820_OPER_ENABLE(module)			*SIS_3820_A_KEY_OPER_ENABLE(module) = 0
#define SIS_3820_OPER_DISABLE(module)			*SIS_3820_A_KEY_OPER_DISABLE(module) = 0
#define SIS_3820_ENABLE_REF_PULSER(module)		*SIS_3820_A_STATUS(module) = SIS_3820_CS_B_REF_PULSER
#define SIS_3820_DISABLE_REF_PULSER(module)		*SIS_3820_A_STATUS(module) = 0
#define SIS_3820_COUNTER_CLEAR(module)			*SIS_3820_A_KEY_COUNTER_CLEAR(module) = 0
#define SIS_3820_READ_EXT48(module)				*SIS_3820_A_EXT48(module)
