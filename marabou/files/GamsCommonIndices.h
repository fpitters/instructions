#ifndef __GamsCommonIndices_h__
#define __GamsCommonIndices_h__

//_________________________________________________[C++ CLASS DEFINITION FILE]
//////////////////////////////////////////////////////////////////////////////
// Name:           GamsCommonIndices.h
// Purpose:        Index declarations common to analysis and readout
// Description:    Defines a set of indices which should be used in analysis
//                 as well as readout code to guarantee coherent addresses
//
// Author:         gams
// Revision:         
// Date:           Sun Feb 17 11:13:39 2019
// URL:            
// Keywords:       
//////////////////////////////////////////////////////////////////////////////

enum EMrbSevt_10_12	{	kMrbSevt_10_12_B_Header 		=	(0x1 << 15),
						kMrbSevt_10_12_M_ModuleNumber	=	0xFF,
						kMrbSevt_10_12_M_ModuleId		=	0x7F,
						kMrbSevt_10_12_SH_ModuleId		=	8
					};

enum EMrbTriggerId	{
						kMrbTriggerDevt = 1,		// event devt, trigger 1
						kMrbTriggerXstop = 15,		// event xstop, trigger 15
						kMrbTriggerStartAcquisition = 14,	// start acquisition, trigger 14
						kMrbTriggerStopAcquisition = 15 	// stop acquisition, trigger 15
					};

enum EMrbSevtSerial {
 						kMrbSevtDet = 1,				// subevent det, unique serial number
 						kMrbSevtSsca = 2,				// subevent ssca, unique serial number
						kMrbSevtDeadTime = 998, 			// dead time, serial=998
						kMrbSevtTimeStamp = 999, 			// time stamp, serial=999
						kMrbSevtDummy = 888 	 			// dummy subevent created by MBS internally, serial=888
					};

enum EMrbSevtBits	 {
 						kMrbSevtBitDet = 0x1,		// subevent det
 						kMrbSevtBitSsca = 0x2,		// subevent ssca
					};


enum EMrbModuleID {
						kMrbModIdCaen_v785 = 0x4000d,			// type Caen_v785 (CAEN V785 ADC 32 x 12 bit)
						kMrbModIdSis_3820 = 0x100001e,			// type Sis_3820 (SIS 3820 scaler 32 x 32 bit)
					};

enum EMrbModuleSerial {
						kMrbModuleAdc = 1,			// module adc (CAEN V785 ADC 32 x 12 bit)
						kMrbModuleSca = 2,			// module sca (SIS 3820 scaler 32 x 32 bit)
					};


enum	{	kParEt	=	0	};		// 
enum	{	kParE1l	=	1	};		// 
enum	{	kParE1r	=	2	};		// 
enum	{	kParE2l	=	3	};		// 
enum	{	kParE2r	=	4	};		// 
enum	{	kParE3	=	5	};		// 
enum	{	kParE4	=	6	};		// 
enum	{	kParE5	=	7	};		// 
enum	{	kParDt	=	8	};		// 
enum	{	kParTof	=	9	};		// 
enum	{	kParPup	=	10	};		// 
#endif
